/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.msdk.io.PklReader;
import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Nov 2018, 3:00 PM
 */
public class MSFraggerComplementaryTest
{
    @Test
    public void testCharge2() throws IOException
    {
        MSFraggerComplementary complementary = new MSFraggerComplementary();

        PklReader reader = new PklReader(new File(FileUtils.getResource("complementary.pkl").getFile()));
        MsnSpectrum spectrum = reader.next();
        assertNotNull(spectrum);

        MsnSpectrum copy = spectrum.copy(complementary);
        assertEquals(copy.size(), spectrum.size() + 19); // with one peak less than 100
    }

}