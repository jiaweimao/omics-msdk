/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Jan 2019, 8:21 PM
 */
public class AddPeakRankAnnotationTest
{

    @Test
    public void test()
    {
        PeakList pl = new DoublePeakList();
        pl.add(1, 3); // 3
        pl.add(3, 2); // 4
        pl.add(5, 5); // 2
        pl.add(7, 7); // 1
        pl.add(10, 2); // 5

        pl.apply(new AddPeakRankAnnotation());

        int[] rankArray = new int[]{3, 4, 2, 1, 5};
        for (int i = 0; i < pl.size(); i++) {
            assertEquals(((PeakRankAnnotation) pl.getFirstAnnotation(i)).getRank(), rankArray[i]);
        }
    }

}