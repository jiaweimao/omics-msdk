/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoubleFloatPeakList;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Sep 2018, 10:05 AM
 */
public class ComplementaryGeneratorTest
{

    @Test
    public void test()
    {
        PeakList pl = new DoubleFloatPeakList();
        pl.setPrecursor(Peak.noIntensity(500, 1));
        pl.add(10, 1);
        pl.add(20, 1);
        pl.add(30, 1);
        pl.add(40, 1);
        pl.add(50, 1);
        pl.add(60, 1);

        double mass = pl.getPrecursor().getMass() + PeriodicTable.PROTON_MASS * 2;
        PeakList copy = pl.copy(new ComplementaryGenerator());
        for (int i = 0; i < copy.size(); i++) {
            assertEquals(copy.getX(i), mass - pl.getX(pl.size() - i - 1), 0.01);
        }
    }

}