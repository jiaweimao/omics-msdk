/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.msdk.io.PklReader;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.ms.peaklist.impl.PpmTolerance;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 09 Nov 2018, 8:53 AM
 */
public class ComplementaryFinderTest {
    @Test
    public void testProcessCached() throws IOException {
        String file = getClass().getClassLoader().getResource("complementary.pkl").getFile();
        PklReader reader = new PklReader(new File(file));
        MsnSpectrum spectrum = reader.next();

        ComplementaryFinder finder = new ComplementaryFinder(new PpmTolerance(10), ComplementaryFinder.Intensify.HIGHER, new AbsoluteTolerance(0.05));
        MsnSpectrum copy = spectrum.copy(finder);
        reader.close();
    }
}