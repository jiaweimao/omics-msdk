/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.msdk.io.MzMLReader;
import omics.msdk.io.MzMLWriter;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Spectrum;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Sep 2018, 9:50 PM
 */
public class ZscoreDeisotoperTest
{
    public static void main(String[] args) throws IOException, XMLStreamException
    {
        ZscoreDeisotoper deisotoper = new ZscoreDeisotoper(new AbsoluteTolerance(0.02));

        MzMLReader reader = new MzMLReader(new File("Z:\\MaoJiawei\\dataset-2\\test\\b1937_293T_proteinID_01B_QE3_122212.mzML"));
        MzMLWriter writer = new MzMLWriter(new File("Z:\\MaoJiawei\\dataset-2\\test\\b1937_293T_proteinID_01B_QE3_122212_zscore.mzML"));
        writer.writeStartDocument();
        writer.writeFileHeader(reader.getHeader());
        writer.writeStartSpectrumList();

        int count = 0;
        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
            if (spectrum.isMsnSpectrum()) {
                MsnSpectrum msnSpectrum = spectrum.asMsnSpectrum();
                msnSpectrum.apply(deisotoper);
                writer.writeSpectrum(msnSpectrum);

                count++;
                System.out.println(count);
            }
        }
        writer.writeEndSpectrumList();
        writer.writeEndDocument();

        writer.close();
        reader.close();
    }

}