/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.msdk.io.MgfWriter;
import omics.msdk.io.MzMLAccessor;
import omics.msdk.model.MSDataFile;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Sep 2018, 4:16 PM
 */
public class PoissonDeisotoperTest {

    @Test(enabled = false)
    public void export() throws IOException, XMLStreamException {
        MzMLAccessor access = new MzMLAccessor(new File("I:\\dataset1\\rawdata\\20170803_Serum_Native_Prefraction_Run1_F5.mzML"));
        access.start();
        MSDataFile value = access.getValue();
        MgfWriter writer = new MgfWriter(new File("I:\\test\\deisotope\\20161027_phostest_2_161027170116-qb.mgf"));
        for (MsnSpectrum spectrum : value.getSpectrumList()) {
            writer.writeSpectrum(spectrum);
        }
        writer.close();
        value.clear();
    }

    @Test
    public void test4() {

    }

    @Test
    public void test() {
        double[][] testPeak1 = new double[][]{
                {300.0, 302.1, 303.11, 304.12, 305.20},
                {1.0, 85.0, 15.0, 3.0, 3.0}
        };
        double[][] testPeak2 = new double[][]{
                {299.5, 300.01, 300.52, 301.03},
                {10.0, 75.0, 25.0, 40.0}
        };
        double[][] testPeak3 = new double[][]{
                {302.1, 302.435, 302.77, 302.94, 303.11},
                {61.0, 31.0, 8.0, 45.0, 40.0}
        };

        double[][] std1 = new double[][]{
                {300.0, 302.1, 305.20},
                {1.0, 85.0, 3.0}
        };
        double[][] std2 = new double[][]{
                {299.5, 300.01, 301.03},
                {10.0, 75.0, 40.0}
        };
        double[][] std3 = new double[][]{
                {302.1, 302.94, 303.11},
                {61.0, 45.0, 40.0}
        };

        testData(testPeak1, std1);
        testData(testPeak2, std2);
        testData(testPeak3, std3);

    }

    private void testData(double[][] testData, double[][] stdData) {
        PeakList pl = parse(testData);
//        pl.apply(new PoissonDeisotoper());

        ArrayAsserts.assertArrayEquals(pl.getXs(), stdData[0], 1e-5);
        ArrayAsserts.assertArrayEquals(pl.getYs(), stdData[1], 1e-5);
    }

    private PeakList parse(double[][] data) {
        PeakList pl = new DoublePeakList(data[0].length);
        for (int i = 0; i < data[0].length; i++) {
            pl.add(data[0][i], data[1][i]);
        }
        return pl;
    }

}