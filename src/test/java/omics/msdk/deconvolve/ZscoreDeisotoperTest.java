/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.deconvolve;

import omics.msdk.io.MgfReader;
import omics.msdk.io.MgfWriter;
import omics.msdk.io.MzMLReader;
import omics.msdk.processors.ChargeAssigner;
import omics.msdk.processors.ZscoreDeisotoper;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Polarity;
import omics.util.ms.Spectrum;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Precision;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.ms.peaklist.transform.RemoveAllPeakProcessor;
import omics.util.utils.ArrayIndexSorter;
import omics.util.utils.ArrayUtils;
import omics.util.utils.DoubleDescendingComparator;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author JiaweiMao on 2017.07.23
 * @since 1.0.0
 */
public class ZscoreDeisotoperTest {
    @Test
    void collapse() throws IOException {
        ZscoreDeisotoper zscore = new ZscoreDeisotoper(new AbsoluteTolerance(0.05), Polarity.POSITIVE, true);

        MgfReader reader = new MgfReader(new File("G:\\test_data\\phosph\\20161027_phostest_2_161027170116.mgf"));
        MgfWriter writer = new MgfWriter(new File("G:\\test_data\\phosph\\20161027_phostest_2_161027170116_iso.mgf"));
        MgfWriter writer2 = new MgfWriter(new File("G:\\test_data\\phosph\\20161027_phostest_2_161027170116_raw.mgf"));

        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();

            if (spectrum.size() < 6)
                continue;

            MsnSpectrum peakList = spectrum.copy(zscore);

            MsnSpectrum copy = spectrum.copy(new RemoveAllPeakProcessor<>());
            copy.addPeaksNoAnnotations(peakList);

            writer.writeSpectrum(copy);
            writer2.writeSpectrum(spectrum);
        }
        writer.close();
        writer2.close();
        reader.close();
    }

    @Test
    void sort() {
        PeakList<PeakAnnotation> pl = new DoublePeakList<>();
        pl.add(1, 1);
        pl.add(2, 2);
        pl.add(3, 3);
        pl.add(4, 4);
        pl.add(5, 5);
        pl.add(6, 6);
        pl.add(7, 7);
        pl.add(8, 8);
        pl.add(10, 9);

        Integer[] sort = ArrayIndexSorter.sort(new DoubleDescendingComparator(ArrayUtils.toObject(pl.getYs())));
        System.out.println(Arrays.toString(sort));
    }

    @Test
    void assign() throws IOException {
        MzMLReader reader = new MzMLReader(new File("G:\\methyl\\20151217\\raw\\mzML\\hM3_HEK293_11_20151205_NF17.mzML"), Precision.DOUBLE);

        HashMap<Integer, MsnSpectrum> map = new HashMap<>();
        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
            if (spectrum instanceof MsnSpectrum) {
                MsnSpectrum sp = (MsnSpectrum) spectrum;
                map.put(sp.getSpectrumID().getScanNumber().getValue(), sp);
            }
        }
        reader.close();


        MsnSpectrum spectrum = map.get(29642);

        ChargeAssigner assigner = new ZscoreDeisotoper(new AbsoluteTolerance(0.05), Polarity.POSITIVE, false);
        MsnSpectrum peakList = spectrum.copy(assigner);
        printPeaks(peakList);

    }

    private void printPeaks(MsnSpectrum peakList) {
        for (int i = 0; i < peakList.size(); i++) {
            System.out.print(peakList.getMz(i) + "  " + peakList.getIntensity(i) + "  ");
            List<PeakAnnotation> annotations = peakList.getAnnotations(i);
            if (!annotations.isEmpty()) {
                for (PeakAnnotation annotation : annotations) {
                    System.out.print(annotation.getSymbol() + " ");
                }
            }

            System.out.println();
        }
    }

}