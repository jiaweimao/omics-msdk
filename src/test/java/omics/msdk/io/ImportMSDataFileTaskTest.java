/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSDataFile;
import org.testng.annotations.Test;

import java.io.File;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 18 Jan 2018, 7:09 PM
 */
public class ImportMSDataFileTaskTest {

    @Test
    public void testGo() throws Exception {
        MSDataAccessor task = new MSDataAccessor(new File("Z:\\MaoJiawei\\test\\MS\\hM3_HEK293_11_20151205_NF5.mzML"));
        task.start();

        MSDataFile dataFile = task.getValue();
        System.out.println(dataFile.getSpectrumList().size());

        dataFile.clear();
    }
}