/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.CvTermList;
import omics.msdk.io.util.ReferenceableParamGroup;
import omics.msdk.model.*;
import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Polarity;
import omics.util.ms.Spectrum;
import omics.util.ms.SpectrumType;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.util.MzMLSpectrumID;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 07 May 2018, 2:53 PM
 */
public class MzMLReaderTest {

    @Test
    public void testPrecursorScan() throws IOException {
        MzMLReader reader = new MzMLReader(new File("H:\\methyl\\pair label data\\20151217\\spec_data\\mzML\\hM3_HEK293_11_20151205_CF1.mzML"));
        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
        }

        reader.close();
    }

    @Test
    public void testBufferSize() throws IOException {
        long t1 = System.currentTimeMillis();
        MzMLReader reader = new MzMLReader(new File("Z:\\MaoJiawei\\dataset-2\\mzML_ms_convert\\b1906_293T_proteinID_01A_QE3_122212.mzML"));
        while (reader.hasNext()) {
            reader.next();
        }
        reader.close();
        long t2 = System.currentTimeMillis();
        System.out.println((t2 - t1) / 1000);
        reader = new MzMLReader(new File("Z:\\MaoJiawei\\dataset-2\\mzML_ms_convert\\b1906_293T_proteinID_01A_QE3_122212.mzML"), 81920);
        while (reader.hasNext()) {
            reader.next();
        }
        reader.close();

        long t3 = System.currentTimeMillis();
        System.out.println((t3 - t2) / 1000);
    }

    @Test
    public void testParse() throws IOException {
        MzMLReader reader = new MzMLReader(new File("Z:\\MaoJiawei\\dataset-2\\mzML_ms_convert\\b1906_293T_proteinID_01A_QE3_122212.mzML"));
        while (reader.hasNext()) {
            reader.next();
        }
        reader.close();
    }

    @Test
    public void testPd_no() throws URISyntaxException, IOException {
        MzMLReader reader = new MzMLReader(new File(FileUtils.getResource("pd64-no-compression.mzML").toURI()));
        MSHeader header = reader.getHeader();

        FileDescription fileDescription = header.getFileDescription();
        ParamGroup fileContent = fileDescription.getFileContent();
        assertEquals(fileContent.getCvParams().size(), 2);
        assertTrue(fileContent.contains(FileDescription.MS1_Spectrum));
        assertTrue(fileContent.contains(FileDescription.MSn_Spectrum));

        MsnSpectrum spectrum = reader.next().asMsnSpectrum();
        assertEquals(spectrum.size(), 328);

        spectrum = reader.next().asMsnSpectrum();
        assertEquals(spectrum.size(), 31);
        ArrayAsserts.assertArrayEquals(spectrum.getXs(), new double[]{
                101.0238495,
                102.0272293,
                119.0341644,
                133.9824524,
                167.9581146,
                185.9686279,
                190.0390015,
                198.9174652,
                204.7245483,
                217.9444427,
                225.9540253,
                229.0383911,
                243.9645386,
                246.9547577,
                247.0490112,
                247.9355774,
                254.9072723,
                255.0538177,
                260.0094604,
                272.9231262,
                273.0647278,
                289.9294128,
                290.9450073,
                308.9552002,
                327.0780334,
                343.9970703,
                344.2943115,
                345.9762573,
                345.9943542,
                362.0245667,
                388.8437195
        }, 1e-6);
        ArrayAsserts.assertArrayEquals(spectrum.getYs(), new double[]{
                1372.4754638672,
                363.2172546387,
                433.4738769531,
                486.1702270508,
                529.5661621094,
                1204.6606445313,
                397.2258605957,
                2914.6850585938,
                535.3837280273,
                1071.6876220703,
                609.0163574219,
                1334.2375488281,
                475.274230957,
                1029.0021972656,
                1108.2054443359,
                387.0539245605,
                397.6927490234,
                523.6914672852,
                355.1064147949,
                326.399017334,
                1856.1280517578,
                536.0440063477,
                494.6879272461,
                5097.9379882813,
                372.2926635742,
                1332.8520507813,
                2010.287109375,
                1646.9571533203,
                356.6064147949,
                4817.69921875,
                298.3062744141
        }, 1e-6);

        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 343.919128417969, 1e-6);
        assertEquals(precursor.getIntensity(), 35441.046875, 1e-6);
        assertEquals(precursor.getCharge(), 2);

        reader.close();
    }

    @Test
    public void testMsconvert_zlib_numpress() throws IOException, URISyntaxException {
        test("msconvert-numpress-zlib.mzML");
    }

    @Test
    public void testMsConvert_zlib() throws IOException, URISyntaxException {
        test("msconvert-zlib-64.mzML");
    }

    public void test(String name) throws URISyntaxException, IOException {
        MzMLReader reader = new MzMLReader(new File(FileUtils.getResource(name).toURI()));
        MSHeader header = reader.getHeader();

        // cvList
        List<Cv> cvList = header.getCvList();
        assertEquals(cvList.size(), 2);
        assertEquals(cvList.get(0).getFullName(), "Proteomics Standards Initiative Mass Spectrometry Ontology");
        assertEquals(cvList.get(1).getFullName(), "Unit Ontology");

        // fileDescription
        FileDescription fileDescription = header.getFileDescription();
        List<CvParam> fileContents = fileDescription.getFileContent().getCvParams();
        assertEquals(fileContents.size(), 2);
        assertEquals(fileContents.get(0).getCvTerm().getAccession(), "MS:1000579");
        assertEquals(fileContents.get(1).getCvTerm().getAccession(), "MS:1000580");

        List<SourceFile> sourceFileList = fileDescription.getSourceFileList();
        assertEquals(sourceFileList.size(), 1);
        SourceFile sourceFile = sourceFileList.get(0);
        assertEquals(sourceFile.getId(), "RAW1");
        assertEquals(sourceFile.getName(), "20180201_Hela_Transglyco_2.raw");
        assertEquals(sourceFile.getLocation(), "file:///D:\\");
        List<CvParam> cvParams = sourceFile.getCvParams();
        assertEquals(cvParams.size(), 3);
        assertEquals(cvParams.get(0).getCvTerm().getName(), "Thermo nativeID format");
        assertEquals(cvParams.get(1).getCvTerm().getName(), "Thermo RAW format");
        assertEquals(cvParams.get(2).getCvTerm().getName(), "SHA-1");

        List<ReferenceableParamGroup> groupList = header.getReferenceableParamGroupList();
        assertEquals(groupList.size(), 1);
        ReferenceableParamGroup referenceableParamGroup = groupList.get(0);
        assertEquals(referenceableParamGroup.getId(), "CommonInstrumentParams");
        List<CvParam> cvParamList = referenceableParamGroup.getCvParamList();
        assertEquals(cvParamList.get(0).getCvTerm().getName(), "Q Exactive");
        assertEquals(cvParamList.get(1).getCvTerm().getName(), "instrument serial number");

        List<Software> softwareList = header.getSoftwareList();
        assertEquals(softwareList.size(), 2);
        assertEquals(softwareList.get(0).getID(), "Xcalibur");
        assertEquals(softwareList.get(1).getID(), "pwiz");

        List<InstrumentConfiguration> icList = header.getInstrumentConfigurationList();
        assertEquals(icList.size(), 1);
        assertEquals(icList.get(0).getSoftwareRef(), "Xcalibur");
        assertEquals(icList.get(0).getComponentList().size(), 4);

        List<DataProcessing> dataProcessingList = header.getDataProcessingList();
        assertEquals(dataProcessingList.size(), 1);
        DataProcessing dataProcessing = dataProcessingList.get(0);
        assertEquals(dataProcessing.getId(), "pwiz_Reader_Thermo_conversion");
        List<ProcessingMethod> processingMethodList = dataProcessing.getProcessingMethodList();
        assertEquals(processingMethodList.size(), 2);
        assertEquals(processingMethodList.get(0).getSoftwareRef(), "pwiz");
        assertEquals(processingMethodList.get(1).getSoftwareRef(), "pwiz");

        assertEquals(header.getRunId(), "_x0032_0180201_Hela_Transglyco_2");

        assertEquals(header.getScanCount(), 2);
        MsnSpectrum spectrum = reader.next().asMsnSpectrum();
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
        assertEquals(spectrumID.getTitle(), "20180201_Hela_Transglyco_2.41.41. File:\"20180201_Hela_Transglyco_2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=41\"");
        assertEquals(spectrum.getMsLevel(), 1);
        assertEquals(spectrumID.getPolarity(), Polarity.POSITIVE);
        assertEquals(spectrum.getSpectrumType(), SpectrumType.CENTROIDED);
        assertEquals(spectrumID.getRetentionTime().getTime(), 613.4, 0.1);
        assertEquals(spectrum.getPMString(CvTermList.ACC_SCAN_FILTER_STRING), "FTMS + p NSI Full ms [350.0000-1800.0000]");
        assertEquals(spectrum.getPMString(CvTermList.ACC_PRESET_SCAN_CONFIGURATION), "1");
        assertEquals(spectrumID.getIonInjectTime(), 25, 0.1);

        MsnSpectrum msnSpectrum = reader.next().asMsnSpectrum();
        assertEquals(msnSpectrum.getMsLevel(), 2);
        assertEquals(spectrumID.getScanNumber().getValue(), 42);
        assertEquals(spectrumID.getScanLowerMz(), 200.0, 0.1);
        assertEquals(spectrumID.getScanUpperMz(), 1490.0, 0.1);
        assertEquals(spectrumID.getIsolateWindowTargetMz(), 719.7650, 0.0001);
        Peak precursor = msnSpectrum.getPrecursor();
        assertEquals(precursor.getMz(), 719.7650, 0.0001);
        assertEquals(precursor.getCharge(), 2);
        assertEquals(precursor.getIntensity(), 32920, 1);
        assertEquals(spectrumID.getCollisionEnergy(), 25.0, 0.1);

        assertEquals(msnSpectrum.size(), 9);

        ArrayAsserts.assertArrayEquals(msnSpectrum.getXs(),
                new double[]{204.08813,
                        229.98349,
                        263.77658,
                        263.94171,
                        301.45764,
                        366.14010,
                        380.42077,
                        397.95974,
                        1261.0301
                }, 0.0001);
        ArrayAsserts.assertArrayEquals(msnSpectrum.getYs(),
                new double[]{
                        10289.7675781,
                        1739.106567,
                        1963.93017578,
                        1716.81335449,
                        2246.13720703,
                        12501.3525390,
                        2003.52038574,
                        2097.08618164,
                        2702.60083007,

                }, 0.0001);
    }

    @Test
    public void testMsconvert_zlib_64() throws URISyntaxException, IOException {
        MzMLReader reader = new MzMLReader(new File(FileUtils.getResource("msconvert-zlib-64.mzML").toURI()));
        reader.acceptUnsortedSpectra();
        MSHeader header = reader.getHeader();

        // cvList
        List<Cv> cvList = header.getCvList();
        assertEquals(cvList.size(), 2);
        assertEquals(cvList.get(0).getFullName(), "Proteomics Standards Initiative Mass Spectrometry Ontology");
        assertEquals(cvList.get(1).getFullName(), "Unit Ontology");

        // fileDescription
        FileDescription fileDescription = header.getFileDescription();
        List<CvParam> fileContents = fileDescription.getFileContent().getCvParams();
        assertEquals(fileContents.size(), 2);
        assertEquals(fileContents.get(0).getCvTerm().getAccession(), "MS:1000579");
        assertEquals(fileContents.get(1).getCvTerm().getAccession(), "MS:1000580");

        List<SourceFile> sourceFileList = fileDescription.getSourceFileList();
        assertEquals(sourceFileList.size(), 1);
        SourceFile sourceFile = sourceFileList.get(0);
        assertEquals(sourceFile.getId(), "RAW1");
        assertEquals(sourceFile.getName(), "20180201_Hela_Transglyco_2.raw");
        assertEquals(sourceFile.getLocation(), "file:///D:\\");
        List<CvParam> cvParams = sourceFile.getCvParams();
        assertEquals(cvParams.size(), 3);
        assertEquals(cvParams.get(0).getCvTerm().getName(), "Thermo nativeID format");
        assertEquals(cvParams.get(1).getCvTerm().getName(), "Thermo RAW format");
        assertEquals(cvParams.get(2).getCvTerm().getName(), "SHA-1");

        List<ReferenceableParamGroup> groupList = header.getReferenceableParamGroupList();
        assertEquals(groupList.size(), 1);
        ReferenceableParamGroup referenceableParamGroup = groupList.get(0);
        assertEquals(referenceableParamGroup.getId(), "CommonInstrumentParams");
        List<CvParam> cvParamList = referenceableParamGroup.getCvParamList();
        assertEquals(cvParamList.get(0).getCvTerm().getName(), "Q Exactive");
        assertEquals(cvParamList.get(1).getCvTerm().getName(), "instrument serial number");

        List<Software> softwareList = header.getSoftwareList();
        assertEquals(softwareList.size(), 2);
        assertEquals(softwareList.get(0).getID(), "Xcalibur");
        assertEquals(softwareList.get(1).getID(), "pwiz");

        List<InstrumentConfiguration> icList = header.getInstrumentConfigurationList();
        assertEquals(icList.size(), 1);
        assertEquals(icList.get(0).getSoftwareRef(), "Xcalibur");
        assertEquals(icList.get(0).getComponentList().size(), 4);

        List<DataProcessing> dataProcessingList = header.getDataProcessingList();
        assertEquals(dataProcessingList.size(), 1);
        DataProcessing dataProcessing = dataProcessingList.get(0);
        assertEquals(dataProcessing.getId(), "pwiz_Reader_Thermo_conversion");
        List<ProcessingMethod> processingMethodList = dataProcessing.getProcessingMethodList();
        assertEquals(processingMethodList.size(), 2);
        assertEquals(processingMethodList.get(0).getSoftwareRef(), "pwiz");
        assertEquals(processingMethodList.get(1).getSoftwareRef(), "pwiz");

        assertEquals(header.getRunId(), "_x0032_0180201_Hela_Transglyco_2");

        assertEquals(header.getScanCount(), 2);
        MsnSpectrum spectrum = reader.next().asMsnSpectrum();
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();

        assertEquals(spectrumID.getTitle(), "20180201_Hela_Transglyco_2.41.41. File:\"20180201_Hela_Transglyco_2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=41\"");
        assertEquals(spectrum.getMsLevel(), 1);
        assertEquals(spectrumID.getPolarity(), Polarity.POSITIVE);
        assertEquals(spectrum.getSpectrumType(), SpectrumType.CENTROIDED);
        assertEquals(spectrumID.getRetentionTime().getTime(), 613.4, 0.1);
        assertEquals(spectrum.getPMString(CvTermList.ACC_SCAN_FILTER_STRING), "FTMS + p NSI Full ms [350.0000-1800.0000]");
        assertEquals(spectrum.getPMString(CvTermList.ACC_PRESET_SCAN_CONFIGURATION), "1");
        assertEquals(spectrumID.getIonInjectTime(), 25, 0.1);

        MsnSpectrum msnSpectrum = reader.next().asMsnSpectrum();
        assertEquals(msnSpectrum.getMsLevel(), 2);
        assertEquals(spectrumID.getScanNumber().getValue(), 42);
        assertEquals(spectrumID.getScanLowerMz(), 200.0, 0.1);
        assertEquals(spectrumID.getScanUpperMz(), 1490.0, 0.1);
        assertEquals(spectrumID.getIsolateWindowTargetMz(), 719.7650, 0.0001);
        Peak precursor = msnSpectrum.getPrecursor();
        assertEquals(precursor.getMz(), 719.7650, 0.0001);
        assertEquals(precursor.getCharge(), 2);
        assertEquals(precursor.getIntensity(), 32920, 1);
        assertEquals(spectrumID.getCollisionEnergy(), 25.0, 0.1);

        assertEquals(msnSpectrum.size(), 9);

        ArrayAsserts.assertArrayEquals(msnSpectrum.getXs(),
                new double[]{204.08813,
                        229.98349,
                        263.77658,
                        263.94171,
                        301.45764,
                        366.14010,
                        380.42077,
                        397.95974,
                        1261.0301
                }, 0.0001);
        ArrayAsserts.assertArrayEquals(msnSpectrum.getYs(),
                new double[]{
                        10289.7675781,
                        1739.106567,
                        1963.93017578,
                        1716.81335449,
                        2246.13720703,
                        12501.3525390,
                        2003.52038574,
                        2097.08618164,
                        2702.60083007,

                }, 0.0001);

    }

    public static void main(String[] args) throws IOException {
        double eps = 0.0001;

        File mgf = new File("I:\\test\\rawdata\\20110610_Q2_TaGe_SA_SSM1_2.mgf");
        File mzML = new File("I:\\test\\rawdata\\20110610_Q2_TaGe_SA_SSM1_2.mzML");

        MzMLReader reader = new MzMLReader(mzML);
        reader.acceptUnsortedSpectra();
        MgfReader mgfReader = new MgfReader(mgf);
        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
            if (spectrum instanceof MsnSpectrum) {
                MsnSpectrum msnSpectrum = spectrum.asMsnSpectrum();
                if (msnSpectrum.getMsLevel() == 2) {
                    MsnSpectrum mgfSpectrum = mgfReader.next();

                    assertEquals(msnSpectrum.getPrecursor().getCharge(), mgfSpectrum.getPrecursor().getCharge(), eps);
                    assertEquals(msnSpectrum.getPrecursor().getMz(), mgfSpectrum.getPrecursor().getMz(), eps);
                    assertEquals(msnSpectrum.size(), mgfSpectrum.size());

                    double[] mzValues = msnSpectrum.getXs();
                    double[] inValues = msnSpectrum.getYs();
                    double[] mzValues1 = mgfSpectrum.getXs();
                    double[] inValues1 = mgfSpectrum.getYs();
                    for (int k = 0; k < mzValues.length; k++) {
                        assertEquals(mzValues[k], mzValues1[k], eps);
                        assertEquals(inValues[k], inValues1[k], 0.1);
                    }
                }

            }
        }

        reader.close();
        mgfReader.close();
    }
}
