/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSDataFile;
import omics.msdk.model.MSHeader;
import omics.util.ms.Dissociation;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Polarity;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.util.MzMLSpectrumID;
import org.testng.annotations.Test;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;


/**
 * @author JiaweiMao 2017.04.17
 * @since 1.0-SNAPSHOT
 */
public class MzMLAccessorTest {
    private double eps = 0.0001;

    @Test
    public void testPd2_1() throws IOException {
        MzMLReader reader = new MzMLReader(new File("Z:\\MaoJiawei\\dataset-2\\mzML_deisotoped_pd\\b1945_293T_proteinID_09B_QE3_122212.mzML"));
        MSHeader header = reader.getHeader();
//        while (reader.hasNext()){
//            Spectrum<PeakAnnotation> spectrum = reader.next();
//            System.out.println(spectrum.getMsLevel());
//        }

        reader.close();
    }

    @Test
    public void pd() throws Exception {
        File file = new File("I:\\methyl\\20151217\\spec_data\\mzML\\hM3_HEK293_11_20151205_NF14.mzML");
        MzMLAccessor task = new MzMLAccessor(file);
        task.start();

        MSDataFile rawDataFile = task.getValue();
        List<MsnSpectrum> scans = rawDataFile.getSpectrumList();
        assertEquals(45884, scans.size());

        for (MsnSpectrum msScan : scans) {

            MzMLSpectrumID spectrumID = (MzMLSpectrumID) msScan.getSpectrumID();
            // ms1 test
            if (spectrumID.getScanNumber().getValue() == 6053) {
                double rt = msScan.getSpectrumID().getRetentionTime().getTime();
                assertEquals(47.82, rt / 60, 0.1);

                assertEquals(300.0, spectrumID.getScanLowerMz(), 0.01);
                assertEquals(1750.0, spectrumID.getScanUpperMz(), 0.01);

                assertEquals(Polarity.POSITIVE, spectrumID.getPolarity());
                assertEquals(298, msScan.size());
            }

            if (spectrumID.getScanNumber().getValue() == 6054) {

                double rt = spectrumID.getRetentionTime().getTime();
                assertEquals(47.83, rt / 60, 0.1);

                assertEquals(100.0, spectrumID.getScanLowerMz(), 0.01);
                assertEquals(645.0, spectrumID.getScanUpperMz(), 0.01);

                Float ionInjectTime = spectrumID.getIonInjectTime();
                assertEquals(240.0, ionInjectTime, 0.1);

                int precursorScan = spectrumID.getParentScanNumber().getValue();
                assertEquals(6053, precursorScan);

                Peak precursor = msScan.getPrecursor();
                assertEquals(308.170959472656, precursor.getMz(), eps);
                assertEquals(2, precursor.getCharge());
                assertEquals(4343872, precursor.getIntensity(), 1);


                assertEquals(27.0, spectrumID.getCollisionEnergy(), 0.1);
                assertEquals(Dissociation.HCD, spectrumID.getDissociation());
                break;
            }
        }

        rawDataFile.clear();
    }

    @Test
    void test() throws IOException, XMLStreamException {
        File mzML = new File("I:\\methyl_used\\20151217\\spec_data\\mzML\\hM3_HEK293_11_20151205_CF1.mzML");

        MzMLAccessor importMethod = new MzMLAccessor(mzML);
        importMethod.start();

        MSDataFile rawDataFile = importMethod.getValue();
    }

    /**
     * Test the peak value parse
     */
    @Test
    void testpd2() throws IOException, XMLStreamException {
        // A rough test of all peak values, to ensure that the binary parser is right.
        // The mgf is directed generated from the mzML file with ProteoWizard.

        File mgf = new File("I:\\test\\rawdata\\20110610_Q2_TaGe_SA_SSM1_2.mgf");
        File mzML = new File("I:\\test\\rawdata\\20110610_Q2_TaGe_SA_SSM1_2.mzML");

        MzMLAccessor importMethod = new MzMLAccessor(mzML);
        importMethod.start();

        MSDataFile rawDataFile = importMethod.getValue();

        List<MsnSpectrum> ms2Scans = new ArrayList<>();
        for (MsnSpectrum scan : rawDataFile.getSpectrumList()) {
            if (scan.getMsLevel() == 2) {
                ms2Scans.add(scan);
            }
        }

        MgfAccessor mgfImportMethod = new MgfAccessor(mgf);
        mgfImportMethod.start();
        MSDataFile dataFile = mgfImportMethod.getValue();

        assert dataFile != null;

        List<MsnSpectrum> scanList = dataFile.getSpectrumList();
        for (int i = 0; i < ms2Scans.size(); i++) {
            MsnSpectrum scan = scanList.get(i);
            MsnSpectrum scan1 = ms2Scans.get(i);

            System.out.println("scan=" + scan.getSpectrumID().getScanNumber().getValue());

            assertEquals(scan.getPrecursor().getCharge(), scan1.getPrecursor().getCharge(), eps);
            assertEquals(scan.getPrecursor().getMz(), scan1.getPrecursor().getMz(), eps);
            assertEquals(scan.size(), scan1.size());

            double[] mzValues = scan.getXs();
            double[] inValues = scan.getYs();
            double[] mzValues1 = scan1.getXs();
            double[] inValues1 = scan1.getYs();
            for (int k = 0; k < mzValues.length; k++) {
                assertEquals(mzValues[k], mzValues1[k], eps);
                assertEquals(inValues[k], inValues1[k], 0.1);
            }
        }

        rawDataFile.clear();
        dataFile.clear();
    }
}