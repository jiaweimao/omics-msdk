/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSDataFile;
import omics.util.ms.MsnSpectrum;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Oct 2018, 8:39 PM
 */
public class DoExport2MzMLTest {
    @Test
    public void testMgf2MzML() throws IOException, XMLStreamException {
        MgfAccessor access = new MgfAccessor(new File("Z:\\MaoJiawei\\test\\MS\\hM3_HEK293_11_20151205_NF5.mgf"));
        access.start();
        MSDataFile value = access.getValue();

        DoExport2MzML task = new DoExport2MzML(value, new File("Z:\\MaoJiawei\\test\\MS\\hM3_HEK293_11_20151205_NF5_test.mzML"));
        task.start();
    }

    @Test
    public void check() throws IOException {
        MgfReader reader = new MgfReader(new File("Z:\\MaoJiawei\\test\\MS\\hM3_HEK293_11_20151205_NF5.mgf"));
        MzMLReader mzMLReader = new MzMLReader(new File("Z:\\MaoJiawei\\test\\MS\\hM3_HEK293_11_20151205_NF5_test.mzML"));
        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            MsnSpectrum mzMLSpec = mzMLReader.next().asMsnSpectrum();

            check(spectrum, mzMLSpec);
        }
        reader.close();
        mzMLReader.close();
    }


    private void check(MsnSpectrum spec1, MsnSpectrum spec2) {
        assertEquals(spec1.size(), spec2.size());
        for (int i = 0; i < spec1.size(); i++) {
            Assert.assertEquals(spec1.getMz(i), spec2.getMz(i), 0.01);
            Assert.assertEquals(spec1.getIntensity(i), spec2.getIntensity(i), 0.01);
        }
    }
}