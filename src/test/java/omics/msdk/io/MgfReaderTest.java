/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Peak;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringJoiner;

import static org.testng.Assert.assertEquals;


/**
 * @author JiaweiMao 2017-04-26
 * @since 1.0-SNAPSHOT
 */
public class MgfReaderTest
{
    @Test
    public void testId() throws IOException
    {
        MgfReader reader = new MgfReader(new File("Z:\\MaoJiawei\\test\\omics_msgf\\F8_29498.mgf"));
//        int scan = 69589;
        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            System.out.println(spectrum.getTitle());
//            if (spectrum.getScanNumber().getValue() == scan) {
//                System.out.println(toString(spectrum));
//                break;
//            }
        }

        reader.close();
    }

    private String toString(MsnSpectrum spectrum)
    {
        StringJoiner joiner = new StringJoiner(";");
        for (int i = 0; i < spectrum.size(); i++) {
            joiner.add(String.valueOf(spectrum.getMz(i)));
            joiner.add(String.valueOf(spectrum.getIntensity(i)));
        }
        return joiner.toString();
    }

    @Test
    void titleParser() throws IOException
    {
        MgfReader reader1 = new MgfReader(new File("I:\\test_data\\complementary\\pxd002619_1\\To mgf\\20110610_Q2_TaGe_SA_SSM1_2.mgf"));
        MgfReader reader2 = new MgfReader(new File("I:\\test_data\\complementary\\pxd002619_1\\20110610_Q2_TaGe_SA_SSM1_2.mgf"));

        while (reader1.hasNext() && reader2.hasNext()) {

            MsnSpectrum spectrum1 = reader1.next();
            MsnSpectrum spectrum2 = reader2.next();

            System.out.println(spectrum1.getScanNumberList());
            assertEquals(spectrum1.size(), spectrum2.size());
            assertEquals(spectrum1.getPrecursor().getMz(), spectrum2.getPrecursor().getMz(), 0.01);
            assertEquals(spectrum1.getScanNumberList().getFirst(), spectrum2.getScanNumberList().getFirst());

            for (int i = 0; i < spectrum1.size(); i++) {
                assertEquals(spectrum1.getMz(i), spectrum2.getMz(i), 0.0001);
                assertEquals(spectrum1.getIntensity(i), spectrum2.getIntensity(i), 0.01);
            }
        }

        reader1.close();
        reader2.close();

    }

    @Test
    public void testReading() throws IOException
    {
        MgfReader reader = new MgfReader(new File(FileUtils.getResource("test.mgf").getFile()));
        reader.acceptUnsortedSpectra();
        int count = 0;
        while (reader.hasNext()) {
            reader.next();
            count++;
        }
        reader.close();
        assertEquals(count, 10);
    }

    @Test
    public void testmsconvert() throws URISyntaxException, IOException
    {
        URI uri = FileUtils.getResource("msconvert-zlib-64.mgf").toURI();
        MgfReader reader = new MgfReader(new File(uri));
        MsnSpectrum spectrum = reader.next();
        assertEquals(spectrum.getTitle(), "20180201_Hela_Transglyco_2.42.42.2 File:\"20180201_Hela_Transglyco_2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=42\"");
        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 719.765017956865, 1e-5);
        assertEquals(precursor.getIntensity(), 32920.0078125, 1e-5);
        assertEquals(precursor.getCharge(), 2);
        assertEquals(spectrum.getSpectrumID().getRetentionTime().getTime(), 613.7121, 1e-3);
        assertEquals(spectrum.getSpectrumID().getScanNumber().getValue(), 42);
        assertEquals(spectrum.size(), 9);
        assertEquals(spectrum.getMz(0), 204.0881348, 1e-5);
        assertEquals(spectrum.getMz(spectrum.size() - 1), 1261.030151, 1e-5);

        reader.close();

    }

    @Test
    public void testPD() throws URISyntaxException, IOException
    {
        URI uri = FileUtils.getResource("pd-2.mgf").toURI();
        MgfReader reader = new MgfReader(new File(uri));
        MsnSpectrum spectrum = reader.next();
        assertEquals(spectrum.getTitle(), "File: \"G:\\test_data\\complementary\\pxd002619_1\\20110610_Q2_TaGe_SA_SSM1_2.raw\"; SpectrumID: \"1\"; scans: \"65\"");
        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 352.73691, 1e-5);
        assertEquals(precursor.getIntensity(), 18726.01563, 1e-5);
        assertEquals(precursor.getCharge(), 3);
        assertEquals(spectrum.getSpectrumID().getRetentionTime().getTime(), 17, 0.1);
        assertEquals(spectrum.getSpectrumID().getScanNumber().getValue(), 65);
        assertEquals(spectrum.size(), 70);
        assertEquals(spectrum.getMz(0), 101.05989, 1e-5);
        assertEquals(spectrum.getMz(spectrum.size() - 1), 656.43652, 1e-5);

        spectrum = reader.next();
        assertEquals(spectrum.getTitle(), "File: \"G:\\test_data\\complementary\\pxd002619_1\\20110610_Q2_TaGe_SA_SSM1_2.raw\"; SpectrumID: \"2\"; scans: \"297\"");
        precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 352.73712, 1e-5);
        assertEquals(precursor.getIntensity(), 43014.02734, 1e-5);
        assertEquals(precursor.getCharge(), 3);
        assertEquals(spectrum.getSpectrumID().getRetentionTime().getTime(), 79, 0.1);
        assertEquals(spectrum.getSpectrumID().getScanNumber().getValue(), 297);
        assertEquals(spectrum.size(), 55);
        assertEquals(spectrum.getMz(0), 100.07569, 1e-5);
        assertEquals(spectrum.getMz(spectrum.size() - 1), 657.52631, 1e-5);
        reader.close();
    }
}