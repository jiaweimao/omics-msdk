/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSHeader;
import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Peak;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Sep 2018, 9:04 AM
 */
public class MzXMLReaderTest
{

    @Test
    public void testWriter() throws IOException, XMLStreamException
    {
        File file = new File("test.mzXML");

                //File.createTempFile("rwdw", "mzXML");
        File toRead = new File(FileUtils.getResource("redw.mzXML").getFile());

        MzXMLWriter writer = new MzXMLWriter(file);
        writer.writeStartDocument();

        MzXMLReader reader = new MzXMLReader(toRead);
        writer.writeFileHeader(reader.getHeader());

        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            writer.writeSpectrum(spectrum);
        }
        writer.writeEndDocument();

        writer.close();
        reader.close();

        test(file);

        file.deleteOnExit();
    }

    @Test
    public void testReader() throws IOException
    {
        File file = new File(FileUtils.getResource("redw.mzXML").getFile());
        test(file);
    }

    public void test(File file) throws IOException
    {
        MzXMLReader reader = new MzXMLReader(file);
        MSHeader header = reader.getHeader();
        assertEquals(header.getScanCount(), 67599);

        reader.next();
        MsnSpectrum spectrum = reader.next();
        ArrayAsserts.assertArrayEquals(spectrum.getXs(), new double[]{
                110.0719528,
                112.0871887,
                112.0937119,
                113.0716171,
                114.0664978,
                115.0507736,
                115.0868225,
                116.0709152,
                127.0508194,
                129.1021271,
                130.0975189,
                136.0622406,
                141.0653687,
                141.1027222,
                151.8694611,
                152.0822144,
                155.0819244,
                157.0974731,
                157.1092224,
                158.0923615,
                158.1043549,
                167.0381012,
                167.0817719,
                175.1190643,
                175.1347961,
                176.1222534,
                182.0924988,
                184.1080017,
                185.0919952,
                197.1027527,
                202.1183472,
                212.0518646,
                221.9610596,
                227.1138153,
                228.1184998,
                229.1296844,
                239.9722443,
                241.0939331,
                246.1549835,
                249.0988312,
                251.1497192,
                255.1466522,
                256.1470642,
                272.1704712,
                275.9927368,
                277.0014648,
                284.1340027,
                303.1777039,
                304.0619202,
                329.1922607,
                331.1593933,
                340.8202209,
                343.1721497,
                360.1987,
                361.2023621,
                367.5194092,
                370.1822205,
                386.2163696,
                396.5492859,
                396.832428,
                396.8847961,
                397.2171936,
                430.2278137,
                440.2265625,
                443.236969,
                450.7250671,
                457.2489624,
                457.7528076,
                458.7380371,
                459.2406616,
                459.738739,
                471.7495422,
                472.256134,
                493.2670898,
                493.7690735,
                499.2719421,
                500.2582397,
                507.2631836,
                507.7659302,
                515.77771,
                516.2698364,
                516.7736816,
                557.2773438,
                558.2838745,
                563.2822266,
                571.7921753,
                572.2937012,
                572.7945557,
                586.3069458,
                616.3140259,
                643.3195801,
                644.3145752,
                645.3153687,
                661.336792,
                671.3511963,
                672.3563843,
                696.7172241,
                714.3579712,
                728.3731689,
                757.3745728,
                758.3936768,
                760.374939,
                829.388855,
                856.4343262,
                871.4084473,
                872.4191895,
                881.4054565,
                899.4445801,
                940.2848511,
                940.4700317,
                941.4707642,
                968.4702759,
                969.4612427,
                986.4779053,
                987.4860229,
                1003.496826,
                1097.511108,
                1098.519409,
                1099.523071
        }, 1e-4);

        assertEquals(spectrum.getSpectrumID().getRetentionTime().getTime(), 1588.3, 0.1);
        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 396.54883328, 1e-4);
        assertEquals(precursor.getIntensity(), 3144960.0, 0.1);
        assertEquals(precursor.getCharge(), 3);
        reader.close();
    }
}