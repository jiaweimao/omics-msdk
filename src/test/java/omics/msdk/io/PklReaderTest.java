/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Peak;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Nov 2018, 4:43 PM
 */
public class PklReaderTest
{

    @Test
    public void testFile() throws IOException
    {
        URL testFile = FileUtils.getResource("testfile.pkl");

        List<MsnSpectrum> spectrums = new ArrayList<>();
        PklReader reader = new PklReader(new File(testFile.getFile()));
        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            spectrums.add(spectrum);
        }
        reader.close();

        assertEquals(spectrums.size(), 3);

        MsnSpectrum spectrum = spectrums.get(1);
        assertNotNull(spectrum);

        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getIntensity(), 11370039, 0.1);
        assertEquals(precursor.getMz(), 940.12, 0.01);
        assertEquals(precursor.getCharge(), 2);

        assertEquals(spectrum.size(), 491);
        assertEquals(spectrum.getMz(1), 212.87, 0.01);
        assertEquals(spectrum.getIntensity(1), 533.4, 0.01);
    }

    @Test
    public void testDir() throws IOException
    {
        URL pkl_dir = FileUtils.getResource("pkl_dir");
        assertNotNull(pkl_dir);

        List<MsnSpectrum> spectrumList = new ArrayList<>();
        PklReader reader = new PklReader(new File(pkl_dir.getFile()));
        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            spectrumList.add(spectrum);
        }
        reader.close();

        assertEquals(spectrumList.size(), 16);

        MsnSpectrum spectrum = spectrumList.get(11);
        assertNotNull(spectrum);

        Peak precursor = spectrum.getPrecursor();
        assertEquals(precursor.getMz(), 648.20, 0.01);
        assertEquals(precursor.getIntensity(), 171079939.0, 0.1);
        assertEquals(spectrum.size(), 399);
    }

}