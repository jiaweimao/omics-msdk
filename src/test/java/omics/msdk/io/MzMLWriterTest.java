/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.CompressionType;
import omics.msdk.io.util.CvTermList;
import omics.msdk.io.util.ReferenceableParamGroup;
import omics.msdk.model.*;
import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.io.FileUtils;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Polarity;
import omics.util.ms.Spectrum;
import omics.util.ms.SpectrumType;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.util.MzMLSpectrumID;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static org.testng.Assert.assertEquals;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Sep 2018, 12:29 PM
 */
public class MzMLWriterTest {

    @Test
    public void testHeader() throws URISyntaxException, IOException, XMLStreamException {
        MzMLReader reader = new MzMLReader(new File(FileUtils.getResource("msconvert-zlib-64.mzML").toURI()));

        MzMLWriter writer = new MzMLWriter(new File("test.mzML"));
        writer.writeStartDocument();

        MSHeader header = reader.getHeader();
        System.out.println(header.getRunId());

        writer.writeFileHeader(reader.getHeader());
        writer.writeStartSpectrumList();
        boolean first = true;
        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
            if (spectrum.isMsnSpectrum()) {
                writer.writeSpectrum(spectrum.asMsnSpectrum());
            } else if (spectrum.isChromatogram()) {
                if (first) {
                    writer.writeEndSpectrumList();
                    writer.writeStartChromatogramList();
                    first = false;
                }
                writer.writeChromatogram(spectrum.asChromatogram());
            }
        }
        writer.writeEndChromatogramList();
        writer.writeEndDocument();
        reader.close();
        writer.close();
    }

    @Test
    public void testNumpress() throws URISyntaxException, IOException, XMLStreamException {
        MzMLAccessor inTask = new MzMLAccessor(new File(FileUtils.getResource("msconvert-numpress-zlib.mzML").toURI()));
        inTask.start();
        MSDataFile value = inTask.getValue();

        File out = new File("temp.mzML");
        DoExport2MzML outTask = new DoExport2MzML(value, out, CompressionType.NUMPRESS_LINPRED_ZLIB, CompressionType.ZLIB);
        outTask.start();
    }

    @Test
    public void testMsconvert_zlib_64() throws IOException {
        MzMLAccessor inTask = new MzMLAccessor(new File("D:\\code\\omics\\omics-msdk\\test.mzML"));
        inTask.start();
        MSDataFile rawDataFile = inTask.getValue();

        MSHeader header = rawDataFile.getHeader();

        // cvList
        List<Cv> cvList = header.getCvList();
        assertEquals(cvList.size(), 2);
        assertEquals(cvList.get(0).getFullName(), "Proteomics Standards Initiative Mass Spectrometry Ontology");
        assertEquals(cvList.get(1).getFullName(), "Unit Ontology");

        // fileDescription
        FileDescription fileDescription = header.getFileDescription();
        List<CvParam> fileContents = fileDescription.getFileContent().getCvParams();
        assertEquals(fileContents.size(), 2);
        assertEquals(fileContents.get(0).getCvTerm().getAccession(), "MS:1000579");
        assertEquals(fileContents.get(1).getCvTerm().getAccession(), "MS:1000580");

        List<SourceFile> sourceFileList = fileDescription.getSourceFileList();
        assertEquals(sourceFileList.size(), 1);
        SourceFile sourceFile = sourceFileList.get(0);
        assertEquals(sourceFile.getId(), "RAW1");
        assertEquals(sourceFile.getName(), "20180201_Hela_Transglyco_2.raw");
        assertEquals(sourceFile.getLocation(), "file:///D:\\");
        List<CvParam> cvParams = sourceFile.getCvParams();
        assertEquals(cvParams.size(), 3);
        assertEquals(cvParams.get(0).getCvTerm().getName(), "Thermo nativeID format");
        assertEquals(cvParams.get(1).getCvTerm().getName(), "Thermo RAW format");
        assertEquals(cvParams.get(2).getCvTerm().getName(), "SHA-1");

        List<ReferenceableParamGroup> groupList = header.getReferenceableParamGroupList();
        assertEquals(groupList.size(), 1);
        ReferenceableParamGroup referenceableParamGroup = groupList.get(0);
        assertEquals(referenceableParamGroup.getId(), "CommonInstrumentParams");
        List<CvParam> cvParamList = referenceableParamGroup.getCvParamList();
        assertEquals(cvParamList.get(0).getCvTerm().getName(), "Q Exactive");
        assertEquals(cvParamList.get(1).getCvTerm().getName(), "instrument serial number");

        List<Software> softwareList = header.getSoftwareList();
        assertEquals(softwareList.size(), 2);
        assertEquals(softwareList.get(0).getID(), "Xcalibur");
        assertEquals(softwareList.get(1).getID(), "pwiz");

        List<InstrumentConfiguration> icList = header.getInstrumentConfigurationList();
        assertEquals(icList.size(), 1);
        assertEquals(icList.get(0).getSoftwareRef(), "Xcalibur");
        assertEquals(icList.get(0).getComponentList().size(), 4);

        List<DataProcessing> dataProcessingList = header.getDataProcessingList();
        assertEquals(dataProcessingList.size(), 1);
        DataProcessing dataProcessing = dataProcessingList.get(0);
        assertEquals(dataProcessing.getId(), "pwiz_Reader_Thermo_conversion");
        List<ProcessingMethod> processingMethodList = dataProcessing.getProcessingMethodList();
        assertEquals(processingMethodList.size(), 2);
        assertEquals(processingMethodList.get(0).getSoftwareRef(), "pwiz");
        assertEquals(processingMethodList.get(1).getSoftwareRef(), "pwiz");

        assertEquals(header.getRunId(), "_x0032_0180201_Hela_Transglyco_2");


        List<MsnSpectrum> spectrumList = rawDataFile.getSpectrumList();
        assertEquals(spectrumList.size(), 2);
        MsnSpectrum spectrum = spectrumList.get(0);
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();

        assertEquals(spectrumID.getTitle(), "20180201_Hela_Transglyco_2.41.41. File:\"20180201_Hela_Transglyco_2.raw\", NativeID:\"controllerType=0 controllerNumber=1 scan=41\"");
        assertEquals(spectrum.getMsLevel(), 1);
        assertEquals(spectrumID.getPolarity(), Polarity.POSITIVE);
        assertEquals(spectrum.getSpectrumType(), SpectrumType.CENTROIDED);
        assertEquals(spectrumID.getRetentionTime().getTime(), 613.4, 0.1);
        assertEquals(spectrum.getPMString(CvTermList.ACC_SCAN_FILTER_STRING), "FTMS + p NSI Full ms [350.0000-1800.0000]");
        assertEquals(spectrum.getPMString(CvTermList.ACC_PRESET_SCAN_CONFIGURATION), "1");
        assertEquals(spectrumID.getIonInjectTime(), 25, 0.1);

        MsnSpectrum msnSpectrum = spectrumList.get(1);
        assertEquals(msnSpectrum.getMsLevel(), 2);
        assertEquals(spectrumID.getScanNumber().getValue(), 42);
        assertEquals(spectrumID.getScanLowerMz(), 200.0, 0.1);
        assertEquals(spectrumID.getScanUpperMz(), 1490.0, 0.1);
        assertEquals(spectrumID.getIsolateWindowTargetMz(), 719.7650, 0.0001);
        Peak precursor = msnSpectrum.getPrecursor();
        assertEquals(precursor.getMz(), 719.7650, 0.0001);
        assertEquals(precursor.getCharge(), 2);
        assertEquals(precursor.getIntensity(), 32920, 1);
        assertEquals(spectrumID.getCollisionEnergy(), 25.0, 0.1);

        assertEquals(msnSpectrum.size(), 9);

        ArrayAsserts.assertArrayEquals(msnSpectrum.getXs(),
                new double[]{204.08813,
                        229.98349,
                        263.77658,
                        263.94171,
                        301.45764,
                        366.14010,
                        380.42077,
                        397.95974,
                        1261.0301
                }, 0.0001);
        ArrayAsserts.assertArrayEquals(msnSpectrum.getYs(),
                new double[]{
                        10289.7675781,
                        1739.106567,
                        1963.93017578,
                        1716.81335449,
                        2246.13720703,
                        12501.3525390,
                        2003.52038574,
                        2097.08618164,
                        2702.60083007,

                }, 0.0001);

    }

}