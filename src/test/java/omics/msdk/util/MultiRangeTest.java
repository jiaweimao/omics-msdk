/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.util;

import it.unimi.dsi.fastutil.ints.Int2IntAVLTreeMap;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntRBTreeMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Nov 2018, 11:40 AM
 */
public class MultiRangeTest
{
    @Test
    public void testAdd()
    {
        MultiRange range = new MultiRange();
        range.add(1, 3);
        range.add(4, 5);
        range.add(6, 8);
        range.add(7, 9);
        range.add(9, 10);

        ArrayAsserts.assertArrayEquals(range.getPoints(), new double[]{1, 3, 4, 5, 6, 10}, 0.1);
    }

    @Test
    public void testTreeMap()
    {

    }
}