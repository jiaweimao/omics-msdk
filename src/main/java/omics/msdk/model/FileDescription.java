/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;

import java.util.ArrayList;
import java.util.List;

/**
 * This class describe the content of the file.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Aug 2018, 9:44 AM
 */
public class FileDescription {

    public static final CvParam MS1_Spectrum = new CvParam(new CvTerm("MS:1000579", "MS1 spectrum"), "");
    public static final CvParam MSn_Spectrum = new CvParam(new CvTerm("MS:1000580", "MSn spectrum"), "");
    public static final CvParam SIM_Spectrum = new CvParam(new CvTerm("MS:1000582", "SIM spectrum"), "");
    public static final CvParam SRM_Spectrum = new CvParam(new CvTerm("MS:1000583", "SRM spectrum"), "");

    private ParamGroup fileContent;
    private List<SourceFile> sourceFileList;
    private List<ParamGroup> contactList;

    public FileDescription() {
    }

    /**
     * for non mzML file, the file content is null.
     *
     * @return file content.
     */
    public ParamGroup getFileContent() {
        return fileContent;
    }

    public void setFileContent(ParamGroup fileContent) {
        this.fileContent = fileContent;
    }

    public List<SourceFile> getSourceFileList() {
        if (sourceFileList == null)
            sourceFileList = new ArrayList<>();
        return sourceFileList;
    }

    public void addSourceFile(SourceFile sourceFile) {
        if (sourceFileList == null)
            sourceFileList = new ArrayList<>();
        this.sourceFileList.add(sourceFile);
    }

    public void setSourceFileList(List<SourceFile> sourceFileList) {
        this.sourceFileList = sourceFileList;
    }

    public List<ParamGroup> getContactList() {
        if (contactList == null)
            contactList = new ArrayList<>();
        return contactList;
    }

    public void setContactList(List<ParamGroup> contactList) {
        this.contactList = contactList;
    }

    public void addContact(ParamGroup paramGroup) {
        if (contactList == null)
            contactList = new ArrayList<>();
        contactList.add(paramGroup);
    }
}
