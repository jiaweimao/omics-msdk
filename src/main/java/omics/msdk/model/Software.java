/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 7:09 PM
 */
public class Software extends ParamGroup {

    public static Software Xcalibur = new Software("Xcalibur", new CvParam(new CvTerm("MS:1000532", "Xcalibur"), ""));
    public static Software PWIZ = new Software("pwiz", new CvParam(new CvTerm("MS:1000615", "ProteoWizard software"), ""));
    public static Software PD = new Software("Proteome Discoverer", new CvParam(new CvTerm("MS:1000650", "Proteome Discoverer"), ""));

    private String id;
    private String version;

    public Software(String id, CvParam param) {

        this.id = id;
        this.version = "";
        addCvParam(param);
    }

    /**
     * Create a software with its name and version.
     *
     * @param id      software name
     * @param version software version
     */
    public Software(String id, String version) {

        checkNotNull(id);
        checkNotNull(version);

        this.id = id;
        this.version = version;
    }

    public String getID() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    /**
     * set the version of the software
     *
     * @param version version
     * @return this {@link Software}
     */
    public Software version(String version) {
        this.version = version;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Software software = (Software) o;

        if (!id.equals(software.id)) return false;
        return version.equals(software.version);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }
}
