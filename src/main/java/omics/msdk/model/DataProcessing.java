/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.UserParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Description of the way in which a particular software was used.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 8:26 PM
 */
public class DataProcessing {

    private List<UserParam> userParamList;
    private List<ProcessingMethod> processingMethodList;
    protected String id;

    /**
     * Constructor with the data processing id.
     *
     * @param id data processing identifier, most of the time it is the processing name.
     */
    public DataProcessing(String id) {
        Objects.requireNonNull(id);
        this.id = id;
    }

    public void addProcessingMethod(ProcessingMethod processingMethod) {
        if (processingMethodList == null)
            processingMethodList = new ArrayList<>();
        this.processingMethodList.add(processingMethod);
    }

    public void setProcessingMethodList(List<ProcessingMethod> processingMethodList) {
        this.processingMethodList = processingMethodList;
    }

    /**
     * Gets the value of the processingMethodList property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the processingMethodList property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessingMethodList().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessingMethod }
     */
    public List<ProcessingMethod> getProcessingMethodList() {
        if (processingMethodList == null) {
            processingMethodList = new ArrayList<>();
        }
        return this.processingMethodList;
    }

    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * <p>
     * Note: the id attribute is restricted by the XML schema data type 'ID'.
     * Valid values follow the NCName definition.
     * See also:
     * http://www.w3.org/TR/2000/CR-xmlschema-2-20001024/#ID
     * http://www.w3.org/TR/REC-xml-names/#NT-NCName
     *
     * @param value allowed object is {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    public void addUserParam(UserParam userParam) {
        if (userParamList == null)
            userParamList = new ArrayList<>();
        this.userParamList.add(userParam);
    }

    public List<UserParam> getUserParamList() {
        if (userParamList == null)
            userParamList = new ArrayList<>();
        return userParamList;
    }
}
