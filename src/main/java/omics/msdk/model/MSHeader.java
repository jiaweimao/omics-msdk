/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.msdk.io.util.ReferenceableParamGroup;
import omics.util.OmicsObject;
import omics.util.cv.Cv;
import omics.util.ms.MSDataID;

import java.util.ArrayList;
import java.util.List;

/**
 * Information except spectra of a raw data file.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Sep 2018, 4:31 PM
 */
public class MSHeader extends OmicsObject {

    private List<Sample> sampleList;
    private List<ReferenceableParamGroup> referenceableParamGroupList;
    private List<Cv> cvList = null;
    private FileDescription fileDescription = new FileDescription();
    private List<Software> softwareList;
    private List<InstrumentConfiguration> configurationList = null;
    private List<DataProcessing> dataProcessingList;
    private int scanCount = 0;
    private int chromatogramCount;
    private String runId;
    private String defaultInstrumentConfigurationRef;
    private String defaultDataProcessingRef;
    private MSDataID msDataID;

    /**
     * add a new {@link InstrumentConfiguration}
     *
     * @param configuration {@link InstrumentConfiguration}
     */
    public void addInstrumentConfiguration(InstrumentConfiguration configuration) {
        if (configurationList == null)
            configurationList = new ArrayList<>();
        configurationList.add(configuration);
    }

    public List<InstrumentConfiguration> getInstrumentConfigurationList() {
        if (configurationList == null)
            configurationList = new ArrayList<>();
        return configurationList;
    }

    /**
     * @return defaultDataProcessingRef for spectrumList, required for mzML
     */
    public String getDefaultDataProcessingRef() {
        return defaultDataProcessingRef;
    }

    public void setDefaultDataProcessingRef(String defaultDataProcessingRef) {
        this.defaultDataProcessingRef = defaultDataProcessingRef;
    }

    public FileDescription getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(FileDescription fileDescription) {
        this.fileDescription = fileDescription;
    }

    /**
     * @return number of spectra in the MS data file.
     */
    public int getScanCount() {
        return scanCount;
    }

    /**
     * set the number of spectra in the MS file.
     *
     * @param scanCount spectra count.
     */
    public void setScanCount(int scanCount) {
        this.scanCount = scanCount;
    }

    public List<Software> getSoftwareList() {
        return softwareList;
    }

    public void addSoftware(Software software) {
        if (softwareList == null)
            softwareList = new ArrayList<>();
        this.softwareList.add(software);
    }

    public void addDataProcessing(DataProcessing dataProcessing) {
        if (dataProcessingList == null)
            dataProcessingList = new ArrayList<>();
        this.dataProcessingList.add(dataProcessing);
    }

    public List<DataProcessing> getDataProcessingList() {
        if (dataProcessingList == null)
            dataProcessingList = new ArrayList<>();
        return dataProcessingList;
    }

    public void setDataProcessingList(List<DataProcessing> dataProcessingList) {
        this.dataProcessingList = dataProcessingList;
    }

    public void addCv(Cv cv) {
        if (cvList == null)
            cvList = new ArrayList<>();
        this.cvList.add(cv);
    }

    public List<Cv> getCvList() {
        if (cvList == null)
            cvList = new ArrayList<>();
        return cvList;
    }

    public List<ReferenceableParamGroup> getReferenceableParamGroupList() {
        return referenceableParamGroupList;
    }

    public void addReferenceableParamGroup(ReferenceableParamGroup paramGroup) {
        if (referenceableParamGroupList == null)
            referenceableParamGroupList = new ArrayList<>();
        this.referenceableParamGroupList.add(paramGroup);
    }

    public void setReferenceableParamGroupList(List<ReferenceableParamGroup> referenceableParamGroupList) {
        this.referenceableParamGroupList = referenceableParamGroupList;
    }

    public List<Sample> getSampleList() {
        return sampleList;
    }

    public void setSampleList(List<Sample> sampleList) {
        this.sampleList = sampleList;
    }

    public void addSample(Sample sample) {
        if (sampleList == null)
            sampleList = new ArrayList<>();
        this.sampleList.add(sample);
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getDefaultInstrumentConfigurationRef() {
        return defaultInstrumentConfigurationRef;
    }

    public void setDefaultInstrumentConfigurationRef(String defaultInstrumentConfigurationRef) {
        this.defaultInstrumentConfigurationRef = defaultInstrumentConfigurationRef;
    }

    /**
     * @return number of chromatograms in the raw data.
     */
    public int getChromatogramCount() {
        return chromatogramCount;
    }

    public void setChromatogramCount(int chromatogramCount) {
        this.chromatogramCount = chromatogramCount;
    }

    /**
     * @return {@link MSDataID} represent the raw file
     */
    public MSDataID getMSDataID() {
        return msDataID;
    }

    public void setMSDataID(MSDataID spectraData) {
        this.msDataID = spectraData;
    }
}
