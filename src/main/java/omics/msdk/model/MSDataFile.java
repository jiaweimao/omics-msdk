/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.msdk.io.MSDataAccessor;
import omics.util.OmicsObject;
import omics.util.interfaces.Clearable;
import omics.util.interfaces.Filter;
import omics.util.ms.Chromatogram;
import omics.util.ms.MSFileType;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumFilter;

import java.io.File;
import java.util.*;

/**
 * Raw data file, typically obtained by loading data from one of the supported
 * file formats. A raw data file is a collection of scans (MsnSpectrum) and Chromatogram.
 *
 * @author JiaweiMao
 * @version 1.5.2
 * @since 25 Aug 2018, 3:49 PM
 */
public class MSDataFile extends OmicsObject implements Clearable, Iterable<MsnSpectrum> {

    public static MSDataFile read(File file) {
        MSDataFile msDataFile = null;
        try {
            MSDataAccessor accessor = new MSDataAccessor(file);
            accessor.start();
            msDataFile = accessor.getValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msDataFile;
    }

    private String sampleName;
    private File iFile;
    private MSFileType iMSFileType;
    private ArrayList<MsnSpectrum> iSpectrumList = new ArrayList<>();
    private ArrayList<Chromatogram> iChromatogramList = new ArrayList<>();
    private HashMap<String, MsnSpectrum> iIdMap = new HashMap<>();
    private String name;
    private MSHeader header = null;

    public MSDataFile() {
    }

    /**
     * Constructor.
     *
     * @param sampleName name of the raw data file.
     */
    public MSDataFile(String sampleName) {
        this(sampleName, null, MSFileType.UNKNOWN);
    }

    /**
     * Constructor.
     *
     * @param sampleName          name of the raw data file.
     * @param originalRawDataFile {@link File} of the raw data file.
     * @param fileType            {@link MSFileType} of the raw data file.
     */
    public MSDataFile(String sampleName, File originalRawDataFile, MSFileType fileType) {
        this.sampleName = sampleName;
        this.iFile = originalRawDataFile;
        this.iMSFileType = fileType;
    }

    public MSDataFile(File rawFile, MSFileType fileType) {
        this(null, rawFile, fileType);
    }

    public MSHeader getHeader() {
        return header;
    }

    public void setHeader(MSHeader header) {
        this.header = header;
    }

    /**
     * @return raw data file name
     */
    public String getName() {
        if (name == null && iFile != null)
            name = iFile.getName();
        return name;
    }

    /**
     * set the name of this {@link MSDataFile}
     *
     * @param name unique name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of this raw data file. This can be any descriptive name,
     * not necessarily the original file name.
     *
     * @return Raw data file name
     */
    public String getSampleName() {
        return sampleName;
    }

    /**
     * Updates the name of this raw data file.
     *
     * @param name New name
     */
    public void setSampleName(String name) {
        Objects.requireNonNull(name);

        this.sampleName = name;
    }

    /**
     * Returns the original file name and path where the data was loaded from,
     * or null if this file was created in silico.
     *
     * @return Original filename and path.
     */
    public File getFile() {
        return iFile;
    }

    /**
     * Sets the original file name and path of the raw data file.
     *
     * @param newOriginalFile Original filename and path.
     */
    public void setFile(File newOriginalFile) {
        this.iFile = newOriginalFile;
    }

    /**
     * Returns the file type of this raw data file.
     *
     * @return Raw data file type
     */
    public MSFileType getMSFileType() {
        return iMSFileType;
    }

    /**
     * Sets the file type of this raw data file.
     *
     * @param rawDataFileType Raw data file type
     */
    public void setMSFileType(MSFileType rawDataFileType) {
        this.iMSFileType = rawDataFileType;
    }

    /**
     * Return an immutable list of all scans.
     *
     * @return all Spectrums.
     */
    public List<MsnSpectrum> getSpectrumList() {
        return iSpectrumList;
    }

    /**
     * Returns all chromatograms.
     *
     * @return A list of all chromatograms.
     */
    public List<Chromatogram> getChromatogramList() {
        return iChromatogramList;
    }

    /**
     * Return {@link Chromatogram} at given index.
     *
     * @param index index
     * @return {@link Chromatogram} at given index.
     */
    public Chromatogram getChromatogram(int index) {
        return iChromatogramList.get(index);
    }

    /**
     * Return {@link MsnSpectrum} at given index.
     *
     * @param index index
     * @return {@link MsnSpectrum} at given index.
     */
    public MsnSpectrum getSpectrum(int index) {
        return iSpectrumList.get(index);
    }

    /**
     * Return {@link MsnSpectrum} of given id
     *
     * @param id spectrum identifier
     */
    public MsnSpectrum get(String id) {
        return iIdMap.get(id);
    }

    /**
     * Add a new {@link MsnSpectrum}
     *
     * @param spectrum a {@link MsnSpectrum}
     */
    public void addSpectrum(MsnSpectrum spectrum) {
        this.iSpectrumList.add(spectrum);
        this.iIdMap.put(spectrum.getSpectrumID().getID(), spectrum);
    }

    public void addChromatogram(Chromatogram chromatogram) {
        this.iChromatogramList.add(chromatogram);
    }

    /**
     * Apply a given filter to this RawDataFile.
     *
     * @param filter a MsnSpectrum {@link Filter}
     */
    public void apply(SpectrumFilter filter) {
        Iterator<MsnSpectrum> it = iSpectrumList.iterator();
        while (it.hasNext()) {
            MsnSpectrum spectrum = it.next();
            if (!filter.test(spectrum)) {
                it.remove();
                iIdMap.remove(spectrum.getSpectrumID().getID());
            }
        }
    }

    /**
     * trim the capacity of this RawDataFile.
     */
    public void trim() {
        this.iSpectrumList.trimToSize();
        this.iChromatogramList.trimToSize();
    }

    /**
     * @return number of spectrum in this RawDataFile.
     */
    public int getSpectrumCount() {
        return iSpectrumList.size();
    }

    /**
     * @return number of chromatogram in this RawDataFile.
     */
    public int getChromatogramCount() {
        return iChromatogramList.size();
    }

    /**
     * Remove all data associated with this file from the disk. After this
     * method is called, any subsequent method calls on this object will throw
     * IllegalStateException.
     */
    @Override
    public void clear() {
        iChromatogramList.clear();
        iSpectrumList.clear();
        iIdMap.clear();

        iChromatogramList = null;
        iSpectrumList = null;
        iIdMap = null;
        header = null;
    }

    @Override
    public Iterator<MsnSpectrum> iterator() {
        return iSpectrumList.iterator();
    }
}
