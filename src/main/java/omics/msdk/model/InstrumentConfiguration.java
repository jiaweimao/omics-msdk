/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 7:25 PM
 */
public class InstrumentConfiguration extends ParamGroup {

    private String id;
    private List<Component> componentList;
    private String softwareRef;
    private Software software;
    private String vendor;
    private String model;
    private Double resolution;

    public InstrumentConfiguration(String id) {
        this.id = id;
    }

    /**
     * configuration id, required for mzML.
     *
     * @return configuration id.
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Component> getComponentList() {
        if (componentList == null)
            componentList = new ArrayList<>();
        return componentList;
    }

    public void setComponentList(List<Component> componentList) {
        this.componentList = componentList;
    }

    public void addComponent(Component component) {
        if (componentList == null)
            componentList = new ArrayList<>();
        this.componentList.add(component);
    }

    public String getSoftwareRef() {
        return softwareRef;
    }

    public void setSoftwareRef(String softwareRef) {
        this.softwareRef = softwareRef;
    }

    public Optional<Software> getSoftware() {
        return Optional.ofNullable(software);
    }

    public void setSoftware(Software software) {
        this.software = software;
        this.softwareRef = software.getID();
    }

    /**
     * @return the vendor of the instrument.
     */
    public Optional<String> getVendor() {
        return Optional.ofNullable(vendor);
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * instrument model, such as "LTQ Orbitrap Velos"
     *
     * @return instrument model.
     */
    public Optional<String> getModel() {
        return Optional.ofNullable(model);
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getResolution() {
        return resolution;
    }

    public void setResolution(Double resolution) {
        this.resolution = resolution;
    }
}
