/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.CvParam;
import omics.util.cv.UserParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Structure allowing the use of a controlled (cvParams) or uncontrolled vocabulary (userParams),
 * or a reference to a predefined set of these in this mzML file (paramGroupRef).
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 7:31 PM
 */
public class ParamGroup {

    private List<String> referenceableParamGroupRefs;
    private List<CvParam> cvParams;
    private List<UserParam> userParams;

    /**
     * Gets the value of the referenceableParamGroupRefs property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceableParamGroupRefs property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceableParamGroupRefs().add(newItem);
     * </pre>
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     */
    public List<String> getReferenceableParamGroupRefs() {
        if (referenceableParamGroupRefs == null) {
            referenceableParamGroupRefs = new ArrayList<>();
        }
        return this.referenceableParamGroupRefs;
    }

    public void addReferenceableParamGroupRef(String ref) {
        if (referenceableParamGroupRefs == null)
            referenceableParamGroupRefs = new ArrayList<>();
        this.referenceableParamGroupRefs.add(ref);
    }

    /**
     * Gets the value of the cvParams property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cvParams property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCvParams().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CvParam }
     */
    public List<CvParam> getCvParams() {
        if (cvParams == null) {
            cvParams = new ArrayList<>();
        }
        return this.cvParams;
    }

    public void addCvParam(CvParam cvParam) {
        if (cvParams == null)
            cvParams = new ArrayList<>();
        this.cvParams.add(cvParam);
    }

    public void addCvParams(List<CvParam> cvParamList) {
        if (cvParams == null)
            cvParams = new ArrayList<>();
        this.cvParams.addAll(cvParamList);
    }

    /**
     * return true if this {@link ParamGroup} contains given {@link CvParam}
     *
     * @param cvParam cvParam to test
     */
    public boolean contains(CvParam cvParam) {
        if (cvParams != null) {
            for (CvParam param : cvParams) {
                if (param.getCvTerm().getAccession().equals(cvParam.getCvTerm().getAccession()))
                    return true;
            }
        }

        return false;
    }

    /**
     * Gets the value of the userParams property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userParams property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserParams().add(newItem);
     * </pre>
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserParam }
     */
    public List<UserParam> getUserParams() {
        if (userParams == null) {
            userParams = new ArrayList<>();
        }
        return this.userParams;
    }

    public void addUserParam(UserParam userParam) {
        if (this.userParams == null)
            userParams = new ArrayList<>();
        this.userParams.add(userParam);
    }
}
