/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 8:30 PM
 */
public class ProcessingMethod extends ParamGroup {

    public static final CvTerm DEISOTOPING = new CvTerm("MS:1000033", "deisotoping");
    public static final CvTerm CHARGE_DECONVOLUTION = new CvTerm("MS:1000034", "charge deconvolution");
    public static final CvTerm PEAK_PICKING = new CvTerm("MS:1000035", "peak picking");
    public static final CvTerm CONVERSION_2_MZML = new CvTerm("MS:1000544", "Conversion to mzML");
    public static final CvTerm CONVERSION_2_MZXML = new CvTerm("MS:1000545", "Conversion to mzXML");
    public static final CvTerm CONVERSION_2_MZDATA = new CvTerm("MS:1000546", "Conversion to mzData");
    public static final CvTerm CONVERSION_2_DTA = new CvTerm("MS:1000741", "Conversion to dta");

    public static final CvTerm PRECURSOR_RECALCULATION = new CvTerm("MS:1000780", "precursor recalculation");
    public static final CvTerm INTENSITY_NORMALIZATION = new CvTerm("MS:1001484", "intensity normalization");
    public static final CvTerm DATA_FILTERING = new CvTerm("MS:1001486", "data filtering");

    /**
     * the order of the processing, missed in mzXML, mandatory in mzML
     */
    protected Integer order;
    protected String softwareRef;
    private Software software;

    public ProcessingMethod(Integer order, Software software, CvParam cvParam) {
        this.order = order;
        this.software = software;
        if (software != null)
            this.softwareRef = software.getID();
        addCvParam(cvParam);
    }

    /**
     * Create a {@link ProcessingMethod} with given {@link CvParam}
     *
     * @param param a {@link CvParam}
     */
    public ProcessingMethod(CvParam param) {
        addCvParam(param);
        this.order = 0;
    }

    public ProcessingMethod() {
    }

    /**
     * Gets the value of the order property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setOrder(Integer value) {
        this.order = value;
    }

    /**
     * Gets the value of the softwareRef property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSoftwareRef() {
        return softwareRef;
    }

    /**
     * Sets the value of the softwareRef property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSoftwareRef(String value) {
        this.softwareRef = value;
    }

    public Software getSoftware() {
        return software;
    }

    public void setSoftware(Software software) {
        this.software = software;
        if (software != null) {
            this.softwareRef = software.getID();
        }
    }
}
