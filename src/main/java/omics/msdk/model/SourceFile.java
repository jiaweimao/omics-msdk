/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;

import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;
import omics.util.ms.MSFileType;
import omics.util.ms.TitleFormat;
import omics.util.utils.ObjectUtils;

/**
 * Description of the source file of spectrum data, including location and type.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 2:07 PM
 */
public class SourceFile extends ParamGroup {

    public static final CvParam Thermo_NativeID = new CvParam(new CvTerm("MS:1000768", "Thermo nativeID format"), "");
    public static final CvParam Thermo_Raw = new CvParam(new CvTerm("MS:1000563", "Thermo RAW format"), "");
    public static final CvParam Waters_Raw = new CvParam(new CvTerm("MS:1000526", "Waters raw format"), "");
    public static final CvParam ABI_Wiff = new CvParam(new CvTerm("MS:1000562", "ABI WIFF format"), "");
    /**
     * Parameter file used to configure the acquisition of raw data on the instrument.
     */
    public static final CvParam Parameter = new CvParam(new CvTerm("MS:1000740", "parameter file"), "");

    /**
     * Return the {@link TitleFormat} of the spectrum, reutrn {@link TitleFormat#CUSTOM} for default.
     */
    public TitleFormat getTitleFormat() {

        for (CvParam cvParam : getCvParams()) {
            TitleFormat fileType = TitleFormat.format4Acc(cvParam.getCvTerm().getAccession());
            if (fileType != TitleFormat.CUSTOM)
                return fileType;
        }
        return TitleFormat.CUSTOM;
    }

    /**
     * @return {@link MSFileType} of the source file
     */
    public MSFileType getMSFileType() {
        for (CvParam cvParam : getCvParams()) {
            MSFileType fileType = MSFileType.type4Acc(cvParam.getCvTerm().getAccession());
            if (fileType != MSFileType.UNKNOWN)
                return fileType;
        }
        return MSFileType.UNKNOWN;
    }

    private String id;
    /**
     * the name of the file, with out path
     */
    private String name;
    /**
     * file path
     */
    private String location;

    /**
     * Construct a source file
     *
     * @param id       any string that is unique within the file
     * @param name     file name
     * @param location file location, such as URI location
     */
    public SourceFile(String id, String name, String location) {
        ObjectUtils.checkNotNull(id);
        ObjectUtils.checkNotNull(name);
        ObjectUtils.checkNotNull(location);

        this.id = id;
        this.name = name;
        this.location = location;
    }

    /**
     * @return unique identifier of the source file.
     */
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }
}
