/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.model;


/**
 * MS instrument component:
 * source, analyzer, detector...
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 7:28 PM
 */
public class Component extends ParamGroup
{
    /**
     * This attribute must be used to indicate the order in which the components are encountered from source to
     * detector (e.g., in a Q-TOF, the quadrupole would have the lower order number, and the TOF the higher number of the two).
     */
    private int order;

    private String type;

    public int getOrder()
    {
        return order;
    }

    public void setOrder(int order)
    {
        this.order = order;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
