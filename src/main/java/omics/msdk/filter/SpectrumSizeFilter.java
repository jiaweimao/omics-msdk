/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.filter;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumFilter;

/**
 * filter spectrum based on size
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 31 Oct 2018, 7:43 PM
 */
public class SpectrumSizeFilter implements SpectrumFilter {

    private int minSize;
    private int maxSize;

    public SpectrumSizeFilter(int minSize, int maxSize) {
        this.minSize = minSize;
        this.maxSize = maxSize;
    }


    @Override
    public boolean test(MsnSpectrum filterable) {

        int size = filterable.size();
        return size >= minSize && size <= maxSize;
    }

    @Override
    public String toString() {
        return "SpectrumSizeFilter{" +
                "minSize=" + minSize +
                ", maxSize=" + maxSize +
                '}';
    }
}
