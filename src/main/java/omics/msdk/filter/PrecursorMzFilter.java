/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.filter;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumFilter;
import omics.util.ms.peaklist.Peak;
import omics.util.utils.IntervalList;

/**
 * filter spectrum based on precursor mz
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 31 Oct 2018, 8:46 PM
 */
public class PrecursorMzFilter implements SpectrumFilter {

    private IntervalList intervalList;
    private boolean include;

    /**
     * Constructor.
     *
     * @param intervalList list of tested mzs.
     * @param include      true if include spectrum with these mzs.
     */
    public PrecursorMzFilter(IntervalList intervalList, boolean include) {

        this.intervalList = intervalList;
        this.include = include;
    }

    @Override
    public boolean test(MsnSpectrum filterable) {

        Peak precursor = filterable.getPrecursor();
        return intervalList.contains(precursor.getMz()) == include;
    }

    @Override
    public String toString() {
        return "PrecursorMzFilter{" +
                "intervalList=" + intervalList +
                ", include=" + include +
                '}';
    }
}
