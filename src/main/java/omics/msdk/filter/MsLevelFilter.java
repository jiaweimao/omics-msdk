/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.filter;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumFilter;

import static omics.util.utils.ObjectUtils.checkArgument;

/**
 * filter spectrum based on ms level
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 01 Nov 2018, 8:44 AM
 */
public class MsLevelFilter implements SpectrumFilter {

    private int min;
    private int max;

    /**
     * constructor, if min=max, only spectra of given ms level will be kept.
     *
     * @param min allowed min ms level, inclusive
     * @param max allowed max ms level, inclusive
     */
    public MsLevelFilter(int min, int max) {

        checkArgument(min > 0, "ms level should > 0");
        checkArgument(max > 0, "ms level shold > 0");
        checkArgument(min <= max, "min level should <= max level");

        this.min = min;
        this.max = max;
    }

    @Override
    public boolean test(MsnSpectrum filterable) {

        int msLevel = filterable.getMsLevel();
        return msLevel >= min && msLevel <= max;
    }

    @Override
    public String toString() {

        return "MsLevelFilter{" + "min=" + min + ", max=" + max + '}';
    }
}
