/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.filter;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumFilter;
import omics.util.ms.peaklist.Peak;

/**
 * Filter spectrum based on precursor charge.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 01 Nov 2018, 9:48 AM
 */
public class PrecursorChargeFilter implements SpectrumFilter {

    private int minCharge;
    private int maxCharge;

    /**
     * Constructor.
     *
     * @param minCharge minimum charge, inclusive
     * @param maxCharge maximum charge, inclusive
     */
    public PrecursorChargeFilter(int minCharge, int maxCharge) {

        this.minCharge = minCharge;
        this.maxCharge = maxCharge;
    }

    @Override
    public boolean test(MsnSpectrum filterable) {
        Peak precursor = filterable.getPrecursor();
        int charge = precursor.getCharge();
        return charge >= minCharge && charge <= maxCharge;
    }

    @Override
    public String toString() {
        return "PrecursorChargeFilter{" + "minCharge=" + minCharge + ", maxCharge=" + maxCharge + '}';
    }
}
