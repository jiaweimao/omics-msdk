/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.xic;

import com.google.common.collect.Range;
import omics.util.OmicsException;
import omics.util.OmicsTask;
import omics.util.ms.Chromatogram;
import omics.util.ms.ChromatogramType;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.SeparationType;

import java.util.List;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * A Builder for XIC chromatogram.
 *
 * @author JiaweiMao
 * @version 2.1.1
 * @since 23 Mar 2017, 7:06 PM
 */
public class XicBuilder extends OmicsTask<Chromatogram> {

    private final List<MsnSpectrum> scanList;
    private final ChromatogramType chromatogramType;
    private final Range<Double> mzRange;

    /**
     * Constructor.
     *
     * @param scanList         MsScan list used to construct Chromatogram.
     * @param chromatogramType a {@link ChromatogramType} object, which affect the Chromatogram Intensity value.
     * @param mzRange          mz range to detect points.
     */
    public XicBuilder(List<MsnSpectrum> scanList, ChromatogramType chromatogramType, Range<Double> mzRange) {
        checkNotNull(scanList);
        checkArgument(!scanList.isEmpty(), "The Spectrum list is empty");

        this.scanList = scanList;
        this.chromatogramType = chromatogramType;
        this.mzRange = mzRange;
    }

    /**
     * Build Chromatogram from a list of {@link MsnSpectrum}.
     *
     * @param spectrumList     {@link MsnSpectrum} list.
     * @param chromatogramType {@link ChromatogramType}.
     * @param mzRange          mz range to detect peaks.
     * @return {@link Chromatogram}. or null if build cancelled or  failed.
     */
    public static Chromatogram getChromatogram(List<MsnSpectrum> spectrumList, ChromatogramType chromatogramType,
            Range<Double> mzRange) {
        try {
            XicBuilder builder = new XicBuilder(spectrumList, chromatogramType, mzRange);
            builder.start();
            return builder.getValue();
        } catch (OmicsException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void start() throws OmicsException {
        int totalScanCount = scanList.size();

        double[] rts = new double[totalScanCount];
        double[] chromMzValues = new double[totalScanCount];
        double[] chromIntensityValues = new double[totalScanCount];

        for (int processedScanCount = 0; processedScanCount < totalScanCount; processedScanCount++) {

            if (isStopped()) {
                updateMessage("Chromatogram build work is cancelled.");
                return;
            }

            MsnSpectrum scan = scanList.get(processedScanCount);
            rts[processedScanCount] = scan.getSpectrumID().getRetentionTime().getTime();

            int index = scan.getMostIntenseIndex(mzRange.lowerEndpoint(), mzRange.upperEndpoint());
            if (index > 0)
                chromMzValues[processedScanCount] = scan.getMz(index);
            else
                chromMzValues[processedScanCount] = (mzRange.lowerEndpoint() + mzRange.upperEndpoint()) / 2;

            switch (chromatogramType) {
                case BPC:
                    if (index > 0)
                        chromIntensityValues[processedScanCount] = scan.getIntensity(index);
                    break;
                case TIC:
                case SIC: {
                    int low = scan.indexOf(mzRange.lowerEndpoint());
                    if (low < 0)
                        low = -(low + 1);
                    int high = scan.indexOf(mzRange.upperEndpoint());
                    if (high < 0)
                        high = -(high + 1);

                    double sum = 0;
                    for (int key = low; key <= high; key++) {
                        if (mzRange.contains(scan.getMz(key)))
                            sum += scan.getIntensity(key);
                    }
                    chromIntensityValues[processedScanCount] = sum;
                    break;
                }
                default:
                    throw new OmicsException("Invalid chromatogram type:" + chromatogramType);

            }

            updateProgress(processedScanCount, totalScanCount);
        }

        Chromatogram result = new Chromatogram(chromatogramType, SeparationType.UNKNOWN);
        result.addSorted(rts, chromIntensityValues, chromMzValues);
        updateValue(result);
        updateMessage("Done!");
    }
}
