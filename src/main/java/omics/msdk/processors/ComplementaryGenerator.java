/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.SpectrumProcessor;

import java.util.List;

/**
 * Generate the complementary spectrum.
 * <ol>
 * <li>b/y ions are converted to y/b ions.</li>
 * <li>a/x ions are converted to x/a ions.</li>
 * </ol>
 * <p>
 * the spectrum should be de-isotoped and de-convoluted before this operation. As this method can only applied to charge 1+
 * peaks.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 10 Sep 2018, 3:04 PM
 */
public class ComplementaryGenerator extends SpectrumProcessor<PeakAnnotation, PeakAnnotation>
{
    @Override
    public void processPeak(double mz, double intensity, List<PeakAnnotation> annotations)
    {
        double pairMz = getPrecursor().getMass() - mz + PeriodicTable.PROTON_MASS * 2;
        if (pairMz > 100)
            sink.processPeak(pairMz, intensity, annotations);
    }
}
