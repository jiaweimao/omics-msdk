/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DelayedSpectrumProcessor;
import omics.util.protein.MassCalculator;

import java.util.List;

/**
 * A simple implementation of spectrum deconvolution algorithm.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 29 May 2019, 2:44 PM
 */
public class DeconvoluteSimple extends DelayedSpectrumProcessor<PeakAnnotation, PeakAnnotation>
{
    private static final double ISOTOPE = PeriodicTable.C12C13MassDiff;
    private static final double ISOTOPE2 = PeriodicTable.C14_MASS - PeriodicTable.C13_MASS;

    private Tolerance tol;
    private double chargeCarrierMass;

    public DeconvoluteSimple(Tolerance tol)
    {
        this(tol, MassCalculator.getChargeCarrierMass());
    }

    /**
     * Constructor.
     *
     * @param tol               {@link Tolerance} used to match peaks
     * @param chargeCarrierMass mass of the particle with charge
     */
    public DeconvoluteSimple(Tolerance tol, double chargeCarrierMass)
    {
        this.tol = tol;
        this.chargeCarrierMass = chargeCarrierMass;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<PeakAnnotation>> annotationMap)
    {
        Peak precursor = getPrecursor();
        int maxCharge = Math.min(precursor.getCharge(), 4);

        boolean[] ignore = new boolean[mzList.size()];
        for (int i = 0; i < mzList.size(); i++) {
            if (ignore[i])
                continue;

            double mz = mzList.getDouble(i);
            double newMz = mz;

            for (int z = 2; z < maxCharge; z++) {
                boolean isDeconvoluted = false;

                for (int j = i + 1; j < mzList.size(); j++) {
                    double mz2 = mzList.getDouble(j);
                    if (tol.withinTolerance(mz2, mz + ISOTOPE / z)) {
                        ignore[j] = true;
                        newMz = z * mz - (z - 1) * chargeCarrierMass;
                        isDeconvoluted = true;

                        for (int k = j + 1; k < mzList.size(); k++) {
                            double mz3 = mzList.getDouble(k);

                            if (tol.withinTolerance(mz3, mz2 + ISOTOPE2 / z)) {
                                ignore[k] = true;
                                double kMz = z * mz3 - (z - 1) * chargeCarrierMass;
                                sink.processPeak(kMz, intensityList.getDouble(k), annotationMap.get(k));
                                break;
                            } else if (tol.isLarger(mz2 + ISOTOPE2 / z, mz3))
                                break;
                        }

                        double mzj = z * mz2 - (z - 1) * chargeCarrierMass;
                        sink.processPeak(mzj, intensityList.getDouble(j), annotationMap.get(j));
                        break;
                    } else if (tol.isLarger(mz + ISOTOPE / z, mz2))
                        break;
                }
                if (isDeconvoluted)
                    break;
            }
            sink.processPeak(newMz, intensityList.getDouble(i), annotationMap.get(i));
        }
    }
}
