/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;
import omics.util.ms.peaklist.impl.PpmTolerance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Sep 2018, 12:24 PM
 */
public class PoissonDeisotoper<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{
    private double massNeutron = PeriodicTable.C12C13MassDiff;
    private Tolerance tol = new PpmTolerance(100);
    private int maxCharge = 3;
    private int minCharge = 1;
    private int maxIsotopePeaks = 5;
    private static final double[] KL_CUTOFFS = new double[]{0.025, 0.05, 0.1, 0.2, 0.3};
    private static final double[] SSE_CUTOFFS = new double[]{0.00005, 0.0001, 0.0003, 0.0006, 0.001};

    public PoissonDeisotoper(Tolerance tol)
    {
        this.tol = tol;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<A>> annotationMap)
    {
        int nPeaks = mzList.size();
        if (nPeaks < 2) {
            for (int i = 0; i < mzList.size(); i++) {
                processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotationMap.get(i));
            }
            return;
        }

        List<IsoChain> chains = new ArrayList<>();
        // build chains of potential isotopes
        for (int j = 0; j < nPeaks; j++) {

            for (int k = j + 1; k < nPeaks; k++) {
                // subsequent sets of peaks will have spacing that is too large
                if (tol.check(mzList.getDouble(j) + massNeutron, mzList.getDouble(k)) == Tolerance.Location.LARGER)
                    break;

                double recip = massNeutron / (mzList.getDouble(k) - mzList.getDouble(j));
                int possibleCharge = (int) (recip + 0.5);
                if (possibleCharge > maxCharge || possibleCharge < minCharge)
                    continue;

                if (tol.withinTolerance(massNeutron / possibleCharge + mzList.getDouble(j), mzList.getDouble(k))) {
                    // checking if this can be connected to previous chains
                    for (int w = 0; w < chains.size(); ++w) {
                        IsoChain testChain = chains.get(w);

                        if (possibleCharge != testChain.charge) continue;
                        if (testChain.indexList.size() >= maxIsotopePeaks) continue;

                        int finalIndex = testChain.indexList.get(testChain.indexList.size() - 1);
                        if (j == finalIndex) {
                            chains.add(testChain); // push back copy of previous chain with current size
                            testChain.indexList.add(k); // now extend the size of the chain
                        }

                    }

                    // create a new chain of length 2
                    IsoChain newChain = new IsoChain(possibleCharge);
                    newChain.indexList.add(j);
                    newChain.indexList.add(k);
                    chains.add(newChain);
                }
            } // end if peak is within tolerance
        }

        Map<Integer, Integer> index2Charge = new HashMap<>();
        boolean[] removePeak = new boolean[nPeaks];
        for (IsoChain chain : chains) {
            double klScore = getKLScore(chain, mzList, intensityList);
            int length = chain.indexList.size();

            // calculate average monoisotopic mass absed on m/z positions in chain
            double average = 0.0;
            for (int k = 0; k < length; k++) {
                average += (mzList.getDouble(chain.indexList.get(k)) - k * massNeutron / chain.charge);
            }
            average /= length;

            // calculate summed square error of m/z positions in chain relative to the average monoisotopic m/z
            double sse = 0.0;
            for (int k = 0; k < length; ++k) {
                sse += Math.pow(average - (mzList.getDouble(chain.indexList.get(k)) - k * massNeutron / chain.charge), 2);
            }

            int reducedLength = length <= 6 ? length - 2 : 4;
            double thisKLcutoff = KL_CUTOFFS[reducedLength];
            double thisSSEcutoff = SSE_CUTOFFS[reducedLength];
            double monoIntenisty = intensityList.getDouble(chain.indexList.get(0));

            if (klScore < thisKLcutoff && sse < thisSSEcutoff && monoIntenisty > 5.0) {
                index2Charge.put(chain.indexList.get(0), chain.charge);
                for (int i = 1; i < chain.indexList.size(); ++i) { // do not remove first peak
                    removePeak[chain.indexList.get(i)] = true;
                }
            }
        }
//        System.out.println(Arrays.toString(removePeak));
        for (int i = 0; i < nPeaks; i++) {
            if (!removePeak[i]) {
                List<A> annotations;
                if (annotationMap.containsKey(i)) {
                    annotations = annotationMap.get(i);
                } else {
                    annotations = new ArrayList<>(1);
                }
                if (index2Charge.containsKey(i)) {
                    annotations.add((A) new PeakChargeAnnotation(index2Charge.get(i)));
                }
                sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotations);
            }
        }
    }

    private double getKLScore(IsoChain chain, DoubleArrayList mzList, DoubleArrayList inList)
    {
        // convert from molecular weight of ion to Mstar, which is linear mapping
        double lambda = 1.0 / 1800; // parameter for poisson model
        double mstar = lambda * mzList.getDouble(chain.indexList.get(0)) * chain.charge; // from msInspect paper
        double mexp = Math.exp(-mstar);

        double poissonSum = 0.0; // sum up all the poisson values to normalize
        double observedIntensitySum = 0.0; //initialize this sum
        double KLscore = 0.0;

        double[] poissonVals = new double[chain.indexList.size()];

        // calculate poisson distribution and sum up the intensities for normalization
        for (int k = 0; k < chain.indexList.size(); ++k) {
            // probability of seeing isotope with k additional ions relative to monoisotope
            double poisson = mexp * Math.pow(mstar, k);
            for (int w = k; w > 1; w--) poisson /= (double) w;
            poissonVals[k] = poisson; // store value

            // sums for normalization
            poissonSum += poisson;
            observedIntensitySum += inList.getDouble(chain.indexList.get(k));
        }

        // calculate the K-L score
        for (int k = 0; k < chain.indexList.size(); ++k) {
            poissonVals[k] /= poissonSum; // normalize these values to use in the K-L score
            double normObservedIntensity = inList.getDouble(chain.indexList.get(k)) / observedIntensitySum;
            KLscore += normObservedIntensity * Math.log10(normObservedIntensity / poissonVals[k]);
        }

        return KLscore;
    }


    private class IsoChain
    {
        int charge;
        List<Integer> indexList;

        public IsoChain(int charge)
        {
            this.charge = charge;
            this.indexList = new ArrayList<>();
        }
    }
}
