/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.msdk.io.MzMLReader;
import omics.util.io.csv.CsvWriter;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.Polarity;
import omics.util.ms.Spectrum;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.filter.ThresholdFilter;
import omics.util.ms.peaklist.impl.PpmTolerance;

import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 15 Apr 2018, 12:48 PM
 */
public class ZscoreApp
{
    public static void main(String[] args) throws IOException
    {
        MzMLReader reader = new MzMLReader(new File(args[0]));
        CsvWriter writer = new CsvWriter(args[1]);

        double ppm = Double.parseDouble(args[2]);
        double threshold = 0.01;
        if (args.length > 3) {
            try {
                threshold = Double.parseDouble(args[3]);
            } catch (Exception ignored) {
            }
        }

        ZscoreDeisotoper zscore = new ZscoreDeisotoper(new PpmTolerance(ppm), Polarity.POSITIVE, true);
        zscore.useMostIntenseMz(true);
        zscore.useMostIntenseIntensity(true);
        zscore.setConvert2One(false);

        while (reader.hasNext()) {
            Spectrum<PeakAnnotation> spectrum = reader.next();
            if (spectrum instanceof MsnSpectrum) {
                MsnSpectrum msnSpectrum = (MsnSpectrum) spectrum;
                msnSpectrum.apply(new ThresholdFilter<>(spectrum.getBasePeakY() * threshold));
                writer.writeRecord(new String[]{"M/Z", "Intensity"});

                MsnSpectrum peakList = msnSpectrum.copy(zscore);
                for (int i = 0; i < peakList.size(); i++) {
                    writer.write(peakList.getMz(i));
                    writer.write(peakList.getIntensity(i));
                    writer.endRecord();
                }
                writer.endRecord();
            }
        }

        reader.close();
        writer.close();
    }
}
