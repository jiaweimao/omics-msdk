/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;
import omics.util.utils.ArrayIndexSorter;
import omics.util.utils.DoubleDescendingComparator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Jan 2019, 7:21 PM
 */
public class AddPeakRankAnnotation<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{
    private DoubleDescendingComparator comparator;

    public AddPeakRankAnnotation()
    {
        comparator = new DoubleDescendingComparator();
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<A>> annotationMap)
    {
        comparator.setArray(intensityList.toDoubleArray());
        Integer[] indexes = ArrayIndexSorter.sort(comparator);
        for (int rank = 0; rank < indexes.length; rank++) {
            int index = indexes[rank];
            List<A> ans = annotationMap.get(index);
            if (ans == null) {
                ans = new ArrayList<>(1);
                annotationMap.put(index, ans);
            }
            ans.add((A) new PeakRankAnnotation(rank + 1));
        }

        for (int i = 0; i < mzList.size(); i++) {
            sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotationMap.get(i));
        }
    }
}
