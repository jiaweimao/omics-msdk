/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.*;
import omics.util.chem.IonTools;
import omics.util.chem.PeriodicTable;
import omics.util.ms.Polarity;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DoublePeakList;
import omics.util.utils.ArrayIndexSorter;
import omics.util.utils.DoubleDescendingComparator;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Detail of this algorithm can be seen here:<br> Zhang, Z. and A. G. Marshall (1998). "A universal algorithm for fast
 * and automated charge state deconvolution of electrospray mass-to-charge ratio spectra." Journal of the American
 * Society for Mass Spectrometry 9(3): 225-233.
 * <p>
 * Only the High-Resolution Low-Charge ESI Spectra are accepted, (max-charge=20)
 * <p>
 * As current high-resolution MS have really high SNR, so we ignore the SNR filter step.
 * <p>
 * For peptide ms2 spectrum, as the mass is relative small, this algorithm is not very efficient.
 *
 * @author JiaweiMao
 * @version 2.3.3
 * @since 30 Apr 2017, 12:38 PM
 */
public class ZscoreDeisotoper extends ChargeAssigner
{
    /**
     * The isotopeDistance constant defines expected distance between isotopes. Actual weight of 1 neutron is 1.008665
     * Da, but part of this mass is consumed as binding energy to other protons/neutrons. Actual mass increase of
     * isotopes depends on chemical formula of the molecule. Since we don't know the formula, we can assume the distance
     * to be ~1.0033 Da, with user-defined tolerance.
     */
    private static final double IsotopeDistance = PeriodicTable.C12C13MassDiff;
    /**
     * With the Averagine model, when the peak mass is larger than this value, the second peak will larger than the
     * monopeak, we need to step forward to search for IsotopePattern. 'F'
     */
    private static final double MonoMass = 1342;

    // use min mz or base peak mz
    private boolean useMinMz = true;
    // use tic or base peak intensity
    private boolean useTic = true;
    /**
     * convert isotope peak cluster to a peak
     */
    private boolean collapse = true;
    /**
     * convert the high charge peaks to 1+
     */
    private boolean convert2One = true;

    private double intensityRatioAfter = 10;
    private double intensityRatioBefore = 5;

    private int minPeakPerPattern = 2;

    /**
     * Constructor with default settings.
     *
     * @param aTol peak match {@link Tolerance}
     */
    public ZscoreDeisotoper(Tolerance aTol)
    {
        super(aTol, Polarity.POSITIVE);
    }

    public ZscoreDeisotoper(Tolerance aTol, Polarity aPolarity, boolean collapse)
    {
        super(aTol, aPolarity);
        this.collapse = collapse;
    }

    /**
     * Use the most intense peak m/z as the IsotopePattern m/z, default to be the mono peak m/z.
     */
    public void useMostIntenseMz(boolean value)
    {
        this.useMinMz = !value;
    }

    /**
     * Use the most intense peak intensity as the IsotopePattern intensity, default to use the tic.
     */
    public void useMostIntenseIntensity(boolean value)
    {
        this.useTic = !value;
    }

    /**
     * Set the minimum size of a IsotopePattern, which default to be 2.
     *
     * @param size a int value.
     */
    public void setMinPatternSize(int size)
    {
        this.minPeakPerPattern = size;
    }

    public void setIntensityRatioAfter(double intensityRatioAfter)
    {
        this.intensityRatioAfter = intensityRatioAfter;
    }

    public void setIntensityRatioBefore(double intensityRatioBefore)
    {
        this.intensityRatioBefore = intensityRatioBefore;
    }

    /**
     * set Convert peak charge to 1+, default to be true.
     *
     * @param convert true if the convert peaks to 1+.
     */
    public void setConvert2One(boolean convert)
    {
        this.convert2One = convert;
    }

    public void setCollapse(boolean collapse)
    {
        this.collapse = collapse;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap annotationMap)
    {
        PeakList<PeakAnnotation> pl = new DoublePeakList<>(mzList.size());
        for (int i = 0; i < mzList.size(); i++) {
            pl.add(mzList.getDouble(i), intensityList.getDouble(i));
        }

        PeakList<PeakChargeAnnotation> resultPeaks = assign_highRes_lowZ(pl);

        for (int i = 0; i < resultPeaks.size(); i++) {
            sink.processPeak(resultPeaks.getX(i), resultPeaks.getY(i), resultPeaks.getAnnotations(i));
        }
    }


    /**
     * For a low-resolution m/z spectrum, the only information available for charge state determination is the
     * distribution of charge states for ions of the same mass.
     * <p>
     * A charge scoring system based on these relations can be developed to deconvolve a complex ESI spectrum.
     * <p>
     * currently only support positive charge.
     *
     * @param peakList a PeakList
     * @param maxMass  maximum mass of fragment ion.
     */
    private void assign_lowRes(PeakList<PeakAnnotation> peakList, double maxMass)
    {
        DoubleDescendingComparator comparator = new DoubleDescendingComparator();
        comparator.setArray(peakList.getYs());

        Integer[] indexes = ArrayIndexSorter.sort(comparator);

        Int2IntMap rank2IdexMap = new Int2IntOpenHashMap(indexes.length);
        for (int i = 0; i < indexes.length; i++) {
            rank2IdexMap.put(indexes[i].intValue(), i);
        }

        IntSet processedSet = new IntOpenHashSet();
        for (int rank = 0; rank < peakList.size(); rank++) {

            int index = rank2IdexMap.get(rank);
            if (processedSet.contains(index))
                continue;

            double mz = peakList.getX(index);
            int zmax = (int) Math.round(maxMass / mz);

            double maxScore = 0.0;
            int bestZ = -1;
            for (int z = 1; z <= zmax; z++) {


            }


        }

    }


    /**
     * For a Mass spectrometry with resolution ~ 60,000, this method can works reliably up to z~20
     *
     * @param peakList a peak list
     */
    private PeakList<PeakChargeAnnotation> assign_highRes_lowZ(PeakList<PeakAnnotation> peakList)
    {
        Peak[] peakArray = new Peak[peakList.size()];
        for (int i = 0; i < peakList.size(); i++)
            peakArray[i] = new Peak(peakList.getX(i), peakList.getY(i));

        Arrays.sort(peakArray, (o1, o2) -> Double.compare(o2.getIntensity(), o1.getIntensity()));

        PeakList<PeakChargeAnnotation> resultPeaks = new DoublePeakList<>();

        IntSet processedSet = new IntOpenHashSet();
        for (Peak peak : peakArray) {

            double mz = peak.getMz();
            int index = peakList.getMostIntenseIndex(iMzTol.getMin(mz), iMzTol.getMax(mz));

            if (processedSet.contains(index))
                continue;

            double lMz = index == 0 ? Double.MIN_VALUE : peakList.getX(index - 1);
            double rMz = index == (peakList.size() - 1) ? Double.MAX_VALUE : peakList.getX(index + 1);

            double deltaMz = Math.min(mz - lMz, rMz - mz);
            if (iMzTol.isLarger(IsotopeDistance, deltaMz)) {
                resultPeaks.add(peakList.getX(index), peakList.getY(index));
                processedSet.add(index);
                continue;
            }

            int pMaxZ = (int) Math.round(IsotopeDistance / deltaMz);
            pMaxZ = Math.min(pMaxZ, maxAbsCharge);

            int bestZ = -1;
            double bestScore = -1;

            IntList bestIds = new IntArrayList();
            for (int z = 1; z <= pMaxZ; z++) {

                IntList zIds = new IntArrayList();

                findPeaks(peakList, index, z, 1, intensityRatioAfter, zIds, processedSet);
                if (IonTools.calcNeutralMolecularMass(mz, z) > MonoMass) {
                    findPeaks(peakList, index, z, -1, intensityRatioBefore, zIds, processedSet);
                }

                double score = 0;
                for (int id : zIds) {
                    score += Math.log(peakList.getY(id));
                }
                if (score > bestScore) {
                    bestZ = z;
                    bestScore = score;
                    bestIds.clear();
                    bestIds.addAll(zIds);
                }
            }

            bestIds.add(index);
            if (bestIds.size() >= minPeakPerPattern) {

                processedSet.addAll(bestIds);
                bestIds.sort(Comparator.naturalOrder());

                if (collapse) {

                    double newMz;
                    if (useMinMz) {
                        if (convert2One)
                            newMz = IonTools.calcMz(IonTools.calcNeutralMolecularMass(peakList.getX(bestIds.getInt(0)), bestZ), 1);
                        else
                            newMz = peakList.getX(bestIds.getInt(0));
                    } else {
                        if (convert2One)
                            newMz = IonTools.calcMz(IonTools.calcNeutralMolecularMass(mz, bestZ), 1);
                        else
                            newMz = mz;
                    }

                    double newIn;
                    if (useTic) {
                        double sum = 0;
                        for (int id : bestIds)
                            sum += peakList.getY(id);

                        newIn = sum;
                    } else
                        newIn = peak.getIntensity();

                    resultPeaks.add(newMz, newIn);
                } else {
                    for (int id : bestIds)
                        resultPeaks.add(peakList.getX(id), peakList.getY(id), new PeakChargeAnnotation(bestZ));
                }

            } else {
                processedSet.add(index);
                resultPeaks.add(mz, peakList.getY(index));
            }
        }

        return resultPeaks;
    }

    private void findPeaks(PeakList<PeakAnnotation> peakList, int index, int z, int direction, double inRatio,
                           IntList idList, IntSet processedSet)
    {
        int n = direction;
        boolean followPeakFound;

        double mz = peakList.getX(index);
        double previousIn = peakList.getY(index);

        do {
            // Assume we don't find match for n:th peak in the pattern (which will end the loop)
            followPeakFound = false;
            double nextMz = mz + IsotopeDistance * n / z;

            int goodId = peakList.getMostIntenseIndex(iMzTol.getMin(nextMz), iMzTol.getMax(nextMz));
            if (processedSet.contains(goodId))
                break;

            if (goodId > 0) {
                double goodIn = peakList.getY(goodId);

                if ((previousIn > goodIn) && ((previousIn / goodIn) < inRatio)) {
                    idList.add(goodId);
                    followPeakFound = true;
                    // n:th peak was found, move on to n+1
                    n += direction;
                    previousIn = goodIn;
                }
            }
        } while (followPeakFound);
    }
}
