/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.filter.TopNList;
import omics.util.ms.peaklist.impl.DelayedSpectrumProcessor;

import java.util.Collections;
import java.util.List;

/**
 * Implement the add_topN_complementary function in MSFragger:
 * Kong, A. T., Leprevost, F. V, Avtonomov, D. M., Mellacheruvu, D. & Nesvizhskii, A. I.
 * MSFragger: ultrafast and comprehensive peptide identification in mass spectrometry–based proteomics. Nat. Methods 14, 513–520 (2017).
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 10 Nov 2018, 8:57 AM
 */
public class MSFraggerComplementary extends DelayedSpectrumProcessor<PeakAnnotation, PeakAnnotation>
{
    private TopNList<PeakAnnotation> topNList;
    private boolean addHighCharge;
    private static final double MZ_LIMIT = 100;

    public MSFraggerComplementary()
    {
        this(20);
    }

    public MSFraggerComplementary(int topN)
    {
        this(topN, true);
    }

    /**
     * Constructor
     *
     * @param topN          add complementary ions for topN peaks
     * @param addHighCharge true if add 2+ complementary ion for precursor with charge > 2
     */
    public MSFraggerComplementary(int topN, boolean addHighCharge)
    {
        this.topNList = new TopNList<>(topN);
        this.addHighCharge = addHighCharge;
    }

    @Override
    public void start(int size)
    {
        super.start(size);
        topNList.clear();
    }

    @Override
    public void processPeak(double mz, double intensity, List<PeakAnnotation> annotations)
    {
        super.processPeak(mz, intensity, annotations);

        topNList.add(mz, intensity, annotations);
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<PeakAnnotation>> annotationMap)
    {
        for (int i = 0; i < mzList.size(); i++) {
            List<PeakAnnotation> annotations;
            if (annotationMap.containsKey(i))
                annotations = annotationMap.get(i);
            else
                annotations = Collections.emptyList();

            sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), annotations);
        }

        double mass = getPrecursor().getMass() + PeriodicTable.PROTON_MASS * 2;
        int charge = getPrecursor().getCharge();

        // add 1+ complementary ion
        topNList.resetCursor();
        while (topNList.next()) {
            double newMass = mass - topNList.currMz();
            if (newMass > MZ_LIMIT) {
                sink.processPeak(newMass, topNList.currIntensity(), topNList.currAnnotations());
            }
        }

        // add 2+ complementary ion
        if (addHighCharge && charge > 2) {
            mass = getPrecursor().getMass() + PeriodicTable.PROTON_MASS * 4;
            topNList.resetCursor();
            while (topNList.next()) {
                double newMass = mass - topNList.currMz();
                if (newMass > MZ_LIMIT)
                    sink.processPeak(newMass, topNList.currIntensity(), topNList.currAnnotations());
            }
        }
    }
}
