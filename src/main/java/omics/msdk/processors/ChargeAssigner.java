/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;


import omics.util.ms.Polarity;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DelayedPeakProcessor;

/**
 * Assign charge state for peaks in Spectrum.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 May 2017, 7:40 PM
 */
public abstract class ChargeAssigner<A extends PeakAnnotation> extends DelayedPeakProcessor<A, A>
{
    /**
     * mz tolerance of mz judge.
     */
    protected Tolerance iMzTol;
    /**
     * Polarity of charge.
     */
    protected Polarity iPolarity;

    protected int maxAbsCharge = 5;


    public ChargeAssigner(Tolerance aTol, Polarity aPolarity)
    {
        this.iMzTol = aTol;
        this.iPolarity = aPolarity;
    }

    /**
     * set the allowed maximum charge.
     *
     * @param charge a int value.
     */
    public void setMaxCharge(int charge)
    {
        this.maxAbsCharge = charge;
    }
}
