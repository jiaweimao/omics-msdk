/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.protein.ms.fragment.FragmentIonType;

/**
 * A peak annotation to add rank of the peak.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Jan 2019, 7:22 PM
 */
public class PeakRankAnnotation implements PeakAnnotation
{
    private int rank;

    public PeakRankAnnotation(int rank)
    {
        this.rank = rank;
    }

    /**
     * @return rank of the peak, 1-based value.
     */
    public int getRank()
    {
        return rank;
    }

    @Override
    public String getSymbol()
    {
        return Integer.toString(rank);
    }

    @Override
    public PeakRankAnnotation copy()
    {
        return new PeakRankAnnotation(rank);
    }

    /**
     * @return null for this
     */
    @Override
    public FragmentIonType getFragmentIonType()
    {
        return null;
    }
}
