/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.*;
import omics.msdk.util.MultiRange;
import omics.util.chem.PeriodicTable;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.DelayedSpectrumProcessor;
import omics.util.ms.peaklist.impl.PpmTolerance;
import omics.util.utils.ArrayUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

/**
 * Find complementary peaks in the spectrum, and do intensitification on these peaks.
 * <p>
 * De-isotoping and De-convolution is required before this.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 29 Oct 2018, 1:01 PM
 */
public class ComplementaryFinder extends DelayedSpectrumProcessor<PeakAnnotation, PeakAnnotation>
{
    /**
     * intensity strategy
     */
    public enum Intensify
    {
        /**
         * promote the intensity of the pair peaks to higher one
         */
        HIGHER,
        /**
         * promote the intensity of pair peaks to the base peak intensity
         */
        BASE_PEAK,
        /**
         * do no changed to pair peak intensity
         */
        NONE
    }

    private static final double PROTON_MASS = PeriodicTable.PROTON_MASS;
    /**
     * Path to the exclusion list (the default one contains all immonium ions > 50 Da)
     */
    private static String ignoreListPath;
    private Tolerance ignoreListPPM;
    private Tolerance parentMassTol;
    private Intensify intensify;
    private MultiRange exclusionList;

    /**
     * Constructor with default parameters.
     */
    public ComplementaryFinder()
    {
        this(new PpmTolerance(5), Intensify.HIGHER, new PpmTolerance(10));
    }

    /**
     * Constructor
     *
     * @param parentMassTol tolerance for match of precursor mass
     * @param intensify     type of {@link Intensify} strategy for complementary peaks
     * @param ignoreListTol tolerance for looking for ignored peaks
     */
    public ComplementaryFinder(Tolerance parentMassTol, Intensify intensify, Tolerance ignoreListTol)
    {
        this.parentMassTol = parentMassTol;
        this.ignoreListPPM = ignoreListTol;
        this.intensify = intensify;

        this.exclusionList = loadExcludes();
    }

    public static void setIgnoreListPath(String path)
    {
        ignoreListPath = path;
    }

    private MultiRange loadExcludes()
    {
        MultiRange exclusionList = new MultiRange();
        try {
            BufferedReader reader;
            if (ignoreListPath != null) {
                reader = new BufferedReader(new FileReader(ignoreListPath));
            } else {
                reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("cf.exclude")));
            }
            String line;
            while ((line = reader.readLine()) != null) {
                double mass = Double.parseDouble(line);
                exclusionList.add(ignoreListPPM.getMin(mass), ignoreListPPM.getMax(mass));
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return exclusionList;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap<List<PeakAnnotation>> annotationMap)
    {
        double baseIntensity = Double.MIN_VALUE;
        for (double in : intensityList) {
            if (in > baseIntensity) {
                baseIntensity = in;
            }
        }

        double[] mzArray = mzList.toDoubleArray();
        IntSet excludedSet = new IntOpenHashSet();
        for (int i = 0; i < exclusionList.size(); i += 2) {
            int[] ids = ArrayUtils.indexOf(mzArray, exclusionList.get(i), exclusionList.get(i + 1));
            for (int id : ids) {
                excludedSet.add(id);
            }
        }

        double precursorMass = getPrecursor().getMass() + 2 * PROTON_MASS;
        // map for complementary peaks
        Int2IntSortedMap map = new Int2IntRBTreeMap();
        int start = 0;
        int end = mzList.size() - 1;
        while (start < end) {
            if (excludedSet.contains(start)) {
                start++;
                continue;
            }

            if (excludedSet.contains(end)) { // although it seems impossible, but what we could encounter in the future is unpredictable
                end--;
                continue;
            }

            double mass = mzList.getDouble(start) + mzList.getDouble(end);
            Tolerance.Location location = parentMassTol.check(precursorMass, mass);

            switch (location) {
                case LARGER:
                    end--;
                    break;
                case SMALLER:
                    start++;
                    break;
                case WITHIN: {
                    map.put(start, end);
                    map.put(end, start);
                    start++;
                    end--;
                    break;
                }
            }
        }

        for (int i = 0; i < mzList.size(); i++) {

            List<PeakAnnotation> peakAnnotations = annotationMap.get(i);
            if (peakAnnotations == null)
                peakAnnotations = Collections.emptyList();

            if (map.containsKey(i)) {
                switch (intensify) {
                    case NONE:
                        sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), peakAnnotations);
                        break;
                    case BASE_PEAK:
                        sink.processPeak(mzList.getDouble(i), baseIntensity, peakAnnotations);
                        break;
                    case HIGHER:
                        int p2 = map.get(i);
                        double in = Math.max(intensityList.getDouble(i), intensityList.getDouble(p2));
                        sink.processPeak(mzList.getDouble(i), in, peakAnnotations);
                        break;
                }
            } else {
                sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), peakAnnotations);
            }
        }
    }
}
