/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.processors;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.chem.PeriodicTable;
import omics.util.ms.Polarity;
import omics.util.ms.peaklist.Tolerance;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This charge assigner is get from Morpheus. *
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 06 Jun 2017, 7:39 PM
 */
public class ChargeAssignerSimple extends ChargeAssigner
{
    private boolean harmonic;

    public ChargeAssignerSimple(Tolerance aTol, Polarity aPolarity, boolean harmonic)
    {
        super(aTol, aPolarity);
        this.harmonic = harmonic;
    }

    @Override
    protected void processCached(DoubleArrayList mzList, DoubleArrayList intensityList, Int2ObjectMap annotationMap)
    {
        int polar = iPolarity == Polarity.NEGATIVE ? -1 : 1;

        for (int i = 0; i < mzList.size(); i++) {
            int j = i + 1;
            IntList charges = new IntArrayList();
            while (j < mzList.size()) {

                if (iMzTol.isLarger(mzList.getDouble(i) + PeriodicTable.C12C13MassDiff, mzList.getDouble(j)))
                    break;

                for (int c = maxAbsCharge * polar; polar > 0 ? c >= 1 : c <= -1; c -= polar) {

                    if (harmonic) {

                        boolean harm = false;
                        for (int c2 : charges) {

                            if (c2 % c == 0) {
                                harm = true;
                                break;
                            }
                        }

                        if (harm)
                            continue;
                    }

                    if (iMzTol.withinTolerance(mzList.getDouble(j), mzList.getDouble(i) + PeriodicTable.C12C13MassDiff / c)) {
                        sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), Collections.singletonList(new PeakChargeAnnotation(c)));
                        charges.add(c);
                    }
                }
                j++;
            }

            if (charges.size() == 0)
                sink.processPeak(mzList.getDouble(i), intensityList.getDouble(i), new ArrayList<>());
        }
    }
}
