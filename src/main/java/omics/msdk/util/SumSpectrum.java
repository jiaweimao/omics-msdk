/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.util;

import omics.msdk.io.MgfReader;
import omics.util.io.FilenameUtils;
import omics.util.io.csv.CsvWriter;
import omics.util.ms.MsnSpectrum;

import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 13 May 2018, 11:13 PM
 */
public class SumSpectrum
{
    public static void main(String[] args) throws IOException
    {
        MsnSpectrum finalSpectrum = new MsnSpectrum();
        MgfReader reader = new MgfReader(new File(args[0]));
        while (reader.hasNext()) {
            MsnSpectrum spectrum = reader.next();
            for (int i = 0; i < spectrum.size(); i++) {
                double mz = spectrum.getX(i);
                double in = spectrum.getY(i);
                finalSpectrum.add((int) mz, in);
            }
        }
        reader.close();
        CsvWriter writer = new CsvWriter(FilenameUtils.removeExtension(args[0]) + ".csv");

        for (int i = 0; i < finalSpectrum.size(); i++) {
            writer.write(finalSpectrum.getX(i));
            writer.write(finalSpectrum.getY(i));
            writer.endRecord();
        }
        writer.close();
    }
}
