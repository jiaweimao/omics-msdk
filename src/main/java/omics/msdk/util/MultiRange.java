/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.util;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import omics.util.OmicsRuntimeException;

/**
 * Represents an range consisting of several subranges
 * eg. (1.0, 2.1), (5.02, 6.1)
 * <p>
 * NOTE: borders aren't included
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 01 Nov 2018, 9:59 PM
 */
public class MultiRange
{
    private DoubleArrayList points = new DoubleArrayList();

    public MultiRange() { }

    /**
     * @return lowest border of all ranges
     */
    public double getLowest()
    {
        if (points.size() > 0)
            return points.getDouble(0);
        else
            throw new OmicsRuntimeException("Empty MultiRange");
    }

    public double getHighest()
    {
        if (points.size() > 0)
            return points.getDouble(points.size() - 1);
        else
            throw new OmicsRuntimeException("Empty MultiRange");
    }

    /**
     * add a range
     */
    public void add(double lower, double upper)
    {
        if (lower >= upper)
            return;

        int first = 0; // position to insert values
        int last = 0;
        while (first < points.size() && lower > points.getDouble(first)) { // find position of lower
            first++;
        }

        last = first;
        while (last < points.size() && upper > points.getDouble(last)) { // find position of upper
            last++;
        }

        points.removeElements(first, last);
        if (first % 2 == 0 && last % 2 == 0) { // complete pair to the left and to the right
            points.add(first, upper);
            points.add(first, lower);
        } else if (first % 2 == 0 && last % 2 == 1) { // only pair to left
            points.add(first, lower);
        } else if (first % 2 == 1 && last % 2 == 0) {
            points.add(first, upper);
        }
    }

    /**
     * check if value is in multi range
     */
    public boolean isIn(double value)
    {
        if (points.size() % 2 == 1) // 必须成对
            throw new OmicsRuntimeException("Invalid range");

        for (int i = 0; i < points.size(); i += 2) {
            if (value > points.getDouble(i) && value < points.getDouble(i + 1))
                return true;
        }
        return false;
    }

    /**
     * 返回小于 value 的最大值的 index, -1 for none.
     *
     * @param start start test index
     * @param value value to compare
     */
    public int lastIndexBelow(int start, double value)
    {
        for (int i = start; i < points.size(); i++) {
            if (points.getDouble(i) > value)
                return i - 1;
        }
        return -1;
    }

    /**
     * return the index of the first value larger then given value.
     *
     * @param start start search index
     * @param value value to compare
     * @return index of the first value larger than test value, -1 for none.
     */
    public int firstIndexAbove(int start, double value)
    {
        for (int i = start; i < points.size(); i++) {
            if (points.getDouble(i) > value)
                return i;
        }
        return -1;
    }

    /**
     * return value at given index
     *
     * @param index point index
     */
    public double get(int index)
    {
        return points.getDouble(index);
    }


    /**
     * @return all points in the range
     */
    public double[] getPoints()
    {
        return points.toDoubleArray();
    }

    /**
     * @return points count
     */
    public int size()
    {
        return points.size();
    }

    @Override
    public String toString()
    {
        return "MultiRange{" +
                "points=" + points +
                '}';
    }
}
