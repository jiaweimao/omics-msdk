/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.util;

import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumType;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.ArrayList;
import java.util.List;


/**
 * Auto-detection of spectrum type from data points. Determines if the spectrum represented by given
 * array of data points is centroided or continuous (profile or thresholded).
 * Profile spectra are easy to detect, because they contain zero-intensity data points.
 * However, distinguishing centroided from thresholded spectra is not trivial. We use multiple checks for that purpose,
 * as described in the code comments.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Jul 2017, 9:17 PM
 */
public class SpectrumTypeDetector
{
    /**
     * <p>
     * detectSpectrumType.
     * </p>
     *
     * @param msSpectrum a {@link MsnSpectrum} object.
     * @return a {@link SpectrumType} object.
     */
    public static SpectrumType detectSpectrumType(MsnSpectrum msSpectrum)
    {
        double[] mzValues = msSpectrum.getXs();
        double[] intensityValues = msSpectrum.getYs();
        Integer size = msSpectrum.size();
        return detectSpectrumType(mzValues, intensityValues, size);
    }

    public static SpectrumType detectSpectrumType(double[] mzValues, double[] intensityValues, Integer size)
    {
        // If the spectrum has less than 5 data points, it should be centroided.
        if (size < 5)
            return SpectrumType.CENTROIDED;

        List<Double> diffs = new ArrayList<>();
        for (int i = 1; i < size; i++) {
            if (intensityValues[i] == 0f) continue;
            double diff = mzValues[i] - mzValues[i - 1];
            diffs.add(diff);
        }
        DescriptiveStatistics ds = new DescriptiveStatistics();
        for (Double d : diffs) ds.addValue(d);

        double dev = ds.getStandardDeviation();
        double err = dev / ds.getMean();
        if (err >= 0.025)
            return SpectrumType.CENTROIDED;
        else
            return SpectrumType.PROFILE;
    }

    /**
     * <p>
     * detectSpectrumType.
     * </p>
     *
     * @param mzValues        an array of double.
     * @param intensityValues an array of float.
     * @param size            a {@link java.lang.Integer} object.
     * @return a {@link SpectrumType} object.
     */
    public static SpectrumType detectSpectrumTypeOld(double mzValues[], float intensityValues[], Integer size)
    {
        // If the spectrum has less than 5 data points, it should be centroided.
        if (size < 5)
            return SpectrumType.CENTROIDED;

        int basePeakIndex = 0;
        boolean hasZeroDataPoint = false;

        final double scanMzSpan = mzValues[size - 1] - mzValues[0];

        // Go through the data points and find the highest one
        for (int i = 0; i < size; i++) {

            // Update the topDataPointIndex accordingly
            if (intensityValues[i] > intensityValues[basePeakIndex])
                basePeakIndex = i;

            if (intensityValues[i] == 0.0)
                hasZeroDataPoint = true;
        }

        // Find the all data points around the base peak that have intensity
        // above half maximum
        final double halfIntensity = intensityValues[basePeakIndex] / 2.0;
        int leftIndex = basePeakIndex;
        while ((leftIndex > 0)
                && intensityValues[leftIndex - 1] > halfIntensity) {
            leftIndex--;
        }
        int rightIndex = basePeakIndex;
        while ((rightIndex < size - 1)
                && intensityValues[rightIndex + 1] > halfIntensity) {
            rightIndex++;
        }
        final double mainPeakMzSpan = mzValues[rightIndex]
                - mzValues[leftIndex];
        final int mainPeakDataPointCount = rightIndex - leftIndex + 1;

        // If the main peak has less than 3 data points above half intensity, it
        // indicates a centroid spectrum. Further, if the m/z span of the main
        // peak is more than 0.1% of the scan m/z range, it also indicates a
        // centroid spectrum. These criteria are empirical and probably not
        // bulletproof. However, it works for all the test cases we have.
        if ((mainPeakDataPointCount < 3)
                || (mainPeakMzSpan > (scanMzSpan / 1000.0)))
            return SpectrumType.CENTROIDED;
        else {
            return SpectrumType.PROFILE;
        }
    }
}
