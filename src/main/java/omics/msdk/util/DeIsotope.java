/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.util;


import omics.msdk.processors.PeakChargeAnnotation;
import omics.util.chem.IonTools;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakList;
import omics.util.ms.peaklist.impl.DoublePeakList;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 14 Mar 2018, 9:17 PM
 */
public class DeIsotope {

    /**
     * Convert the Peaks in the Peak list to 1+ peaks.
     *
     * @param peakList a PeakList
     * @return deisotoped peaks.
     */
    public static PeakList<PeakAnnotation> deisotope(PeakList<PeakChargeAnnotation> peakList) {
        PeakList<PeakAnnotation> pl = new DoublePeakList<>();
        for (int i = 0; i < peakList.size(); i++) {
            PeakChargeAnnotation firstAnnotation = peakList.getFirstAnnotation(i);
            if (firstAnnotation != null) {
                int charge = firstAnnotation.getCharge();
                if (charge == 1) {
                    pl.add(peakList.getX(i), peakList.getY(i));
                } else {
                    pl.add(IonTools.calcMz(IonTools.calcNeutralMolecularMass(peakList.getX(i), charge), 1), peakList.getY(i));
                }
            }
        }

        return pl;
    }
}
