/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSHeader;
import omics.util.ms.Chromatogram;
import omics.util.ms.MsnSpectrum;

import java.io.IOException;

/**
 * Interface for class to output data.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 19 Sep 2018, 8:49 AM
 */
public interface MSWriter {

    /**
     * do some init work for output.
     */
    void writeStartDocument() throws Exception;

    /**
     * file header information.
     *
     * @param header {@link MSHeader}
     */
    void writeFileHeader(MSHeader header) throws Exception;

    /**
     * start of output of spectra.
     */
    void writeStartSpectrumList() throws IOException;

    /**
     * writer a {@link MsnSpectrum}
     *
     * @param spectrum {@link MsnSpectrum}
     */
    void writeSpectrum(MsnSpectrum spectrum) throws IOException;

    /**
     * end of output of spectra.
     */
    void writeEndSpectrumList() throws IOException;

    /**
     * start of output of chromatogram list.
     */
    void writeStartChromatogramList() throws IOException;

    /**
     * writer a {@link Chromatogram} to the output stream.
     *
     * @param chromatogram {@link Chromatogram}
     */
    void writeChromatogram(Chromatogram chromatogram) throws IOException;

    /**
     * end of output of chromatogram list.
     */
    void writeEndChromatogramList() throws IOException;

    /**
     * end of document.
     */
    void writeEndDocument() throws IOException;

    /**
     * close the output stream.
     */
    void close() throws IOException;
}
