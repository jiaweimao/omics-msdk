/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.msdk.io.MgfReader;
import omics.msdk.io.MgfWriter;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Precision;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * To combine multiple mgf files into a single one.
 *
 * @author JiaweiMao
 * @version 1.1.1
 */
public class MgfFilesMerger
{
    private Pattern mNamePattern;

    public MgfFilesMerger(String pattern)
    {
        this.mNamePattern = Pattern.compile(pattern);
    }

    public void merge(File folder, File out)
    {
        try {
            File[] files = folder.listFiles();
            if (files == null)
                return;

            MgfWriter writer = new MgfWriter(out);
            for (File file : files) {
                String name = file.getName();
                Matcher matcher = mNamePattern.matcher(name);
                if (matcher.matches()) {
                    System.out.println(name);

                    MgfReader reader = new MgfReader(file, Precision.DOUBLE);
                    while (reader.hasNext()) {
                        MsnSpectrum spectrum = reader.next();
                        writer.writeSpectrum(spectrum);
                    }
                }
            }
            System.out.println("Generating: " + out.getName() + "\n\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
