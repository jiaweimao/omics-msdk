/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.utils.ZipUtils;

import javax.xml.stream.XMLStreamException;
import java.nio.ByteBuffer;
import java.util.Base64;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 24 Sep 2018, 11:11 AM
 */
public class Base64Utils {

    public static final Base64.Decoder decoder = Base64.getDecoder();
    public static final Base64.Encoder encoder = Base64.getEncoder();

    public static void decode(String data, boolean compressed, int bitPrecision, int expectPeakCount, double[] mzs, double[] intensities) throws XMLStreamException {
        byte[] bytes = decoder.decode(data);
        if (compressed) {
            bytes = ZipUtils.decompressByInflater(bytes);
        }

        // m/z + int
        int bytePrecision = bitPrecision / 8;
        int expectedByteCount = expectPeakCount * bytePrecision * 2;

        if (bytes.length != expectedByteCount) {
            throw new XMLStreamException("peak bytes expected=" + expectedByteCount + ", actual=" + bytes.length);
        }

        if (bitPrecision == 32) {
            convert32bitsAndPopulate(bytes, mzs, intensities, expectPeakCount);
        } else {
            convert64bitsAndPopulate(bytes, mzs, intensities, expectPeakCount);
        }
    }

    private static void convert32bitsAndPopulate(byte[] bytes, double[] mzs, double[] intensities, int expectPeakCount) {

        checkNotNull(bytes);
        checkArgument(bytes.length % 4 == 0);
        checkArgument(mzs.length == intensities.length);

        int len = bytes.length / 8;
        checkArgument(len == expectPeakCount);

        ByteBuffer buff = ByteBuffer.wrap(bytes);
        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {
            mzs[i] = buff.getFloat();
            intensities[i++] = buff.getFloat();
        }
    }

    private static void convert64bitsAndPopulate(byte[] bytes, double[] mzs, double[] intensities, int expectPeakCount) {
        checkNotNull(bytes);
        checkArgument(bytes.length % 8 == 0);
        checkArgument(mzs.length == intensities.length);

        int len = bytes.length / 16;
        checkArgument(len == expectPeakCount);

        ByteBuffer buff = ByteBuffer.wrap(bytes);
        buff.clear();

        int i = 0;
        while (buff.hasRemaining()) {
            mzs[i] = buff.getDouble();
            intensities[i++] = buff.getDouble();
        }
    }
}
