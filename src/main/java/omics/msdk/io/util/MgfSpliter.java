/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.OmicsTask;
import omics.util.io.FileType;
import omics.util.io.FilenameUtils;

import java.io.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Mar 2018, 2:33 PM
 */
public class MgfSpliter extends OmicsTask<Void>
{
    private String mgfPath;
    private int iFileSize = 500 * 1024 * 1024;
    private int iMaxFileCount = Integer.MAX_VALUE;

    public MgfSpliter(String mgfPath)
    {
        this.mgfPath = mgfPath;
    }

    public static void main(String[] args) throws Exception
    {
        File file = new File("Z:\\MaoJiawei\\o-glycan\\20180827_NCE\\mgf_deglycosylation");
        for (File mgf : FileType.MGF.listFiles(file)) {
            MgfSpliter spliter = new MgfSpliter(mgf.getAbsolutePath());
            spliter.start();
        }
    }

    public void setFileSize(int iFileSize)
    {
        this.iFileSize = iFileSize;
    }

    public void setMaxFileCount(int iMaxFileCount)
    {
        this.iMaxFileCount = iMaxFileCount;
    }

    @Override
    public void start() throws Exception
    {
        File mgfFile = new File(mgfPath);
        System.out.println(mgfFile.getName());

        long length = mgfFile.length();

        double ratio = length / (double) iFileSize;
        int count = (int) ratio;
        if ((ratio - count) > 0.2)
            count++;

        if (count > iMaxFileCount)
            count = iMaxFileCount;

        BufferedReader reader = new BufferedReader(new FileReader(mgfFile));

        int fileCount = 1;
        int scanCount = 0;
        long scanSize = 0;

        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(FilenameUtils.appendSuffix(mgfPath, "-" + fileCount))));

        String line;
        while ((line = reader.readLine()) != null) {

            writer.println(line);
            scanSize += line.length();

            if (line.equals("END IONS")) {
                scanCount++;

                if (scanSize > iFileSize && fileCount < count) {
                    System.out.println("File " + fileCount + ": " + scanCount);
                    scanCount = 0;
                    scanSize = 0;

                    fileCount++;
                    writer.flush();
                    writer.close();

                    writer = new PrintWriter(new BufferedWriter(new FileWriter(FilenameUtils.appendSuffix(mgfPath, "-" + fileCount))));
                }
            }
        }

        System.out.println("File " + fileCount + ": " + scanCount);
        writer.close();
        reader.close();
    }
}
