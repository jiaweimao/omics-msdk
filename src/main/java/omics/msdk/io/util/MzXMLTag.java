/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Sep 2018, 11:07 PM
 */
public interface MzXMLTag
{
    String TAG_MZXML = "mzXML";

    String TAG_MS_RUN = "msRun";

    String ATTR_SCAN_COUNT = "scanCount";

    String ATTR_SCHEMA_LOCATION = "schemaLocation";

    String TAG_PARENT_FILE = "parentFile";

    String TAG_SCAN = "scan";

    String ATTR_FILE_NAME = "fileName";

    String ATTR_FILE_TYPE = "fileType";

    String ATTR_FILE_SHA1 = "fileSha1";

    String TAG_MS_INSTRUMENT = "msInstrument";

    String ATTR_INSTRUMENT_ID = "msInstrumentID";

    String TAG_SOFTWARE = "software";

    String ATTR_NAME = "name";

    String ATTR_VERSION = "version";

    String TAG_MANUFACTURER = "msManufacturer";

    String TAG_MS_MODEL = "msModel";

    String TAG_IONISATION = "msIonisation";

    String TAG_MASS_ANALYZER = "msMassAnalyzer";

    String TAG_DETECTOR = "msDetector";

    String TAG_OPERATOR = "operator";

    String TAG_RESOLUTION = "msResolution";

    String ATTR_CATEGORY = "category";

    String ATTR_VALUE = "value";

    String TAG_DATA_PROCESSING = "dataProcessing";

    /**
     * intensityCutoff (optional): this is the minimal intensity value for an m/z - intensity pair to be
     * included into the current mzXML instance document.
     */
    String ATTR_INTENSITY_CUTOFF = "intensityCutoff";

    /**
     * centroided (optional): if equal to 1 the data in the current mzXML instance document has been
     * centroided
     */
    String ATTR_CENTROIDED = "centroided";

    /**
     * deisotoped (optional): if equal to 1 the data in the current mzXML instance document has been
     * deisotoped.
     */
    String ATTR_DEISOTOPED = "deisotoped";
    /**
     * chargeDeconvoluted (optional); if equal to 1 the charge states in the current mzXML instance
     * document have been deconvoluted.
     */
    String ATTR_CHARGE_DECONVOLUTED = "chargeDeconvoluted";

    /**
     * this attribute is specific for LC-MALDI experiments and indicates if
     * peaks eluting over multiple spots have been integrated into a single spot.
     */
    String ATTR_SPOT_INTEGRATION = "spotIntegration";

    String ATTR_TYPE = "type";

    String TAG_PROCESSING_OPERATION = "processingOperation";

    String ATTR_NUM = "num";

    String ATTR_MS_LEVEL = "msLevel";

    String ATTR_RETENTION_TIME = "retentionTime";

    String ATTR_COLLISION_ENERGY = "collisionEnergy";

    String ATTR_SCAN_TYPE = "scanType";

    String TAG_PRECURSOR_MZ = "precursorMz";

    String TAG_PRECURSOR_INTENSITY = "precursorIntensity";

    String ATTR_POLARITY = "polarity";

    String ATTR_PRECURSOR_SCAN = "precursorScanNum";

    String ATTR_PRECURSOR_CHARGE = "precursorCharge";

    String ATTR_ACTIVATION_METHOD = "activationMethod";

    String TAG_PEAKS = "peaks";

    String ATTR_PEAKS_COUNT = "peaksCount";

    String ATTR_PRECISION = "precision";

    String ATTR_BYTE_ORDER = "byteOrder";

    String ATTR_CONTENT_TYPE = "contentType";

    String ATTR_COMPRESSED_LEN = "compressedLen";

    String ATTR_START_MZ = "startMz";

    String ATTR_END_MZ = "endMz";

    String ATTR_BASE_PEAK_MZ = "basePeakMz";

    String ATTR_BASE_PEAK_INTENSITY = "basePeakIntensity";

    String ATTR_TOT_ION_CURRENT = "totIonCurrent";

    String ATTR_COMPRESSION_TYPE = "compressionType";
}
