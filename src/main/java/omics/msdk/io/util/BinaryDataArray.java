/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.msdk.model.ParamGroup;
import omics.util.OmicsException;
import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;
import omics.util.utils.ArrayUtils;
import omics.util.utils.ByteUtils;
import omics.util.utils.ZipUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Aug 2018, 2:15 PM
 */
public class BinaryDataArray extends ParamGroup {
    private CompressionType compressionType;
    private int encodedLength;
    private int arrayLength;
    private BitLength bitLength;
    private DataType dataType;
    /**
     * encoded binary data
     */
    private byte[] binary;

    public BinaryDataArray() {
    }

    /**
     * Constructor.
     *
     * @param encodedLength byte array length after encode
     * @param arrayLength   data array length
     */
    public BinaryDataArray(int encodedLength, int arrayLength) {
        this.encodedLength = encodedLength;
        this.arrayLength = arrayLength;
    }

    /**
     * @return {@link DataType} of this binary data array
     */
    public DataType getDataType() {
        if (dataType == null) {
            for (CvParam cvParam : getCvParams()) {
                for (DataType dataType : DataType.values()) {
                    if (cvParam.getCvTerm().getAccession().equals(dataType.getAccession())) {
                        this.dataType = dataType;
                        return this.dataType;
                    }
                }
            }

            this.dataType = DataType.UNKNOWN;
        }
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
        CvTerm cvTerm = new CvTerm(Cv.PSI_MS, dataType.getAccession(), dataType.getName());
        if (dataType == DataType.MZ) {
            cvTerm.setUnitCv(Cv.PSI_MS);
            cvTerm.setUnitAccession(CvTermList.ACC_UNIT_MZ);
            cvTerm.setUnitName(CvTermList.NAME_UNIT_MZ);
        } else if (dataType == DataType.INTENSITY) {
            cvTerm.setUnitCv(Cv.PSI_MS);
            cvTerm.setUnitAccession(CvTermList.ACC_UNIT_INTENSITY);
            cvTerm.setUnitName(CvTermList.NAME_UNIT_INTENSITY);
        } else if (dataType == DataType.TIME) {
            cvTerm.setUnitCv(Cv.UNIT_ONTOLOGY);
            cvTerm.setUnitAccession(CvTermList.ACC_UNIT_MIN);
            cvTerm.setUnitName(CvTermList.NAME_UNIT_MIN);
        }
        addCvParam(new CvParam(cvTerm, ""));
    }

    public BitLength getBitLength() {
        if (bitLength == null) {
            List<CvParam> cvParams = getCvParams();
            for (CvParam cvParam : cvParams) {
                for (BitLength bitLength : BitLength.values()) {
                    if (cvParam.getCvTerm().getAccession().equals(bitLength.getAccession())) {
                        this.bitLength = bitLength;
                        return this.bitLength;
                    }
                }
            }
            this.bitLength = BitLength.UNKNOWN;
        }
        return bitLength;
    }

    public void setBitLength(BitLength bitLength) {
        this.bitLength = bitLength;
    }

    public CompressionType getCompressionType() {
        if (compressionType == null) {
            for (CvParam cvParam : getCvParams()) {
                for (CompressionType compressionType : CompressionType.values()) {
                    if (compressionType.getAccession().equals(cvParam.getCvTerm().getAccession())) {
                        this.compressionType = compressionType;
                        return this.compressionType;
                    }
                }
            }

            this.compressionType = CompressionType.UNKNOWN;
        }
        return compressionType;
    }

    public void setCompressionType(CompressionType compressionType) {
        this.compressionType = compressionType;
    }


    public int getEncodedLength() {
        return encodedLength;
    }

    public int getArrayLength() {
        return arrayLength;
    }

    public String getBinaryDataAsString() {
        return new String(binary, StandardCharsets.US_ASCII);
    }

    public void setStringAsBinaryData(String value) {
        byte[] bytes = value.getBytes(StandardCharsets.US_ASCII);
        setBinary(bytes);
    }

    public byte[] getBinary() {
        return binary;
    }

    public void setBinary(byte[] binary) {
        this.binary = binary;
    }

    /**
     * set values
     *
     * @param values          double array
     * @param compressionType {@link CompressionType}
     */
    public void setValues(double[] values, CompressionType compressionType) {
        checkNotNull(values);
        checkArgument(values.length > 0);
        this.compressionType = compressionType;
        this.arrayLength = values.length;

        byte[] encodedData;
        if (CompressionType.isNumpressType(compressionType)) {
            encodedData = MSNumpress.encode(values, compressionType);
        } else {
            ByteBuffer buffer = ByteBuffer.allocate(values.length * 8);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            for (double value : values) {
                buffer.putDouble(value);
            }
            encodedData = buffer.array();
        }

        byte[] output;
        if (CompressionType.isCompressType(compressionType)) {
            output = ZipUtils.compressByDeflater(encodedData);
        } else {
            output = encodedData;
        }

        byte[] encodeBytes = Base64.getEncoder().encode(output);
        setBinary(encodeBytes);

        this.encodedLength = encodeBytes.length;

        BitLength bitLength = BitLength.FLOAT64BIT;
        CvParam cvParam = new CvParam(new CvTerm(Cv.PSI_MS, bitLength.getAccession(), bitLength.getName()), "");
        addCvParam(cvParam);

        CvParam param = new CvParam(new CvTerm(Cv.PSI_MS, compressionType.getAccession(), compressionType.getName()), "");
        addCvParam(param);
    }

    public double[] decode2Double() throws OmicsException {
        if (encodedLength == 0)
            return new double[0];

        byte[] data = Base64.getDecoder().decode(binary);

        // inflation should be before NumPress
        if (CompressionType.isCompressType(getCompressionType()))
            data = ZipUtils.decompressByInflater(data);

        if (CompressionType.isNumpressType(compressionType))
            return MSNumpress.decode(compressionType, data);

        BitLength bitLength = getBitLength();
        if (bitLength == BitLength.NTSTRING || bitLength == null)
            throw new IllegalArgumentException("Bit type " + bitLength + " is not supported.");

        switch (getBitLength()) {
            case INT32BIT:
                int[] ints = ByteUtils.bytes2Ints(data, ByteOrder.LITTLE_ENDIAN);
                return ArrayUtils.int2double(ints, null);
            case FLOAT32BIT:
                float[] floats = ByteUtils.bytes2Floats(data, ByteOrder.LITTLE_ENDIAN);
                return ArrayUtils.float2double(floats, null);
            case FLOAT64BIT:
                return ByteUtils.bytes2Doubles(data, ByteOrder.LITTLE_ENDIAN);
            case INT64BIT:
                long[] longs = ByteUtils.bytes2Longs(data, ByteOrder.LITTLE_ENDIAN);
                return ArrayUtils.long2double(longs, null);
            default:
                throw new OmicsException("Bit length is not supported: " + bitLength);
        }
    }
}
