/*
 *  Copyright 2019 JiaweiMao jiaweiM_philo@hotmail.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;

/**
 * a list of CvTerm for easy use.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Aug 2019, 10:42 AM
 */
public interface CvTermList {

    // Polarity of the scan
    String ACC_NEGATIVE_SCAN = "MS:1000129";
    String ACC_POSITIVE_SCAN = "MS:1000130";

    CvParam NEGATIVE_SCAN = new CvParam(new CvTerm(ACC_NEGATIVE_SCAN, "negative scan"), "");
    CvParam POSITIVE_SCAN = new CvParam(new CvTerm(ACC_POSITIVE_SCAN, "positive scan"), "");

    // Centroid vs profile
    String ACC_CENTROID_SPECTRUM = "MS:1000127";
    String ACC_PROFILE_SPECTRUM = "MS:1000128";
    CvParam CENTROID_SPECTRUM = new CvParam(new CvTerm(ACC_CENTROID_SPECTRUM, "centroid spectrum"), "");
    CvParam PROFILE_SPECTRUM = new CvParam(new CvTerm(ACC_PROFILE_SPECTRUM, "profile spectrum"), "");

    // it is the m/z unit accession, but somepeople put it as the precursor accession.
    String ACC_UNIT_MZ = "MS:1000040";
    String ACC_UNIT_INTENSITY = "MS:1000131";
    String ACC_UNIT_ELECTRONVOLT = "UO:0000266";
    String NAME_UNIT_MZ = "m/z";
    String NAME_UNIT_INTENSITY = "number of detector counts";
    String NAME_UNIT_ELECTRONVOLT = "electronvolt";
    String ACC_UNIT_SECOND = "UO:0000010";
    String ACC_UNIT_MIN = "UO:0000031";
    String NAME_UNIT_MIN = "minute";
    String ACC_UNIT_MILLISECOND = "UO:0000028";
    String NAME_UNIT_MILLISECOND = "millisecond";

    /**
     * M/z value of the signal of highest intensity in the mass spectrum.
     */
    String ACC_BASEPEAK_MZ = "MS:1000504";
    String NAME_BASEPEAK_MZ = "base peak m/z";
    String ACC_BASEPEAK_INTENSITY = "MS:1000505";
    String NAME_BASEPEAK_INTENSITY = "base peak intensity";
    CvTerm BASEPEAK_MZ = new CvTerm(Cv.PSI_MS, ACC_BASEPEAK_MZ, NAME_BASEPEAK_MZ, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);
    CvTerm BASEPEAK_INTENSITY = new CvTerm(Cv.PSI_MS, ACC_BASEPEAK_INTENSITY, NAME_BASEPEAK_INTENSITY, Cv.PSI_MS, ACC_UNIT_INTENSITY, NAME_UNIT_INTENSITY);

    /**
     * total ion current of spectrum
     */
    String ACC_TOTAL_ION_CURRENT = "MS:1000285";
    String NAME_TOTAL_ION_CURRENT = "total ion current";
    CvTerm TOTAL_ION_CURRENT = new CvTerm(ACC_TOTAL_ION_CURRENT, NAME_TOTAL_ION_CURRENT);

    /**
     * Highest and lowest m/z value observed in the m/z array.
     */
    String ACC_HIGHEST_OBSERVED_MZ = "MS:1000527";
    String NAME_HIGHEST_OBSERVED_MZ = "highest observed m/z";
    String ACC_LOWEST_OBSERVED_MZ = "MS:1000528";
    String NAME_LOWEST_OBSERVED_MZ = "lowest observed m/z";

    CvTerm HIGHEST_OBSERVED_MZ = new CvTerm(Cv.PSI_MS, ACC_HIGHEST_OBSERVED_MZ, NAME_HIGHEST_OBSERVED_MZ, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);
    CvTerm LOWEST_OBSERVED_MZ = new CvTerm(Cv.PSI_MS, ACC_LOWEST_OBSERVED_MZ, NAME_LOWEST_OBSERVED_MZ, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);

    /**
     * A free-form text title describing a spectrum.
     */
    String ACC_SPECTRUM_TITLE = "MS:1000796";
    String NAME_SPECTRUM_TITLE = "spectrum title";
    CvTerm SPECTRUM_TITLE = new CvTerm(ACC_SPECTRUM_TITLE, NAME_SPECTRUM_TITLE);

    // scan start time
    String ACC_SCAN_START_TIME = "MS:1000016";
    String NAME_SCAN_START_TIME = "scan start time";
    CvTerm SCAN_START_TIME = new CvTerm(Cv.PSI_MS, ACC_SCAN_START_TIME, NAME_SCAN_START_TIME, Cv.UNIT_ONTOLOGY, ACC_UNIT_MIN, NAME_UNIT_MIN);

    // Scan filter string: A string unique to Thermo instrument describing instrument settings for the scan.
    String ACC_SCAN_FILTER_STRING = "MS:1000512";
    CvTerm SCAN_FILTER_STRING = new CvTerm(ACC_SCAN_FILTER_STRING, "filter string");

    String ACC_PRESET_SCAN_CONFIGURATION = "MS:1000616";
    String NAME_PRESET_SCAN_CONFIGURATION = "preset scan configuration";
    CvTerm PRESET_SCAN_CONFIGURATION = new CvTerm(ACC_PRESET_SCAN_CONFIGURATION, NAME_PRESET_SCAN_CONFIGURATION);

    CvParam MS1 = new CvParam(new CvTerm("MS:1000579", "MS1 spectrum"), "");
    CvParam MSn = new CvParam(new CvTerm("MS:1000580", "MSn spectrum"), "");

    /**
     * Stages of ms achieved in a multi stage mass spectrometry experiment.
     */
    String ACC_MS_LEVEL = "MS:1000511";
    String NAME_MS_LEVEL = "ms level";
    CvTerm MS_LEVEL = new CvTerm(ACC_MS_LEVEL, NAME_MS_LEVEL);

    // ion injection time: "The length of time spent filling an ion trapping device." [PSI:MS]
    String ACC_ION_INJECTION_TIME = "MS:1000927";
    String NAME_ION_INJECTION_TIME = "ion injection time";
    CvTerm ION_INJECTION_TIME = new CvTerm(Cv.PSI_MS, ACC_ION_INJECTION_TIME, NAME_ION_INJECTION_TIME, Cv.UNIT_ONTOLOGY, ACC_UNIT_MILLISECOND, NAME_UNIT_MILLISECOND);

    // The m/z bound of a mass spectrometer scan window." [PSI:MS]
    String ACC_SCAN_WINDOW_LOWER_LIMIT = "MS:1000501";
    String NAME_SCAN_WINDOW_LOWER_LIMIT = "scan window lower limit";
    String ACC_SCAN_WINDOW_UPPER_LIMIT = "MS:1000500";
    String NAME_SCAN_WINDOW_UPPER_LIMIT = "scan window upper limit";
    CvTerm SCAN_WINDOW_LOWER_LIMIT = new CvTerm(Cv.PSI_MS, ACC_SCAN_WINDOW_LOWER_LIMIT, NAME_SCAN_WINDOW_LOWER_LIMIT, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);
    CvTerm SCAN_WINDOW_UPPER_LIMIT = new CvTerm(Cv.PSI_MS, ACC_SCAN_WINDOW_UPPER_LIMIT, NAME_SCAN_WINDOW_UPPER_LIMIT, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);

    /**
     * The primary or reference m/z about which the isolation window is defined."
     */
    String ACC_ISOLATION_WINDOW_TARGET = "MS:1000827";
    String NAME_ISOLATION_WINDOW_TARGET = "isolation window target m/z";
    CvTerm ISOLATION_WINDOW_TARGET = new CvTerm(ACC_ISOLATION_WINDOW_TARGET, NAME_ISOLATION_WINDOW_TARGET);

    /**
     * The extent of the isolation window in m/z below the isolation window target m/z.
     * The lower and upper offsets may be asymmetric about the target m/z.
     */
    String ACC_ISOLATION_WINDOW_LOWER = "MS:1000828";
    String NAME_ISOLATION_WINDOW_LOWER = "isolation window lower offset";
    CvTerm ISOLATION_WINDOW_LOWER = new CvTerm(ACC_ISOLATION_WINDOW_LOWER, NAME_ISOLATION_WINDOW_LOWER);

    /**
     * The extent of the isolation window in m/z above the isolation window target m/z.
     * The lower and upper offsets may be asymmetric about the target m/z.
     */
    String ACC_ISOLATION_WINDOW_UPPER = "MS:1000829";
    String NAME_ISOLATION_WINDOW_UPPER = "isolation window upper offset";
    CvTerm ISOLATION_WINDOW_UPPER = new CvTerm(ACC_ISOLATION_WINDOW_UPPER, NAME_ISOLATION_WINDOW_UPPER);
    // is obsolete, but used by some software
    String ACC_ISOLATION_WIDTH = "MS:1000023";

    // selectedIon
    String ACC_SELECTED_ION_MZ = "MS:1000744";
    String ACC_SELECTED_ION_CHARGE_STATE = "MS:1000041";
    String ACC_SELECTED_ION_PEAK_INTENSITY = "MS:1000042";

    String NAME_SELECTED_ION_MZ = "selected ion m/z";
    String NAME_SELECTED_ION_CHARGE_STATE = "charge state";
    String NAME_SELECTED_ION_PEAK_INTENSITY = "peak intensity";

    CvTerm SELECTED_ION_MZ = new CvTerm(Cv.PSI_MS, ACC_SELECTED_ION_MZ, NAME_SELECTED_ION_MZ, Cv.PSI_MS, ACC_UNIT_MZ, NAME_UNIT_MZ);
    CvTerm SELECTED_ION_CHARGE_STATE = new CvTerm(ACC_SELECTED_ION_CHARGE_STATE, NAME_SELECTED_ION_CHARGE_STATE);
    CvTerm SELECTED_ION_PEAK_INTENSITY = new CvTerm(Cv.PSI_MS, ACC_SELECTED_ION_PEAK_INTENSITY, NAME_SELECTED_ION_PEAK_INTENSITY, Cv.PSI_MS, ACC_UNIT_INTENSITY, NAME_UNIT_INTENSITY);

    //region activation
    String ACC_COLLISION_ENERGY = "MS:1000045";
    String NAME_COLLISION_ENERGY = "collision energy";
    CvTerm COLLISION_ENERGY = new CvTerm(Cv.PSI_MS, ACC_COLLISION_ENERGY, NAME_COLLISION_ENERGY, Cv.UNIT_ONTOLOGY, ACC_UNIT_ELECTRONVOLT, NAME_UNIT_ELECTRONVOLT);

    String ACC_PEAK_LIST_SCANS = "MS:1000797";
    String NAME_PEAK_LIST_SCANS = "peak list scans";
    String ACC_PEAK_LIST_SCANS_OLD = "MS:1001115";
    String NAME_PEAK_LIST_SCANS_OLD = "scan number(s)";

    CvTerm PEAK_LIST_SCANS = new CvTerm(ACC_PEAK_LIST_SCANS, NAME_PEAK_LIST_SCANS);
    CvTerm PEAK_LIST_SCANS_OLD = new CvTerm(ACC_PEAK_LIST_SCANS_OLD, NAME_PEAK_LIST_SCANS_OLD);

    CvTerm RETENTION_TIME = new CvTerm("MS:1000894", "retention time");
}
