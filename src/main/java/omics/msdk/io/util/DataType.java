/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Aug 2018, 3:31 PM
 */
public enum DataType
{
    MZ("MS:1000514", "m/z array"),
    INTENSITY("MS:1000515", "intensity array"),
    TIME("MS:1000595", "time array"),
    UNKNOWN("");

    private String accession;
    private String name;


    DataType(String accession)
    {
        this.accession = accession;
    }

    DataType(String accession, String name)
    {
        this.accession = accession;
        this.name = name;
    }

    public String getAccession()
    {
        return accession;
    }

    public String getName()
    {
        return name;
    }
}
