/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

/**
 * Enumeration of different compression types which are parsed by the MzML Parser
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 26 Aug 2018, 10:31 PM
 */
public enum CompressionType
{
    NUMPRESS_LINPRED("MS:1002312", "MS-Numpress linear prediction compression"), //
    NUMPRESS_POSINT("MS:1002313", "MS-Numpress positive integer compression"), //
    NUMPRESS_SHLOGF("MS:1002314", "MS-Numpress short logged float compression"), //

    ZLIB("MS:1000574", "zlib compression"),
    NO_COMPRESSION("MS:1000576", "no compression"),

    NUMPRESS_LINPRED_ZLIB("MS:1002746", "MS-Numpress linear prediction compression followed by zlib compression"),
    NUMPRESS_POSINT_ZLIB("MS:1002747", "MS-Numpress positive integer compression followed by zlib compression"),
    NUMPRESS_SHLOGF_ZLIB("MS:1002748", "MS-Numpress short logged float compression followed by zlib compression"),
    UNKNOWN("", "");

    private String accession;
    private String name;

    CompressionType(String accession, String name)
    {
        this.accession = accession;
        this.name = name;
    }

    public static boolean isNumpressType(CompressionType type)
    {
        return type != UNKNOWN && type != ZLIB && type != NO_COMPRESSION;
    }

    public static boolean isCompressType(CompressionType type)
    {
        return type == ZLIB || type == NUMPRESS_LINPRED_ZLIB || type == NUMPRESS_POSINT_ZLIB || type == NUMPRESS_SHLOGF_ZLIB;
    }

    /**
     * <p>Getter for the field <code>accession</code>.</p>
     *
     * @return the CV Parameter accession of the compression type as {@link java.lang.String String}
     */
    public String getAccession()
    {
        return accession;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return the CV Parameter name of the compression type as {@link java.lang.String String}
     */
    public String getName()
    {
        return name;
    }
}
