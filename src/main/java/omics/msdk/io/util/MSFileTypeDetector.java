/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.ms.MSFileType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static omics.util.utils.ObjectUtils.checkNotNull;


/**
 * Detector of raw data file format
 *
 * @author JiaweiMao
 * @version 1.1.1
 * @since 08 Oct 2017, 8:31 PM
 */
public class MSFileTypeDetector
{
    private static final String MGF_HEADER = "BEGIN IONS";

    /*
     * See "http://www.unidata.ucar.edu/software/netcdf/docs/netcdf/File-Format-Specification.html"
     */
    private static final String CDF_HEADER = "CDF";

    /*
     * mzML files with index start with <indexedmzML><mzML>tags, but files with
     * no index contain only the <mzML> tag. See
     * "http://psidev.cvs.sourceforge.net/viewvc/psidev/psi/psi-ms/mzML/schema/mzML1.1.0.xsd"
     */
    private static final String MZML_HEADER = "<mzML";

    /*
     * mzXML files with index start with <mzXML><msRun> tags, but files with no
     * index contain only the <msRun> tag. See
     * "http://sashimi.sourceforge.net/schema_revision/mzXML_3.2/mzXML_3.2.xsd"
     */
    private static final String MZXML_HEADER = "<msRun";

    /*
     * See "http://www.psidev.info/sites/default/files/mzdata.xsd.txt"
     */
    private static final String MZDATA_HEADER = "<mzData";

    /*
     * See "https://code.google.com/p/unfinnigan/wiki/FileHeader"
     */
    private static final String THERMO_HEADER = String.valueOf(
            new char[]{0x01, 0xA1, 'F', 0, 'i', 0, 'n', 0, 'n', 0, 'i', 0, 'g', 0, 'a', 0, 'n', 0});

    /*
     * See "http://www.psidev.info/mztab#mzTab_1_0"
     */
    private static final String MZTAB_HEADER = "mzTab-version";

    /**
     * Detect the file type according to file extension.
     *
     * @param file {@link File} to test
     * @return the {@link MSFileType} of the file.
     */
    public static MSFileType detect(File file)
    {
        String name = file.getName();
        name = name.toLowerCase();
        if (name.endsWith(".mzml")) {
            return MSFileType.MZML;
        } else if (name.endsWith(".mgf")) {
            return MSFileType.MGF;
        } else if (name.endsWith(".mzxml")) {
            return MSFileType.MZXML;
        } else {
            return MSFileType.UNKNOWN;
        }
    }

    /**
     * <p>Detect the type of the data file.</p>
     *
     * @param fileName a {@link java.io.File} object.
     * @return a {@link MSFileType} object, or {@link MSFileType#UNKNOWN} if detection failed.
     * @throws java.io.IOException if any.
     */
    public static MSFileType detectDataFileType(File fileName) throws IOException
    {
        checkNotNull(fileName);

        if (fileName.isDirectory()) {
            // To check for Waters .raw directory, we look for _FUNC[0-9]{3}.DAT
            for (File f : fileName.listFiles()) {
                if (f.isFile() && f.getName().matches("_FUNC[0-9]{3}.DAT"))
                    return MSFileType.WATERS_RAW;
            }
            // We don't recognize any other directory type than Waters
            return MSFileType.UNKNOWN;
        }

        // Read the first 1kB of the file into a String
        InputStreamReader reader = new InputStreamReader(new FileInputStream(fileName), "ISO-8859-1");
        char buffer[] = new char[1024];
        reader.read(buffer);
        reader.close();

        String fileHeader = new String(buffer);

        if (fileHeader.startsWith(THERMO_HEADER)) {
            return MSFileType.THERMO_RAW;
        }

        if (fileHeader.contains(MZML_HEADER))
            return MSFileType.MZML;

        if (fileHeader.contains(MZDATA_HEADER))
            return MSFileType.MZDATA;

        if (fileHeader.contains(MZXML_HEADER))
            return MSFileType.MZXML;


        if (fileHeader.contains(MGF_HEADER))
            return MSFileType.MGF;

        return MSFileType.UNKNOWN;
    }
}
