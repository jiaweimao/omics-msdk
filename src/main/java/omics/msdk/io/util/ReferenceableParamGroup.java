/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;
import omics.util.cv.UserParam;
import omics.util.utils.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Aug 2018, 4:34 PM
 */
public class ReferenceableParamGroup {

    public static final CvParam QE = new CvParam(new CvTerm("MS:1001911", "Q Exactive"), "");

    private String id;
    private List<CvParam> cvParamList;
    private List<UserParam> userParamList;

    public ReferenceableParamGroup(String id, List<CvParam> cvParamList) {
        ObjectUtils.checkNotNull(id);

        this.id = id;
        this.cvParamList = cvParamList;
    }

    public ReferenceableParamGroup(String id) {
        this(id, null);
    }

    public void addCvParam(CvParam cvParam) {
        if (cvParamList == null)
            cvParamList = new ArrayList<>();
        this.cvParamList.add(cvParam);
    }

    public String getId() {
        return id;
    }

    public List<CvParam> getCvParamList() {
        return cvParamList;
    }

    public List<UserParam> getUserParamList() {
        return userParamList;
    }

    public void addUserParam(UserParam userParam) {
        if (userParamList == null)
            userParamList = new ArrayList<>();
        userParamList.add(userParam);
    }
}
