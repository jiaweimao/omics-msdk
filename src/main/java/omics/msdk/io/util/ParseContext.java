/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

import omics.util.io.MonitorableFileInputStream;
import omics.util.io.MonitorableInputStream;
import omics.util.ms.MSDataID;

import java.io.*;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * hold information about the parsing position and progress
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Nov 2018, 3:52 PM
 */
public class ParseContext
{
    private final MSDataID msDataID;
    private LineNumberReader currentReader;
    private int numberOfEntryParsed;
    private boolean isEOPReached;
    private int columnNumber = -1;
    private MonitorableInputStream monitorableInputStream;

    public ParseContext(File file, MSDataID source, int bufferSize)
    {
        checkNotNull(source);
        checkArgument(bufferSize > 0);

        this.msDataID = source;
        isEOPReached = false;
        try {
            this.monitorableInputStream = new MonitorableFileInputStream(file);
            this.currentReader = new LineNumberReader(new InputStreamReader(monitorableInputStream), bufferSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ParseContext(File file, MSDataID source)
    {
        checkNotNull(source);
        this.msDataID = source;
        isEOPReached = false;
        try {
            this.monitorableInputStream = new MonitorableFileInputStream(file);
            this.currentReader = new LineNumberReader(new InputStreamReader(monitorableInputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ParseContext(InputStream inputStream, MSDataID source)
    {
        checkNotNull(source);
        this.msDataID = source;
        isEOPReached = false;

        try {
            this.monitorableInputStream = new MonitorableInputStream(inputStream);
            this.currentReader = new LineNumberReader(new InputStreamReader(monitorableInputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ParseContext(InputStream inputStream, MSDataID source, int bufferSize)
    {
        checkNotNull(source);
        checkArgument(bufferSize > 0);

        this.msDataID = source;
        isEOPReached = false;

        try {
            this.monitorableInputStream = new MonitorableInputStream(inputStream);
            this.currentReader = new LineNumberReader(new InputStreamReader(monitorableInputStream), bufferSize);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MonitorableInputStream getMonitorableInputStream()
    {
        return monitorableInputStream;
    }

    public LineNumberReader getCurrentReader()
    {
        return currentReader;
    }

    public MSDataID getSource()
    {
        return msDataID;
    }

    /**
     * @return the lineNumber or -1 if unavailable
     */
    public final int getLineNumber()
    {
        if (currentReader == null) {
            return -1;
        }

        return currentReader.getLineNumber() + 1;
    }

    /**
     * @return current column number
     */
    public final int getColumnNumber()
    {
        return columnNumber;
    }

    /**
     * setter of current column number
     */
    public void setColumnNumber(int columnNumber)
    {
        this.columnNumber = columnNumber;
    }

    public int getNumberOfParsedEntry()
    {
        return numberOfEntryParsed;
    }

    /**
     * increase of number of parsed entry.
     */
    public void incrementParsedEntryNumber()
    {
        this.numberOfEntryParsed++;
    }

    /**
     * @return true if parsed to the end.
     */
    public boolean isEndOfParsing()
    {
        return isEOPReached;
    }

    public void setEndOfParsing(boolean bool)
    {
        isEOPReached = bool;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("source=");
        sb.append(msDataID);

        if (getLineNumber() != -1) {
            sb.append(", line=");
            sb.append(getLineNumber());
        }
        if (getColumnNumber() != -1) {
            sb.append(", column=");
            sb.append(getColumnNumber());
        }

        return sb.toString();
    }
}
