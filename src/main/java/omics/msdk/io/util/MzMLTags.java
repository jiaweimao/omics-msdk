/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

/**
 * Controlled vocabulary (CV) values for mzML files.
 * <p>
 * Class containing String constants of various XML tags and attributes found in the mzML format
 * </p>
 *
 * @see <a href= "http://psidev.cvs.sourceforge.net/viewvc/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo">
 * * Official CV specification</a>
 */
public interface MzMLTags {

    /**
     * Constant <code>TAG_INDEXED_MZML="indexedmzML"</code>
     */
    String TAG_INDEXED_MZML = "indexedmzML";
    /**
     * Constant <code>TAG_MZML="mzML"</code>
     */
    String TAG_MZML = "mzML";
    /**
     * Constant <code>TAG_CV_LIST="cvList"</code>
     */
    String TAG_CV_LIST = "cvList";
    String TAG_CV = "cv";
    String TAG_URI = "URI";

    String TAG_CONTACT = "contact";

    /**
     * Constant <code>TAG_DATA_PROCESSING_LIST="dataProcessingList"</code>
     */
    String TAG_DATA_PROCESSING_LIST = "dataProcessingList";
    /**
     * Constant <code>TAG_DATA_PROCESSING="dataProcessing"</code>
     */
    String TAG_DATA_PROCESSING = "dataProcessing";
    /**
     * Constant <code>TAG_PROCESSING_METHOD="processingMethod"</code>
     */
    String TAG_PROCESSING_METHOD = "processingMethod";
    /**
     * Constant <code>TAG_RUN="run"</code>
     */
    String TAG_RUN = "run";
    /**
     * Constant <code>TAG_SPECTRUM_LIST="spectrumList"</code>
     */
    String TAG_SPECTRUM_LIST = "spectrumList";
    /**
     * Constant <code>TAG_SPECTRUM="spectrum"</code>
     */
    String TAG_SPECTRUM = "spectrum";
    /**
     * Constant <code>TAG_CV_PARAM="cvParam"</code>
     */
    String TAG_CV_PARAM = "cvParam";

    String TAG_USER_PARAM = "userParam";

    String ATTR_USER_PARAM_TYPE = "type";

    /**
     * Constant <code>TAG_SCAN_LIST="scanList"</code>
     */
    String TAG_SCAN_LIST = "scanList";
    /**
     * Constant <code>TAG_SCAN="scan"</code>
     */
    String TAG_SCAN = "scan";
    /**
     * Constant <code>TAG_SCAN_WINDOW_LIST="scanWindowList"</code>
     */
    String TAG_SCAN_WINDOW_LIST = "scanWindowList";
    /**
     * Constant <code>TAG_SCAN_WINDOW="scanWindow"</code>
     */
    String TAG_SCAN_WINDOW = "scanWindow";
    /**
     * Constant <code>TAG_BINARY_DATA_ARRAY_LIST="binaryDataArrayList"</code>
     */
    String TAG_BINARY_DATA_ARRAY_LIST = "binaryDataArrayList";
    /**
     * Constant <code>TAG_BINARY_DATA_ARRAY="binaryDataArray"</code>
     */
    String TAG_BINARY_DATA_ARRAY = "binaryDataArray";
    /**
     * Constant <code>TAG_BINARY="binary"</code>
     */
    String TAG_BINARY = "binary";
    /**
     * Constant <code>TAG_CHROMATOGRAM_LIST="chromatogramList"</code>
     */
    String TAG_CHROMATOGRAM_LIST = "chromatogramList";
    /**
     * Constant <code>TAG_CHROMATOGRAM="chromatogram"</code>
     */
    String TAG_CHROMATOGRAM = "chromatogram";
    /**
     * Constant <code>TAG_PRECURSOR="precursor"</code>
     */
    String TAG_PRECURSOR = "precursor";
    /**
     * Constant <code>TAG_ISOLATION_WINDOW="isolationWindow"</code>
     */
    String TAG_ISOLATION_WINDOW = "isolationWindow";
    /**
     * Constant <code>TAG_ACTIVATION="activation"</code>
     */
    String TAG_ACTIVATION = "activation";
    /**
     * Constant <code>TAG_PRODUCT="product"</code>
     */
    String TAG_PRODUCT = "product";
    /**
     * Constant <code>TAG_PRODUCT_LIST="productList"</code>
     */
    String TAG_PRODUCT_LIST = "productList";
    /**
     * Constant <code>TAG_REF_PARAM_GROUP="referenceableParamGroup"</code>
     */
    String TAG_REF_PARAM_GROUP = "referenceableParamGroup";
    /**
     * Constant <code>TAG_REF_PARAM_GROUP_REF="referenceableParamGroupRef"</code>fileDescription
     */
    String TAG_REF_PARAM_GROUP_REF = "referenceableParamGroupRef";

    String TAG_FILE_DESCRIPTION = "fileDescription";

    String TAG_FILE_CONTENT = "fileContent";

    String TAG_SOURCE_FILE_LIST = "sourceFileList";
    String TAG_SOURCE_FILE = "sourceFile";

    String ATTR_LOCATION = "location";
    /**
     * Constant <code>TAG_REF_PARAM_GROUP_LIST="referenceableParamGroupList"</code>
     */
    String TAG_REF_PARAM_GROUP_LIST = "referenceableParamGroupList";

    String TAG_SAMPLE_LIST = "sampleList";

    String TAG_SAMPLE = "sample";

    String TAG_SOFTWARE_LIST = "softwareList";

    String TAG_SOFTWARE = "software";

    String TAG_INSTRUMENT_CONFIGURATION_LIST = "instrumentConfigurationList";

    String TAG_INSTRUMENT_CONFIGURATION = "instrumentConfiguration";

    String TAG_COMPONENT_LIST = "componentList";

    String TAG_SOURCE = "source";

    String TAG_ANALYZER = "analyzer";

    String TAG_DETECTOR = "detector";

    String ATTR_REF = "ref";

    /**
     * Constant <code>TAG_PRECURSOR_LIST="precursorList"</code>
     */
    String TAG_PRECURSOR_LIST = "precursorList";
    /**
     * Constant <code>TAG_SELECTED_ION_LIST="selectedIonList"</code>
     */
    String TAG_SELECTED_ION_LIST = "selectedIonList";
    /**
     * Constant <code>TAG_SELECTED_ION="selectedIon"</code>
     */
    String TAG_SELECTED_ION = "selectedIon";
    /**
     * Constant <code>TAG_INDEX_LIST="indexList"</code>
     */
    String TAG_INDEX_LIST = "indexList";
    /**
     * Constant <code>TAG_INDEX="index"</code>
     */
    String TAG_INDEX = "index";
    /**
     * Constant <code>TAG_OFFSET="offset"</code>
     */
    String TAG_OFFSET = "offset";
    /**
     * Constant <code>TAG_INDEX_LIST_OFFSET="indexListOffset"</code>
     */
    String TAG_INDEX_LIST_OFFSET = "indexListOffset";
    /**
     * Constant <code>TAG_FILE_CHECKSUM="fileChecksum"</code>
     */
    String TAG_FILE_CHECKSUM = "fileChecksum";

    /**
     * Constant <code>ATTR_XSI="xsi"</code>
     */
    String ATTR_XSI = "xsi";
    /**
     * Constant <code>ATTR_SCHEME_LOCATION="schemeLocation"</code>
     */
    String ATTR_SCHEME_LOCATION = "schemeLocation";
    /**
     * Constant <code>ATTR_ID="id"</code>
     */
    String ATTR_ID = "id";
    /**
     * Constant <code>ATTR_VERSION="version"</code>
     */
    String ATTR_VERSION = "version";
    /**
     * Constant <code>ATTR_COUNT="count"</code>
     */
    String ATTR_COUNT = "count";
    /**
     * Constant <code>ATTR_SOFTWARE_REF="softwareRef"</code>
     */
    String ATTR_SOFTWARE_REF = "softwareRef";
    /**
     * Constant <code>ATTR_ORDER="order"</code>
     */
    String ATTR_ORDER = "order";
    /**
     * Constant <code>ATTR_DEFAULT_ARRAY_LENGTH="defaultArrayLength"</code>
     */
    String ATTR_DEFAULT_ARRAY_LENGTH = "defaultArrayLength";
    /**
     * Constant <code>ATTR_INDEX="index"</code>
     */
    String ATTR_INDEX = "index";
    /**
     * Constant <code>ATTR_CV_REF="cvRef"</code>
     */
    String ATTR_CV_REF = "cvRef";
    /**
     * Constant <code>ATTR_NAME="name"</code>
     */
    String ATTR_NAME = "name";
    String ATTR_FULL_NAME = "fullName";
    /**
     * Constant <code>ATTR_ACCESSION="accession"</code>
     */
    String ATTR_ACCESSION = "accession";
    /**
     * Constant <code>ATTR_VALUE="value"</code>
     */
    String ATTR_VALUE = "value";

    String ATTR_UNIT_CV_REF = "unitCvRef";
    /**
     * Constant <code>ATTR_UNIT_ACCESSION="unitAccession"</code>
     */
    String ATTR_UNIT_ACCESSION = "unitAccession";

    String ATTR_UNIT_NAME = "unitName";
    /**
     * Constant <code>ATTR_ENCODED_LENGTH="encodedLength"</code>
     */
    String ATTR_ENCODED_LENGTH = "encodedLength";

    String ATTR_ARRAY_LENGTH = "arrayLength";
    /**
     * Constant <code>ATTR_ID_REF="idRef"</code>
     */
    String ATTR_ID_REF = "idRef";
    /**
     * Constant <code>ATTR_SPECTRUM_REF="spectrumRef"</code>
     */
    String ATTR_SPECTRUM_REF = "spectrumRef";
    /**
     * Constant
     * <code>ATTR_DEFAULT_INSTRUMENT_CONFIGURATION_REF="defaultInstrumentConfigurationRef"</code>
     */
    String ATTR_DEFAULT_INSTRUMENT_CONFIGURATION_REF =
            "defaultInstrumentConfigurationRef";
    /**
     * Constant
     * <code>ATTR_DEFAULT_INSTRUMENT_CONFIGURATION_REF="defaultInstrumentConfigurationRef"</code>
     */
    String ATTR_DEFAULT_DATA_PROCESSING_REF = "defaultDataProcessingRef";

    String ACC_MS1 = "MS:1000579";
    String ACC_MSn = "MS:1000580";

    // Instrument setting, expressed in percent, for adjusting collisional energies of ions in an effort
    // to provide equivalent excitation of all ions.
    String ACC_NORMALIZED_COLLISION_ENERGY = "MS:1000138";
    String ACC_ACTIVATION_ENERGY = "MS:1000509";

    String ACC_ACTIVATION_LCID = "MS:1000433";
    String ACC_ACTIVATION_CID = "MS:1000133";
    String ACC_ACTIVATION_HCD = "MS:1000422";
    String ACC_ACTIVATION_PLASMA = "MS:1000134";
    String ACC_ACTIVATION_POST_SOURCE_DECAY = "MS:1000135";
    String ACC_ACTIVATION_SID = "MS:1000136";
    String ACC_ACTIVATION_BIRD = "MS:1000242";
    String ACC_ACTIVATION_ECD = "MS:1000250";
    String ACC_ACTIVATION_IMD = "MS:1000262";
    String ACC_ACTIVATION_SORI = "MS:1000282";
    String ACC_ACTIVATION_ETD = "MS:1000598";
    String ACC_ACTIVATION_ETHCD = "MS:1002631";

    // binary data
    String ACC_INTENSITY = "MS:1000515";
    String NAME_INTENSITY = "intensity array";

    String ACC_MZ = "MS:1000514";
    String NAME_MZ = "m/z array";

    String ACC_ZLIB = "MS:1000574";
    String NAME_ZLIB = "zlib compression";
    String ACC_NO_COMPRESSION = "MS:1000576";
    String NAME_NO_COMPRESSED = "no compression";

    String ACC_FLOAT32 = "MS:1000521";
    String NAME_FLOAT32BIT = "32-bit float";
    String ACC_FLOAT64 = "MS:1000523";
    String NAME_FLOAT64BIT = "64-bit float";
    String ACC_INT32 = "MS:1000519";
    String NAME_INT32BIT = "32-bit integer";
    String ACC_INT64 = "MS:1000522";
    String NAME_INT64BIT = "64-bit integer";

    String ACC_NTSTRING = "MS:1001479";
    String NAME_NTSTRING = "null-terminated ASCII string";

    String ACC_POSSIBLE_CHARGE = "MS:1000633";

    // Chromatograms
    String ACC_CHROMATOGRAM_TIC = "MS:1000235";
    String ACC_CHROMATOGRAM_MRM_SRM = "MS:1001473";
    String ACC_CHROMATOGRAM_SIC = "MS:1000627";
    String ACC_TIME_ARRAY = "MS:1000595";
}
