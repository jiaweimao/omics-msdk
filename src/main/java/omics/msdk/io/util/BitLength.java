/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io.util;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 27 Aug 2018, 2:19 PM
 */
public enum BitLength
{
    /**
     * Corresponds to the PSI-MS ontology term "MS:1000521" / "32-bit float"
     * and binary data will be represented in the Java primitive: float
     */
    FLOAT32BIT("MS:1000521", "32-bit float"),

    /**
     * Corresponds to the PSI-MS ontology term "MS:1000523" / "64-bit float"
     * and binary data will be represented in the Java primitive: double
     */
    FLOAT64BIT("MS:1000523", "64-bit float"),

    /**
     * Corresponds to the PSI-MS ontology term "MS:1000519" / "32-bit integer"
     * and binary data will be represented in the Java primitive: int
     */
    INT32BIT("MS:1000519", "32-bit integer"),

    /**
     * Corresponds to the PSI-MS ontology term "MS:1000522" / "64-bit integer"
     * and binary data will be represented in the Java primitive: long
     */
    INT64BIT("MS:1000522", "64-bit integer"),

    /**
     * Corresponds to the PSI-MS ontology term "MS:1001479" / "null-terminated ASCII string"
     * and binary data will be represented in the Java type: String
     */
    NTSTRING("MS:1001479", "null-terminated ASCII string"),
    UNKNOWN("", "");

    private String accession;
    private String name;

    BitLength(String accession, String name)
    {
        this.accession = accession;
        this.name = name;
    }

    public String getAccession()
    {
        return accession;
    }

    public String getName()
    {
        return name;
    }
}
