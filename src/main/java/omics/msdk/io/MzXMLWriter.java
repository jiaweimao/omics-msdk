/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.Base64Utils;
import omics.msdk.io.util.BitLength;
import omics.msdk.io.util.MzXMLTag;
import omics.msdk.model.*;
import omics.util.cv.CvParam;
import omics.util.cv.UserParam;
import omics.util.io.xml.IdentingXMLStreamWriter;
import omics.util.ms.*;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.util.MzMLSpectrumID;
import omics.util.utils.NumberFormatFactory;
import omics.util.utils.ZipUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 21 Sep 2018, 11:05 PM
 */
public class MzXMLWriter implements MSWriter {

    private final Logger logger = LoggerFactory.getLogger(MzXMLWriter.class);

    private PeakParameters peakParameters;

    private final String MZXML_NAMESPACE = "http://sashimi.sourceforge.net/schema_revision/mzXML_3.2";
    private final String XML_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";
    private final String PREFIX_XSI = "xsi";
    private final String SCHEMA_LOCATION = "http://sashimi.sourceforge.net/schema_revision/mzXML_3.2/mzXML_3.2.xsd";

    private XMLStreamWriter mWriter;
    private boolean compress;
    private BitLength bitLength;
    private File targetFile;

    public MzXMLWriter(File targetFile) throws IOException {
        this(targetFile, false);
    }

    public MzXMLWriter(File targetFile, boolean compress) throws IOException {
        this(targetFile, compress, BitLength.FLOAT64BIT);
    }

    public MzXMLWriter(File targetFile, boolean compress, BitLength bitLength) throws IOException {
        this.targetFile = targetFile;
        this.mWriter = new IdentingXMLStreamWriter(targetFile);
        this.compress = compress;
        this.bitLength = bitLength;

        this.peakParameters = new PeakParameters(NumberFormatFactory.valueOf(4),
                PeakParameters.SCI_FORMAT, NumberFormatFactory.valueOf(3));

        logger.info("Writer spectrum to file: {}", targetFile);

    }

    public void setPeakParameters(PeakParameters peakParameters) {
        this.peakParameters = peakParameters;
    }

    @Override
    public void writeStartDocument() throws IOException {
        try {
            mWriter.setDefaultNamespace(MZXML_NAMESPACE);
            mWriter.setPrefix(PREFIX_XSI, XML_SCHEMA_INSTANCE);

            mWriter.writeStartDocument("UTF-8", "1.0");
            mWriter.writeStartElement(MzXMLTag.TAG_MZXML);

            mWriter.writeDefaultNamespace(MZXML_NAMESPACE);
            mWriter.writeNamespace(PREFIX_XSI, XML_SCHEMA_INSTANCE);
            mWriter.writeAttribute(XML_SCHEMA_INSTANCE, MzXMLTag.ATTR_SCHEMA_LOCATION, SCHEMA_LOCATION);
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void writeFileHeader(MSHeader header) throws IOException, XMLStreamException {
        mWriter.writeStartElement(MzXMLTag.TAG_MS_RUN);

        // Total number of scans contained in the XML instance document. This number should be equal to the
        // total number of scans in the original RAW data, unless some were not converted (e.g.: signal below threshold)..
        int scanCount = header.getScanCount(); // optional
        if (scanCount > 0)
            mWriter.writeAttribute(MzXMLTag.ATTR_SCAN_COUNT, String.valueOf(scanCount));

        for (SourceFile sourceFile : header.getFileDescription().getSourceFileList()) {
            mWriter.writeEmptyElement(MzXMLTag.TAG_PARENT_FILE);
            mWriter.writeAttribute(MzXMLTag.ATTR_FILE_NAME, sourceFile.getLocation());
            mWriter.writeAttribute(MzXMLTag.ATTR_FILE_TYPE, "processedData");

            String sha1 = "";
            for (UserParam param : sourceFile.getUserParams()) {
                if (param.getName().equals(MzXMLTag.ATTR_FILE_SHA1)) {
                    sha1 = param.getValue();
                    break;
                }
            }
            mWriter.writeAttribute(MzXMLTag.ATTR_FILE_SHA1, sha1);
        }

        int count = 1;
        for (InstrumentConfiguration instrumentConfiguration : header.getInstrumentConfigurationList()) {
            mWriter.writeStartElement(MzXMLTag.TAG_MS_INSTRUMENT);
            String id = instrumentConfiguration.getId();
            if (id == null) {
                id = String.valueOf(count++);
            }
            mWriter.writeAttribute(MzXMLTag.ATTR_INSTRUMENT_ID, id);

            mWriter.writeEmptyElement(MzXMLTag.TAG_MANUFACTURER);
            mWriter.writeAttribute(MzXMLTag.ATTR_CATEGORY, MzXMLTag.TAG_MANUFACTURER);
            mWriter.writeAttribute(MzXMLTag.ATTR_VALUE, instrumentConfiguration.getVendor().orElse(""));

            mWriter.writeEmptyElement(MzXMLTag.TAG_MS_MODEL);
            mWriter.writeAttribute(MzXMLTag.ATTR_CATEGORY, MzXMLTag.TAG_MS_MODEL);
            mWriter.writeAttribute(MzXMLTag.ATTR_VALUE, instrumentConfiguration.getModel().orElse(""));

            List<Component> componentList = instrumentConfiguration.getComponentList();
            for (Component component : componentList) {
                if (component instanceof IonSource) {
                    mWriter.writeEmptyElement(MzXMLTag.TAG_IONISATION);
                    mWriter.writeAttribute(MzXMLTag.ATTR_CATEGORY, MzXMLTag.TAG_IONISATION);
                } else if (component instanceof Analyzer) {
                    mWriter.writeEmptyElement(MzXMLTag.TAG_MASS_ANALYZER);
                    mWriter.writeAttribute(MzXMLTag.ATTR_CATEGORY, MzXMLTag.TAG_MASS_ANALYZER);
                } else if (component instanceof Detector) {
                    mWriter.writeEmptyElement(MzXMLTag.TAG_DETECTOR);
                    mWriter.writeAttribute(MzXMLTag.ATTR_CATEGORY, MzXMLTag.TAG_DETECTOR);
                }
                mWriter.writeAttribute(MzXMLTag.ATTR_VALUE, component.getType());
            }

            mWriter.writeEmptyElement(MzXMLTag.TAG_SOFTWARE);
            mWriter.writeAttribute(MzXMLTag.ATTR_TYPE, "acquisition");
            Optional<Software> software = instrumentConfiguration.getSoftware();
            if (software.isPresent()) {
                Software sw = software.get();
                mWriter.writeAttribute(MzXMLTag.ATTR_NAME, sw.getID());
                mWriter.writeAttribute(MzXMLTag.ATTR_VERSION, sw.getVersion());
            } else {
                mWriter.writeAttribute(MzXMLTag.ATTR_NAME, "");
                mWriter.writeAttribute(MzXMLTag.ATTR_VERSION, "");
            }
            mWriter.writeEndElement(); // msInstrument
        }

        for (DataProcessing dataProcessing : header.getDataProcessingList()) {
            mWriter.writeStartElement(MzXMLTag.TAG_DATA_PROCESSING);

            LinkedHashSet<Software> softwareSet = new LinkedHashSet<>();
            List<ProcessingMethod> unFound = new ArrayList<>();
            for (ProcessingMethod method : dataProcessing.getProcessingMethodList()) {
                softwareSet.add(method.getSoftware());

                boolean found = false;
                List<CvParam> cvParams = method.getCvParams();
                for (CvParam param : cvParams) {
                    if (param.getCvTerm().getAccession().equals(ProcessingMethod.DATA_FILTERING.getAccession())) {
                        mWriter.writeAttribute(MzXMLTag.ATTR_INTENSITY_CUTOFF, "1");
                        found = true;
                        break;
                    } else if (param.getCvTerm().getAccession().equals(ProcessingMethod.PEAK_PICKING.getAccession())) {
                        mWriter.writeAttribute(MzXMLTag.ATTR_CENTROIDED, "1");
                        found = true;
                        break;
                    } else if (param.getCvTerm().getAccession().equals(ProcessingMethod.DEISOTOPING.getAccession())) {
                        mWriter.writeAttribute(MzXMLTag.ATTR_DEISOTOPED, "1");
                        found = true;
                        break;
                    } else if (param.getCvTerm().getAccession().equals(ProcessingMethod.CHARGE_DECONVOLUTION.getAccession())) {
                        mWriter.writeAttribute(MzXMLTag.ATTR_CHARGE_DECONVOLUTED, "1");
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    unFound.add(method);
                }
            }
            for (Software software : softwareSet) {
                if (software == null)
                    continue;
                mWriter.writeEmptyElement(MzXMLTag.TAG_SOFTWARE);
                mWriter.writeAttribute(MzXMLTag.ATTR_TYPE, "processing");
                mWriter.writeAttribute(MzXMLTag.ATTR_NAME, software.getID());
                mWriter.writeAttribute(MzXMLTag.ATTR_VERSION, software.getVersion());
            }

            for (ProcessingMethod method : unFound) {
                Software software = method.getSoftware();
                List<CvParam> cvParams = method.getCvParams();
                if (!cvParams.isEmpty()) {
                    CvParam param = cvParams.get(0);
                    if (param.getCvTerm().getAccession().equals(ProcessingMethod.CONVERSION_2_MZML.getAccession())
                            || param.getCvTerm().getAccession().equals(ProcessingMethod.CONVERSION_2_MZXML.getAccession())) {
                        mWriter.writeEmptyElement(MzXMLTag.TAG_SOFTWARE);
                        mWriter.writeAttribute(MzXMLTag.ATTR_TYPE, "conversion");
                        mWriter.writeAttribute(MzXMLTag.ATTR_NAME, software.getID());
                        mWriter.writeAttribute(MzXMLTag.ATTR_VERSION, software.getVersion());
                    }
                    mWriter.writeEmptyElement(MzXMLTag.TAG_PROCESSING_OPERATION);
                    mWriter.writeAttribute(MzXMLTag.ATTR_NAME, param.getCvTerm().getName());
                }
            }
            mWriter.writeEndElement(); // dataProcessing
        }
    }

    @Override
    public void writeStartSpectrumList() {
    }


    @Override
    public void writeSpectrum(MsnSpectrum spectrum) throws IOException {
        try {
            MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
            mWriter.writeStartElement(MzXMLTag.TAG_SCAN);
            mWriter.writeAttribute(MzXMLTag.ATTR_NUM, spectrumID.getScanNumber().toString());

            ScanType scanType = spectrumID.getScanType();
            if (scanType != null) {
                switch (scanType) {
                    case FULLMS:
                        mWriter.writeAttribute(MzXMLTag.ATTR_SCAN_TYPE, "Full");
                        break;
                    case zoom:
                        mWriter.writeAttribute(MzXMLTag.ATTR_SCAN_TYPE, "zoom");
                        break;
                    case SRM:
                        mWriter.writeAttribute(MzXMLTag.ATTR_SCAN_TYPE, "SRM");
                        break;
                    case SIM:
                        mWriter.writeAttribute(MzXMLTag.ATTR_SCAN_TYPE, "SIM");
                }
            }

            SpectrumType spectrumType = spectrum.getSpectrumType();
            if (spectrumType == SpectrumType.CENTROIDED) {
                mWriter.writeAttribute(MzXMLTag.ATTR_CENTROIDED, "1");
            }

            mWriter.writeAttribute(MzXMLTag.ATTR_MS_LEVEL, String.valueOf(spectrum.getMsLevel()));
            mWriter.writeAttribute(MzXMLTag.ATTR_PEAKS_COUNT, String.valueOf(spectrum.size()));

            Polarity polarity = spectrumID.getPolarity();
            switch (polarity) {
                case POSITIVE:
                    mWriter.writeAttribute(MzXMLTag.ATTR_POLARITY, "+");
                    break;
                case NEGATIVE:
                    mWriter.writeAttribute(MzXMLTag.ATTR_POLARITY, "-");
                    break;
            }

            RetentionTimeList retentionTimes = spectrumID.getRetentionTimeList();
            if (!retentionTimes.isEmpty()) {
                double time = retentionTimes.getFirst().getTime();
                String rt = String.format("PT%sS", peakParameters.rtFormat().format(time));
                mWriter.writeAttribute(MzXMLTag.ATTR_RETENTION_TIME, rt);
            }

            mWriter.writeAttribute(MzXMLTag.ATTR_BASE_PEAK_MZ, peakParameters.mzFormat().format(spectrum.getBasePeakX()));
            mWriter.writeAttribute(MzXMLTag.ATTR_BASE_PEAK_INTENSITY, peakParameters.intensityFormat().format(spectrum.getBasePeakY()));
            mWriter.writeAttribute(MzXMLTag.ATTR_TOT_ION_CURRENT, peakParameters.intensityFormat().format(spectrum.getTotalIonCurrent()));

            if (spectrum.getMsLevel() > 1) {
                Peak precursor = spectrum.getPrecursor();
                mWriter.writeStartElement(MzXMLTag.TAG_PRECURSOR_MZ);
                mWriter.writeAttribute(MzXMLTag.ATTR_PRECURSOR_SCAN, spectrumID.getParentScanNumber().toString());
                mWriter.writeAttribute(MzXMLTag.TAG_PRECURSOR_INTENSITY, peakParameters.intensityFormat().format(precursor.getIntensity()));
                mWriter.writeAttribute(MzXMLTag.ATTR_PRECURSOR_CHARGE, String.valueOf(precursor.getCharge()));

                mWriter.writeAttribute(MzXMLTag.ATTR_ACTIVATION_METHOD, spectrumID.getDissociation().toString());
                mWriter.writeCharacters(peakParameters.mzFormat().format(precursor.getMz()));
                mWriter.writeEndElement();
            }

            mWriter.writeStartElement(MzXMLTag.TAG_PEAKS);
            if (compress) {
                mWriter.writeAttribute(MzXMLTag.ATTR_COMPRESSION_TYPE, "zlib");
            } else {
                mWriter.writeAttribute(MzXMLTag.ATTR_COMPRESSION_TYPE, "none");
            }

            int compressedLen = 0;
            byte[] data;
            if (bitLength == BitLength.FLOAT32BIT) {
                ByteBuffer buffer = ByteBuffer.allocate(spectrum.size() * 8);
                buffer.order(ByteOrder.BIG_ENDIAN);
                for (int i = 0; i < spectrum.size(); i++) {
                    buffer.putFloat((float) spectrum.getMz(i));
                    buffer.putFloat((float) spectrum.getIntensity(i));
                }
                data = buffer.array();
                if (compress) {
                    data = ZipUtils.compressByDeflater(data);
                    compressedLen = data.length;
                }
                data = Base64Utils.encoder.encode(data);
            } else if (bitLength == BitLength.FLOAT64BIT) {
                ByteBuffer buffer = ByteBuffer.allocate(spectrum.size() * 16);
                buffer.order(ByteOrder.BIG_ENDIAN);
                for (int i = 0; i < spectrum.size(); i++) {
                    buffer.putDouble(spectrum.getMz(i));
                    buffer.putDouble(spectrum.getIntensity(i));
                }
                data = buffer.array();
                if (compress) {
                    data = ZipUtils.compressByDeflater(data);
                    compressedLen = data.length;
                }
                data = Base64Utils.encoder.encode(data);
            } else {
                throw new IOException(bitLength + " is not supported");
            }

            String peaks = new String(data, StandardCharsets.US_ASCII);

            mWriter.writeAttribute(MzXMLTag.ATTR_COMPRESSED_LEN, String.valueOf(compressedLen));
            mWriter.writeAttribute(MzXMLTag.ATTR_PRECISION, String.valueOf(bitLength == BitLength.FLOAT64BIT ? 64 : 32));
            mWriter.writeAttribute(MzXMLTag.ATTR_BYTE_ORDER, "network");
            mWriter.writeAttribute(MzXMLTag.ATTR_CONTENT_TYPE, "m/z-int");
            mWriter.writeCharacters(peaks);

            mWriter.writeEndElement(); // end of peaks
            mWriter.writeEndElement(); // end of scan
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void writeEndSpectrumList() {
    }

    @Override
    public void writeStartChromatogramList() {
    }

    @Override
    public void writeChromatogram(Chromatogram chromatogram) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeEndChromatogramList() {
    }

    @Override
    public void writeEndDocument() throws IOException {
        try {
            mWriter.writeEndDocument();
            logger.info("Finish writing to {}" + targetFile);
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            mWriter.close();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }
}
