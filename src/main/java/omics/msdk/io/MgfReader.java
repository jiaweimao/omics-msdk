/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import omics.msdk.io.util.ParseContext;
import omics.msdk.model.MSHeader;
import omics.util.ms.*;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessorChain;
import omics.util.ms.peaklist.Precision;
import omics.util.utils.RegexConstants;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Mascot Generic Format peak list reader.
 * <p>
 * To extend how the TITLE line is handled add more TitleParser instances using addTitleParser
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 08 Oct 2017, 8:33 PM
 */
public class MgfReader extends MSReader<PeakAnnotation, MsnSpectrum> {
    /**
     * Read the first spectrum in the file
     *
     * @param file a mgf file
     */
    public static Optional<MsnSpectrum> readFirst(File file) {
        MsnSpectrum spectrum = null;
        try {
            MgfReader reader = new MgfReader(file);
            if (reader.hasNext())
                spectrum = reader.next();

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(spectrum);
    }

    private static final int READ_AHEAD_LIMIT = 4096; //Half of the default buffer size
    private static final String BEGIN_IONS = "BEGIN IONS";

    private static final Pattern PAT_CHARGE = Pattern.compile("([-+]?)(\\d+)([-+]?)");
    private static final Pattern PAT_COMMENT = Pattern.compile("^[#/!;].*");
    private static final Pattern PAT_PEP_MASS = Pattern.compile("^(\\d+\\.?\\d*)\\s*(\\d+\\.?\\d*)?\\s*([\\d\\s*,?\\s*]*)?");
    private static final Pattern PAT_PEP_MASS_CHARGE = Pattern.compile("\\d+");
    private static final Pattern PAT_SCANS = Pattern.compile("\\s*(\\d+-?\\d*)[,\\s]*");
    private static final Pattern PAT_RT_TAG = Pattern.compile("(\\d+\\.?\\d*-?\\d*\\.?\\d*)");
    private static final Pattern PAT_PEAK = Pattern.compile("(" + RegexConstants.REAL + "|"
            + RegexConstants.INTEGER + ")\\s+(" + RegexConstants.REAL + "|" + RegexConstants.INTEGER + ").*");

    //Set to a non null value by parseHeader
    private int[] defaultCharge = {2, 3, 4};

    private final List<TitleParser> titleParsers;
    private TitleParser currentTitleParser;

    public MgfReader(File file) throws IOException {
        this(file, Precision.DOUBLE);
    }

    public MgfReader(File file, Precision precision) throws IOException {
        this(file, precision, new PeakProcessorChain<>());
    }

    public MgfReader(File file, Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {
        this(new FileInputStream(file), new MSDataID(file.getName(), MSFileType.MGF, file.getName(), file.toURI()), precision, processorChain);
    }

    public MgfReader(InputStream inputStream, MSDataID source, Precision precision) throws IOException {
        this(inputStream, source, precision, new PeakProcessorChain<>());
    }

    public MgfReader(InputStream inputStream, MSDataID source, Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {
        super(inputStream, source, precision, processorChain);
        titleParsers = initTitleParsers();
    }

    /**
     * Convenience method to check if a string matches a pattern
     *
     * @param string  the string
     * @param pattern the patter
     * @return true if the string matches the patter, false otherwise
     */
    private static boolean matches(final String string, final Pattern pattern) {
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    private List<TitleParser> initTitleParsers() {
        List<TitleParser> parsers = new ArrayList<>();
        ServiceLoader<TitleParser> loader = ServiceLoader.load(TitleParser.class);
        for (TitleParser parser : loader) {
            parsers.add(parser);
        }
        if (parsers.isEmpty()) {
            throw new IllegalStateException("No TitleParser implementation class found: org.openide.util.Lookup has " +
                    "not properly worked (hint: check that Annotation Compiler Processing is enable)");
        } else {
            currentTitleParser = parsers.get(0);
        }

        return parsers;
    }

    public void addTitleParser(TitleParser parser) {
        Objects.requireNonNull(parser);
        titleParsers.add(parser);
        currentTitleParser = parser;
    }

    @Override
    protected MSHeader parseHeader(ParseContext context) {
        MSHeader header = new MSHeader();
        header.setMSDataID(context.getSource());
        return header;
    }

    /**
     * Parse all the lines that belong to one MS1 spectrum and the metadata and peaks to the spectrum.
     *
     * @param spectrum the spectrum
     * @param context  the parse context
     * @throws IOException if there are IO exceptions
     */
    protected void parseNextMs1Entry(MsnSpectrum spectrum, ParseContext context) throws IOException {
        LineNumberReader reader = context.getCurrentReader();

        boolean endTag = false;

        for (String line = reader.readLine(); line != null; line = reader.readLine()) {

            reader.mark(READ_AHEAD_LIMIT);

            if (line.length() > 0 && Character.isDigit(line.charAt(0))) {

                handlePeakLine(spectrum, line, context);
            } else if (line.startsWith(BEGIN_IONS)) {

                // reset to the previous mark (for next call of parseNextEntry() that need a valid entry)
                reader.reset();

                endTag = true;
                break;
            } else if (line.length() == 0) {

                handleEndIonLine(spectrum, line);

                endTag = true;
                break;
            } else if (matches(line, PAT_COMMENT)) {

                handleComment(spectrum, line);
            } else {

                handleUnknownLine(spectrum, line);
            }
        }

        if (!endTag) throw new IOException("mandatory 'END IONS' tag is missing!");
    }

    /**
     * Parse all lines that correspond to the next MS2 entry and the metadata and peaks to the spectrum.
     *
     * @param spectrum the spectrum
     * @param context  the parse context
     * @throws IOException if there are IO exceptions
     */
    protected void parseNextMs2Entry(MsnSpectrum spectrum, ParseContext context) throws IOException {
        LineNumberReader reader = context.getCurrentReader();

        boolean endTag = false;

        String line;

        for (line = reader.readLine(); line != null; line = reader.readLine()) {

            if (line.length() > 0 && Character.isDigit(line.charAt(0))) {

                handlePeakLine(spectrum, line, context);
            } else if (line.startsWith(BEGIN_IONS)) {

                // begin tag has already been met! -> missing END IONS
                // reset to the previous mark (for next call of parseNextEntry() that need a valid entry)
                reader.reset();
                break;
            } else if (line.startsWith("END IONS")) {

                handleEndIonLine(spectrum, line);

                endTag = true;
                break;
            } else if (line.contains("=")) {

                final int index = line.indexOf('=');
                handleTagLine(line.substring(0, index), line.substring(index + 1), spectrum);
            } else if (matches(line, PAT_COMMENT)) {

                handleComment(spectrum, line);
            } else {

                handleUnknownLine(spectrum, line);
            }

            reader.mark(READ_AHEAD_LIMIT);
        }

        if (!endTag) throw new IOException("mandatory 'END IONS' tag is missing!");
    }

    /**
     * Allows subclasses to handle lines that occur between BEGIN IONS and END IONS tags. Default implementation does
     * nothing
     *
     * @param line the line
     */
    protected void handleIntermediaryLine(String line) {
    }

    /**
     * Handle intermediary lines between BEGIN IONS and END IONS.
     *
     * @param line the intermediary line to handle
     * @return true if this line must be skipped
     */
    protected boolean skipIntermediaryLine(String line) {
        if (line.isEmpty()) {
            return true;
        } else if (Character.isDigit(line.charAt(0))) { // next MS1 entry: stop skipping!
            return false;
        } else if (line.startsWith(BEGIN_IONS)) { // next MSn entry: stop skipping!
            return false;
        } else {
            handleIntermediaryLine(line);
            return true;
        }
    }

    @Override
    protected MsnSpectrum parseNextEntry(ParseContext context) throws IOException {
        // prepare next spectrum
        MsnSpectrum spectrum = new MsnSpectrum(precision);
        spectrum.getSpectrumID().setMSDataID(context.getSource());
        spectrum.getPrecursor().setMzAndCharge(0, defaultCharge);
        spectrum.setMsLevel(1);

        // skipping intermediary lines
        LineNumberReader reader = context.getCurrentReader();
        String line;
        for (line = reader.readLine(); line != null; line = reader.readLine()) {
            if (!skipIntermediaryLine(line))
                break;
            reader.mark(READ_AHEAD_LIMIT);
        }

        // stop if EOF
        if (line == null)
            return null;

        if (line.startsWith(BEGIN_IONS)) {
            spectrum.setMsLevel(2);
            parseNextMs2Entry(spectrum, context);
        } else if (context.getNumberOfParsedEntry() == 0) {
            reader.reset();
            parseNextMs1Entry(spectrum, context);
        }

        updateProgress();
        return spectrum;
    }

    /**
     * Handle comment line.  Comment lies start with #, ;, ! or /. Default implementation does nothing.
     *
     * @param spectrum the spectrum currently being read
     * @param line     the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handleComment(MsnSpectrum spectrum, String line) {
        return false;
    }

    /**
     * Handle tag value line.  The tag and value is separated by =
     *
     * @param spectrum the spectrum currently being read
     * @param tag      the string from the start of the lien to the = character
     * @param value    the string from the character after the = to the end of the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handleTagLine(String tag, String value, MsnSpectrum spectrum) {
        if (tag.startsWith("TITLE")) {
            spectrum.getSpectrumID().setTitle(value);
            return parseTitleTag(value, spectrum);
        } else if (tag.startsWith("PEPMASS")) {

            return parsePepMassTag(value, spectrum);
        } else if (tag.startsWith("CHARGE")) {

            return parseChargeTag(value, spectrum);
        } else if (tag.startsWith("SCANS")) {

            return parseScanTag(value, spectrum);
        } else if (tag.startsWith("RTINSECONDS")) {

            return parseRetentionTimeTag(value, spectrum);
        } else {

            return parseUnknownTag(tag, value, spectrum);
        }
    }

    /**
     * Handle peak line, which is any line that starts with a digit
     *
     * @param spectrum the spectrum currently being read
     * @param line     the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handlePeakLine(MsnSpectrum spectrum, String line, ParseContext context) {
        Matcher peakMatcher = PAT_PEAK.matcher(line);
        if (peakMatcher.matches()) {

            double mz = Double.parseDouble(peakMatcher.group(1));
            double intensity = Double.parseDouble(peakMatcher.group(2));

            addPeakToSpectrum(spectrum, mz, intensity, context);
            return true;
        } else {

            return false;
        }
    }

    /**
     * Handle end ion line, which is a line that starts with "END IONS". Default implementation does nothing
     *
     * @param spectrum the spectrum currently being read
     * @param line     the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handleEndIonLine(MsnSpectrum spectrum, String line) {
        return false;
    }

    /**
     * Handle line that is not a comment, begin ion, tag, peak or end ion. Default implementation does nothing
     *
     * @param spectrum the spectrum currently being read
     * @param line     the line
     * @return true if the information in the line was processed, false otherwise
     */
    protected boolean handleUnknownLine(MsnSpectrum spectrum, String line) {
        return false;
    }

    /**
     * Extracts charges from a string. Any substring that matches [+-]\d+[+-] is said to be a charge the sign of the
     * charge is determined by the sign before or after the digit.  If there is a sign before and after the digit the
     * sign after the digit takes precedent.
     *
     * @param text the String from which to extract the charges
     * @return TIntArrayList containing the charges
     */
    private IntArrayList extractCharge(String text) {
        IntArrayList charges = new IntArrayList();

        Matcher matcher = PAT_CHARGE.matcher(text);
        while (matcher.find()) {

            int number = Integer.parseInt(matcher.group(2));
            String sign = matcher.group(3);
            if (sign == null || sign.length() == 0) sign = matcher.group(1);

            if (sign != null && sign.length() == 1 && sign.charAt(0) == '-')
                number = number * -1;

            charges.add(number);
        }
        return charges;
    }

    /**
     * Parse the line that starts with a TITLE tag.  This uses the list of title parsers to find a parser that can
     * parse the title. Note that the title parsed value is assign to the spectrum title by default.
     *
     * @param value    the title value
     * @param spectrum the spectrum being parsed
     * @return true if any of the registered title parses extracted information from the title
     */
    protected boolean parseTitleTag(String value, MsnSpectrum spectrum) {
        boolean parsed = currentTitleParser.parse(value, spectrum);

        if (!parsed) {
            for (TitleParser titleParser : titleParsers) {

                if (titleParser.parse(value, spectrum)) {

                    parsed = true;
                    currentTitleParser = titleParser;
                    break;
                }
            }
        }
        return parsed;
    }

    /**
     * Called if there are any tags that are not handled by MgfReader
     * <p/>
     * This is meant to be over ridden by subclasses that want to parse tags that the MgfReader does not know about
     *
     * @param tag      the characters in the line before the first =
     * @param value    the characters in the line after the first =
     * @param spectrum the spectrum that is being read
     * @return true of this handled the tag, false otherwise
     */
    //This is meant to be over ridden by subclasses that want to parse tags that the MgfReader does not know about
    protected boolean parseUnknownTag(String tag, String value, MsnSpectrum spectrum) {
        return false;
    }

    /**
     * Parse the line that starts with RTINSECONDS
     *
     * @param value    the value
     * @param spectrum the spectrum being parsed
     * @return true if the tag was handled, false otherwise
     */
    protected boolean parseRetentionTimeTag(String value, MsnSpectrum spectrum) {
        RetentionTimeList retentionTimeList = new RetentionTimeList();
        Matcher matcher = PAT_RT_TAG.matcher(value);

        while (matcher.find()) {
            String rt = matcher.group(1);

            final int index = rt.indexOf('-');
            if (index != -1) {
                retentionTimeList.add(new RetentionTimeInterval(
                        Double.parseDouble(rt.substring(0, index)),
                        Double.parseDouble(rt.substring(index + 1)),
                        TimeUnit.SECOND
                ));
            } else {
                retentionTimeList.add(new RetentionTimeDiscrete(Double.parseDouble(rt), TimeUnit.SECOND));
            }
        }

        if (!retentionTimeList.isEmpty()) {

            setRetentionTimes(spectrum, retentionTimeList);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add the retention times contained in the retentionTimeList to the spectrum.
     *
     * @param spectrum          the spectrum that is currently being read
     * @param retentionTimeList the retention times that were read
     */
    protected void setRetentionTimes(MsnSpectrum spectrum, RetentionTimeList retentionTimeList) {
        spectrum.getSpectrumID().addRetentionTimes(retentionTimeList);
    }

    /**
     * Parse the line that starts with CHARGE
     *
     * @param value    the value
     * @param spectrum the spectrum being parsed
     * @return true if the tag was handled, false otherwise
     */
    protected boolean parseScanTag(String value, MsnSpectrum spectrum) {
        ScanNumberList scanNumbers = new ScanNumberList();
        Matcher matcher = PAT_SCANS.matcher(value);

        while (matcher.find()) {

            String scanNumber = matcher.group(1);

            final int index = scanNumber.indexOf('-');
            if (index != -1) {

                scanNumbers.add(Integer.parseInt(scanNumber.substring(0, index)), Integer.parseInt(scanNumber.substring(index + 1)));
            } else {

                scanNumbers.add(Integer.parseInt(scanNumber));
            }
        }

        if (!scanNumbers.isEmpty()) {

            setScanNumbers(spectrum, scanNumbers);
            return true;
        } else {

            return false;
        }
    }

    /**
     * Add the scan numbers contained in the scanNumbers to the spectrum.
     *
     * @param spectrum    the spectrum that is currently being read
     * @param scanNumbers the scan numbers that were read
     */
    protected void setScanNumbers(MsnSpectrum spectrum, ScanNumberList scanNumbers) {
        ScanNumberList scanNumberList = spectrum.getScanNumberList();
        for (ScanNumber scanNumber : scanNumbers) {
            if (!scanNumberList.contains(scanNumber)) {
                scanNumberList.add(scanNumber);
            }
        }
    }

    /**
     * Parse the line that starts with CHARGE
     *
     * @param value    the value
     * @param spectrum the spectrum being parsed
     * @return true if the tag was handled, false otherwise
     */
    protected boolean parseChargeTag(String value, MsnSpectrum spectrum) {
        final Peak precursor = spectrum.getPrecursor();

        IntArrayList charges = extractCharge(value);

        precursor.setMzAndCharge(precursor.getMz(), charges.toIntArray());

        return true;
    }

    /**
     * Parse the line that starts with PEPMASS
     *
     * @param value    the value
     * @param spectrum the spectrum being parsed
     * @return true if the tag was handled, false otherwise
     */
    protected boolean parsePepMassTag(String value, MsnSpectrum spectrum) {
        Matcher matcher = PAT_PEP_MASS.matcher(value);
        if (matcher.find()) {
            String mzGroup = matcher.group(1);
            String intensityGroup = matcher.group(2);
            String chargeGroup = matcher.group(3);

            Peak precursor = spectrum.getPrecursor();
            double mz = Double.parseDouble(mzGroup);
            int[] charge = precursor.getCharges();
            if (intensityGroup != null)
                precursor.setIntensity(Double.parseDouble(intensityGroup));
            if (chargeGroup != null && chargeGroup.length() > 0) {

                IntArrayList charges = new IntArrayList();

                matcher = PAT_PEP_MASS_CHARGE.matcher(chargeGroup);
                while (matcher.find()) {

                    charges.add(Integer.parseInt(matcher.group(0)));
                }

                charge = charges.toIntArray();
            }
            precursor.setMzAndCharge(mz, charge);
            return true;
        } else {
            return false;
        }
    }
}