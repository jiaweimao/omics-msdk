/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.Base64Utils;
import omics.msdk.io.util.MzXMLTag;
import omics.msdk.io.util.ParseContext;
import omics.msdk.model.*;
import omics.util.cv.CvParam;
import omics.util.cv.UserParam;
import omics.util.io.FilenameUtils;
import omics.util.io.xml.XMLUtils;
import omics.util.ms.*;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessorChain;
import omics.util.ms.peaklist.Precision;
import omics.util.ms.util.MzMLSpectrumID;
import omics.util.utils.ArrayUtils;
import omics.util.utils.RegexConstants;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A stream-based reader of mzxml file.
 *
 * @author JiaweiMao
 * @version 2.0.0
 * @since 08 Oct 2017, 9:19 PM
 */
public class MzXMLReader extends MSReader<PeakAnnotation, MsnSpectrum> {

    private static final Pattern DURATION_PATTERN = Pattern.compile("PT(" + RegexConstants.REAL + ")([SM])");

    private XMLEventReader xmlReader;
    private StartElement startScan;

    private double[] mzs;
    private double[] intensities;
    private MsnSpectrum lastPrecursorSpectrum;
    private MsnSpectrum previousSpectrum;

    public MzXMLReader(File file) throws IOException {
        this(file, Precision.DOUBLE);
    }

    public MzXMLReader(File file, Precision precision) throws IOException {
        this(file, precision, new PeakProcessorChain<>());
    }

    public MzXMLReader(File file, Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws
            IOException {
        this(new FileInputStream(file), new MSDataID(file.getName(), MSFileType.MZXML, file.getName(), file.toURI()), precision, processorChain);
    }

    private MzXMLReader(InputStream inputStream, MSDataID source, Precision precision) throws IOException {
        this(inputStream, source, precision, new PeakProcessorChain<>());
    }

    private MzXMLReader(InputStream inputStream, MSDataID source, Precision precision, PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {
        super(inputStream, source, precision, processorChain);

        mzs = new double[100];
        intensities = new double[100];

        lastPrecursorSpectrum = previousSpectrum = null;
    }

    /**
     * convert mzXML duration type to {@link RetentionTime}
     *
     * @param duration time format as "PT0.3512S"
     */
    private static RetentionTime mzXMLDuration2retentionTime(final String duration) {
        Matcher matcher = DURATION_PATTERN.matcher(duration);
        double retentionTime = 0;
        char retentionTimeUnit = 'S';

        if (matcher.matches()) {
            retentionTime = Double.parseDouble(matcher.group(1));
            retentionTimeUnit = matcher.group(2).charAt(0);
        }

        return new RetentionTimeDiscrete(retentionTime, (retentionTimeUnit == 'S') ? TimeUnit.SECOND : TimeUnit.MINUTE);
    }

    @Override
    protected void init(ParseContext context) throws IOException {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        xmlif.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        try {
            this.xmlReader = xmlif.createXMLEventReader(context.getCurrentReader());
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            xmlReader.close();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    protected MSHeader parseHeader(ParseContext context) throws XMLStreamException {
        MSHeader header;
        Optional<StartElement> stOp = XMLUtils.toStartElement(xmlReader, MzXMLTag.TAG_MS_RUN);
        if (!stOp.isPresent()) {
            throw new XMLStreamException("missing msRun start element!");
        }

        header = new MSHeader();
        StartElement startRun = stOp.get();
        context.setColumnNumber(startRun.getLocation().getColumnNumber());

        String count = XMLUtils.attr(startRun, MzXMLTag.ATTR_SCAN_COUNT);
        if (count != null) {
            // expect scan count
            int expectedScanNumber = Integer.parseInt(count);
            header.setScanCount(expectedScanNumber);
        }

        int dataProcessingId = 1;
        int sourceFileId = 1;

        while (xmlReader.hasNext()) {
            XMLEvent event = xmlReader.peek();

            if (XMLUtils.isStartElement(event, MzXMLTag.TAG_SCAN))
                break;

            xmlReader.nextEvent();
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String localName = startElement.getName().getLocalPart();
                switch (localName) {
                    case MzXMLTag.TAG_PARENT_FILE: {

                        String parentFileName = XMLUtils.mandatoryAttr(startElement, MzXMLTag.ATTR_FILE_NAME);
                        String id = FilenameUtils.getExtension(parentFileName) + sourceFileId;
                        String name = FilenameUtils.getName(parentFileName);
                        String location = FilenameUtils.getFullPath(parentFileName);

                        SourceFile sourceFile = new SourceFile(id, name, location);
                        sourceFileId++;

                        // fileType is ignored
                        String sha1 = XMLUtils.mandatoryAttr(startElement, MzXMLTag.ATTR_FILE_SHA1);
                        UserParam userParam = new UserParam();
                        userParam.setName(MzXMLTag.ATTR_FILE_SHA1);
                        userParam.setValue(sha1);
                        sourceFile.addUserParam(userParam);

                        header.getFileDescription().addSourceFile(sourceFile);
                        break;
                    }
                    case MzXMLTag.TAG_MS_INSTRUMENT: {
                        String id = XMLUtils.attr(startElement, MzXMLTag.ATTR_INSTRUMENT_ID); // optional
                        if (id == null)
                            id = "1";
                        InstrumentConfiguration configuration = new InstrumentConfiguration(id);
                        while (xmlReader.hasNext()) {
                            XMLEvent instruEvent = xmlReader.nextEvent();
                            if (XMLUtils.isEndElement(instruEvent, MzXMLTag.TAG_MS_INSTRUMENT))
                                break;

                            if (instruEvent.isStartElement()) {
                                StartElement stEvent = instruEvent.asStartElement();
                                String instrLocalName = instruEvent.asStartElement().getName().getLocalPart();
                                switch (instrLocalName) {
                                    case MzXMLTag.TAG_MANUFACTURER: {
                                        String value = XMLUtils.mandatoryAttr(stEvent, MzXMLTag.ATTR_VALUE);
                                        configuration.setVendor(value);
                                        break;
                                    }
                                    case MzXMLTag.TAG_MS_MODEL: {
                                        String value = XMLUtils.mandatoryAttr(stEvent, MzXMLTag.ATTR_VALUE);
                                        configuration.setModel(value);
                                        break;
                                    }
                                    case MzXMLTag.TAG_SOFTWARE:
                                        String name = XMLUtils.attr(stEvent, MzXMLTag.ATTR_NAME);
                                        String version = XMLUtils.attr(stEvent, MzXMLTag.ATTR_VERSION);
                                        Software software = new Software(name, version);
                                        header.addSoftware(software);
                                        configuration.setSoftware(software);
                                        break;
                                    case MzXMLTag.TAG_IONISATION: {
                                        IonSource source = new IonSource();
                                        source.setType(XMLUtils.attr(stEvent, MzXMLTag.ATTR_VALUE));
                                        configuration.addComponent(source);
                                        break;
                                    }
                                    case MzXMLTag.TAG_MASS_ANALYZER: {
                                        Analyzer analyzer = new Analyzer();
                                        analyzer.setType(XMLUtils.attr(stEvent, MzXMLTag.ATTR_VALUE));
                                        configuration.addComponent(analyzer);
                                        break;
                                    }
                                    case MzXMLTag.TAG_DETECTOR: {
                                        Detector detector = new Detector();
                                        detector.setType(XMLUtils.attr(stEvent, MzXMLTag.ATTR_VALUE));
                                        configuration.addComponent(detector);
                                        break;
                                    }
                                    case MzXMLTag.TAG_OPERATOR:
                                        break;
                                    case MzXMLTag.TAG_RESOLUTION:
                                        break;
                                    default: {
                                        UserParam userParam = new UserParam();
                                        userParam.setName(XMLUtils.attr(stEvent, MzXMLTag.ATTR_NAME));
                                        userParam.setValue(XMLUtils.attr(stEvent, MzXMLTag.ATTR_VALUE));
                                        userParam.setType(XMLUtils.attr(stEvent, MzXMLTag.ATTR_TYPE));
                                        configuration.addUserParam(userParam);
                                        break;
                                    }
                                }
                            }
                        }
                        header.addInstrumentConfiguration(configuration);
                        break;
                    }
                    case MzXMLTag.TAG_DATA_PROCESSING: { // at least 1
                        DataProcessing dataProcessing = new DataProcessing(String.valueOf(dataProcessingId++));
                        Iterator attributes = startElement.getAttributes();
                        while (attributes.hasNext()) {
                            Attribute attr = (Attribute) attributes.next();
                            String attrName = attr.getName().getLocalPart();
                            String attrValue = attr.getValue();

                            switch (attrName) {
                                case MzXMLTag.ATTR_INTENSITY_CUTOFF: {
                                    ProcessingMethod method = new ProcessingMethod(new CvParam(ProcessingMethod.DATA_FILTERING, attrValue));
                                    dataProcessing.addProcessingMethod(method);
                                    break;
                                }
                                case MzXMLTag.ATTR_CENTROIDED: {
                                    if (attrValue.equals("1")) {
                                        ProcessingMethod method = new ProcessingMethod(new CvParam(ProcessingMethod.PEAK_PICKING, ""));
                                        dataProcessing.addProcessingMethod(method);
                                    }
                                    break;
                                }
                                case MzXMLTag.ATTR_DEISOTOPED: {
                                    if (attrValue.equals("1")) {
                                        ProcessingMethod method = new ProcessingMethod(new CvParam(ProcessingMethod.DEISOTOPING, ""));
                                        dataProcessing.addProcessingMethod(method);
                                    }
                                    break;
                                }
                                case MzXMLTag.ATTR_CHARGE_DECONVOLUTED: {
                                    if (attrValue.equals("1")) {
                                        ProcessingMethod method = new ProcessingMethod(new CvParam(ProcessingMethod.CHARGE_DECONVOLUTION, ""));
                                        dataProcessing.addProcessingMethod(method);
                                    }
                                    break;
                                }
                                default:
                                    UserParam userParam = new UserParam();
                                    userParam.setName(attrName);
                                    userParam.setValue(attrValue);
                                    // additional process method not known
                                    dataProcessing.addUserParam(userParam);
                                    break;
                            }
                        }
                        while (xmlReader.hasNext()) {
                            XMLEvent dpEvent = xmlReader.nextEvent();
                            if (dpEvent.isEndElement()
                                    && dpEvent.asEndElement().getName().getLocalPart().equals(MzXMLTag.TAG_DATA_PROCESSING))
                                break;

                            if (dpEvent.isStartElement()) {
                                StartElement dpStart = dpEvent.asStartElement();
                                String dpLocal = dpStart.getName().getLocalPart();

                                if (dpLocal.equals(MzXMLTag.TAG_SOFTWARE)) {
                                    String type = XMLUtils.mandatoryAttr(dpStart, MzXMLTag.ATTR_TYPE);
                                    String name = XMLUtils.mandatoryAttr(dpStart, MzXMLTag.ATTR_NAME);
                                    String version = XMLUtils.mandatoryAttr(dpStart, MzXMLTag.ATTR_VERSION);

                                    Software software = new Software(name, version);
                                    header.addSoftware(software);

                                    if (type.equals("conversion")) {
                                        ProcessingMethod method = new ProcessingMethod(new CvParam(ProcessingMethod.CONVERSION_2_MZXML, ""));
                                        method.setSoftware(software);
                                        dataProcessing.addProcessingMethod(method);
                                    } else if (type.equals("processing")) {
                                        for (ProcessingMethod processingMethod : dataProcessing.getProcessingMethodList()) {
                                            processingMethod.setSoftware(software);
                                        }
                                    }
                                } else if (dpLocal.equals(MzXMLTag.TAG_PROCESSING_OPERATION)) {
                                    String name = XMLUtils.attr(dpStart, MzXMLTag.ATTR_NAME);
                                    if (name.equals(ProcessingMethod.CONVERSION_2_MZML.getName())) {
                                        dataProcessing.addProcessingMethod(new ProcessingMethod(new CvParam(ProcessingMethod.CONVERSION_2_MZXML, "")));
                                    } else {
                                        UserParam userParam = new UserParam();
                                        userParam.setName(XMLUtils.attr(dpStart, MzXMLTag.ATTR_NAME));
                                        userParam.setValue(XMLUtils.attr(dpStart, MzXMLTag.ATTR_VALUE));
                                        userParam.setType(XMLUtils.attr(dpStart, MzXMLTag.ATTR_TYPE));

                                        dataProcessing.addUserParam(userParam);
                                    }
                                }
                            }
                        }

                        header.addDataProcessing(dataProcessing);
                        break; // end of dataProcessing
                    }
                }
            }
        }


        return header;
    }

    @Override
    protected MsnSpectrum parseNextEntry(ParseContext context) throws IOException {
        try {
            // parse <scan>
            Optional<StartElement> optionalStartScan = XMLUtils.toStartElement(xmlReader, MzXMLTag.TAG_SCAN);

            if (!optionalStartScan.isPresent()) {
                context.setEndOfParsing(true);
                // no more spectrum to read
                return null;
            }

            startScan = optionalStartScan.get();

            // create new spectrum + set infos from scan attributes
            MsnSpectrum spectrum = newSpectrum(startScan, context);

            // parse <precursorMz>
            Optional<StartElement> optPrecMzStartElement = XMLUtils.nextStartElementIf(xmlReader, MzXMLTag.TAG_PRECURSOR_MZ);
            if (optPrecMzStartElement.isPresent()) {
                // create new Precursor and set spectrum
                setNewSpectrumPrecursor(spectrum, optPrecMzStartElement.get());
            }

            // parse <peaks>
            StartElement startPeaks = XMLUtils.getMandatoryXMLEvent(XMLUtils.nextStartElementIf(xmlReader,
                    MzXMLTag.TAG_PEAKS), "missing <peaks> xml element!");

            // decode, [decompress] and add peaks to spectrum
            int peaksCount = Integer.parseInt(XMLUtils.mandatoryAttr(startScan, MzXMLTag.ATTR_PEAKS_COUNT));
            if (peaksCount > 0) {
                handlePeaks(spectrum, peaksCount, startPeaks, context);
            }

            // if previous spectrum is the precursor of the current one
            if (previousSpectrum != null && spectrum.getMsLevel() > previousSpectrum.getMsLevel()) {
                lastPrecursorSpectrum = previousSpectrum;
            }

            if (lastPrecursorSpectrum != null) {
                ((MzMLSpectrumID) spectrum.getSpectrumID()).setParentScanNumber(lastPrecursorSpectrum.getScanNumberList().getFirst());
            }

            previousSpectrum = spectrum;

            updateProgress();
            return spectrum;
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    /**
     * Create a spectrum
     *
     * @param scanElement the container providing data to spectrum
     * @return a new instance of MsnSpectrum<M>
     */
    protected MsnSpectrum newSpectrum(StartElement scanElement, ParseContext context) throws XMLStreamException {
        MsnSpectrum spectrum = new MsnSpectrum(6, precision, true);
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
        spectrumID.setMSDataID(context.getSource());
        context.setColumnNumber(scanElement.getLocation().getColumnNumber());

        int scan = Integer.parseInt(XMLUtils.mandatoryAttr(scanElement, MzXMLTag.ATTR_NUM));
        spectrumID.addScanNumber(scan);

        String title = "scan=" + scan;
        spectrumID.setTitle(title);
        spectrumID.setID(title);
        spectrum.setMsLevel(Integer.parseInt(XMLUtils.mandatoryAttr(scanElement, MzXMLTag.ATTR_MS_LEVEL)));

        String optRetentionTime = XMLUtils.attr(scanElement, MzXMLTag.ATTR_RETENTION_TIME);
        if (optRetentionTime != null) {
            RetentionTime rt = mzXMLDuration2retentionTime(optRetentionTime);
            spectrumID.addRetentionTime(rt);
        }

        String optCE = XMLUtils.attr(scanElement, MzXMLTag.ATTR_COLLISION_ENERGY);
        if (optCE != null) {
            spectrumID.setCollisionEnergy(Double.parseDouble(optCE));
        }

        String centrioded = XMLUtils.attr(scanElement, MzXMLTag.ATTR_CENTROIDED);
        if (centrioded != null) {
            if (centrioded.equals("1")) {
                spectrum.setSpectrumType(SpectrumType.CENTROIDED);
            }
        }

        String startMz = XMLUtils.attr(scanElement, MzXMLTag.ATTR_START_MZ);
        if (startMz != null) {
            spectrumID.setScanLowerMz(Double.parseDouble(startMz));
        }
        String endMz = XMLUtils.attr(scanElement, MzXMLTag.ATTR_END_MZ);
        if (endMz != null) {
            spectrumID.setScanUpperMz(Double.parseDouble(endMz));
        }

        String scanType = XMLUtils.attr(scanElement, MzXMLTag.ATTR_SCAN_TYPE);
        if (scanType != null) {
            switch (scanType) {
                case "Full":
                    spectrumID.setScanType(ScanType.FULLMS);
                    break;
                case "zoom":
                    spectrumID.setScanType(ScanType.zoom);
                    break;
                case "SIM":
                    spectrumID.setScanType(ScanType.SIM);
                    break;
                case "SRM":
                    spectrumID.setScanType(ScanType.SRM);
                    break;
                default:
                    spectrumID.setScanType(ScanType.FULLMS);
                    break;
            }
        }

        return spectrum;
    }

    private void setNewSpectrumPrecursor(MsnSpectrum spectrum, StartElement precMzStartElement) throws
            XMLStreamException {
        // get mandatory precursor intensity
        double precursorIntensity = Double.parseDouble(XMLUtils.mandatoryAttr(precMzStartElement,
                MzXMLTag.TAG_PRECURSOR_INTENSITY));

        // get mandatory precursor mz
        double precursorMz = Double.parseDouble(XMLUtils.getMandatoryXMLEvent(XMLUtils.nextCharacters
                (xmlReader), "missing precursor mz value!").getData());

        // get optional charge
        String optPolarity = XMLUtils.attr(startScan, MzXMLTag.ATTR_POLARITY);
        Polarity polarity = Polarity.UNKNOWN;
        if (optPolarity != null) {
            if ("+".equals(optPolarity))
                polarity = Polarity.POSITIVE;
            else if ("-".equals(optPolarity))
                polarity = Polarity.NEGATIVE;
        }
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
        spectrumID.setPolarity(polarity);

        String optPrecursorCharge = XMLUtils.attr(precMzStartElement, MzXMLTag.ATTR_PRECURSOR_CHARGE);
        Peak precursor;
        if (optPrecursorCharge != null) {
            int precursorCharge = Polarity.getCharge(polarity, Integer.parseInt(optPrecursorCharge));
            precursor = new Peak(precursorMz, precursorIntensity, precursorCharge);
        } else {
            precursor = new Peak(precursorMz, precursorIntensity);
        }

        // get optional activation method
        String optActivationMethod = XMLUtils.attr(precMzStartElement, MzXMLTag.ATTR_ACTIVATION_METHOD);
        if (optActivationMethod != null) {
            spectrumID.setDissociation(Dissociation.valueOfName(optActivationMethod));
        }

        spectrum.setPrecursor(precursor);
    }

    private void handlePeaks(MsnSpectrum spectrum, int expectedPeakCount, StartElement startPeaks, ParseContext context) throws XMLStreamException {
        // parse precision for mzs and intensities primitives
        int bitPrecision = Integer.parseInt(XMLUtils.mandatoryAttr(startPeaks, MzXMLTag.ATTR_PRECISION));

        if (bitPrecision != 32 && bitPrecision != 64)
            throw newXMLStreamException(startPeaks, bitPrecision + ": illegal peaks precision!", context);

        // parse metadata about peak pair order
        String pairOrder = XMLUtils.mandatoryAttr(startPeaks, "pairOrder", MzXMLTag.ATTR_CONTENT_TYPE);

        // decode only m/z-int peak pair order by now
        if ("m/z-int".equals(pairOrder)) {
            // parse base64 peak content
            Characters peaksCharacters = XMLUtils.getMandatoryXMLEvent(XMLUtils.seekNextCharacters(xmlReader),
                    "missing <peaks> base64 data!");

            // decode [and uncompress] Base64 string peaks to bytes
            String optCompressedLen = XMLUtils.attr(startPeaks, MzXMLTag.ATTR_COMPRESSED_LEN);
            int compressedLen = 0;
            if (optCompressedLen != null) {
                compressedLen = Integer.parseInt(optCompressedLen);
            }

            String peaksBase64Data = peaksCharacters.getData();

            // 1st grow if needed (instantiate a bigger array)
            reinitMzsAndIntensities(expectedPeakCount);
            Base64Utils.decode(peaksBase64Data, compressedLen > 0, bitPrecision, expectedPeakCount, mzs, intensities);

            // add peaks to spectrum
            addPeaksToSpectrum(spectrum, mzs, intensities, expectedPeakCount);
        } else {

            throw newXMLStreamException(startPeaks, pairOrder + ": cannot decode peaks pair order (only m/z-int)!", context);
        }
    }

    private void reinitMzsAndIntensities(int expectedPeakCount) {
        if (mzs.length < expectedPeakCount) {

            int factor = expectedPeakCount - mzs.length;

            mzs = ArrayUtils.grow(mzs, factor);
            intensities = ArrayUtils.grow(intensities, factor);
        }

        Arrays.fill(mzs, 0);
        Arrays.fill(intensities, 0);
    }

    private XMLStreamException newXMLStreamException(XMLEvent event, String message, ParseContext context) {
        context.setColumnNumber(event.getLocation().getColumnNumber());

        return new XMLStreamException(message);
    }
}
