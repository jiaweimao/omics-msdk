/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.ParseContext;
import omics.msdk.model.MSHeader;
import omics.util.OmicsException;
import omics.util.io.FileType;
import omics.util.ms.MSDataID;
import omics.util.ms.MSFileType;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.SpectrumID;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Precision;
import omics.util.utils.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents either a PKL file containing multiple spectra
 * separated at least by one empty line or a directory containing multiple
 * pkl files with one spectrum each.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Nov 2018, 3:17 PM
 */
public class PklReader extends MSReader<PeakAnnotation, MsnSpectrum> {

    /**
     * read the first spectrum in a pkl file.
     *
     * @param file a pkl file
     * @return the first spectrum
     */
    public static MsnSpectrum readSpectrum(String file) {
        try {
            PklReader reader = new PklReader(new File(file));
            MsnSpectrum spectrum = reader.next();
            reader.close();

            return spectrum;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<File> fileList;
    private int currentIndex = 0;
    private int spectrumIndex = 0;
    private BufferedReader currentReader;

    public PklReader(File file) {
        super();

        this.fileList = new ArrayList<>();
        if (file.isDirectory()) {
            File[] files = FileType.PKL.listFiles(file);
            Collections.addAll(fileList, files);
        } else {
            fileList.add(file);
        }

        File file1 = fileList.get(0);
        ParseContext context = new ParseContext(file1, new MSDataID(file1.getName(), MSFileType.PKL, file1.getName(), file1.toURI()));
        setContext(context);
        currentReader = context.getCurrentReader();
    }

    @Override
    protected MsnSpectrum parseNextEntry(ParseContext context) throws Exception {
        MsnSpectrum spectrum = readSpectrum(context);
        if (spectrum == null) {
            context.getCurrentReader().close();

            if (currentIndex == (fileList.size() - 1)) { // no more files
                return null;
            }

            currentIndex++;
            File currentFile = fileList.get(currentIndex);
            ParseContext parseContext = new ParseContext(currentFile,
                    new MSDataID(currentFile.getName(), MSFileType.PKL, currentFile.getName(), currentFile.toURI()));
            setContext(parseContext);
            currentReader = parseContext.getCurrentReader();
            spectrum = readSpectrum(context);
        }

        if (fileList.size() > 1) {
            updateProgress(currentIndex + 1, fileList.size());
        } else {
            updateProgress();
        }

        return spectrum;
    }

    private MsnSpectrum readSpectrum(ParseContext context) throws OmicsException, IOException {
        // skip empty line;
        String line;
        while ((line = currentReader.readLine()) != null) {
            if (StringUtils.isNotEmpty(line.trim()))
                break;
        }

        if (line == null)
            return null;

        MsnSpectrum spectrum = new MsnSpectrum(10, Precision.DOUBLE);
        SpectrumID spectrumID = spectrum.getSpectrumID();
        spectrumID.setMSDataID(context.getSource());
        spectrum.setMsLevel(2);
        spectrumID.setID(String.valueOf(spectrumIndex));
        spectrumID.setIndex(spectrumIndex++);

        // the first line should contain m/z, intensity and charge
        String[] headerFields = line.split("\\s+");
        if (headerFields.length != 3) {
            throw new OmicsException("Wrong title format for pkl file: " + context.getSource());
        }

        double mz = Double.parseDouble(headerFields[0]);
        double intensity = Double.parseDouble(headerFields[1]);
        int charge = Integer.parseInt(headerFields[2].replace(".0", "")); // remove possible .0

        Peak precursor = new Peak(mz, intensity, charge);
        spectrum.setPrecursor(precursor);
        while ((line = currentReader.readLine()) != null) {
            if (StringUtils.isEmpty(line))
                break;

            String[] fields = line.split("\\s+");
            if (fields.length != 2) {
                throw new OmicsException("PKL peaks should contains 2 value: " + context.getSource());
            }

            spectrum.add(Double.parseDouble(fields[0]), Double.parseDouble(fields[1]));
        }

        return spectrum;
    }

    @Override
    protected MSHeader parseHeader(ParseContext context) throws Exception {
        return null;
    }
}
