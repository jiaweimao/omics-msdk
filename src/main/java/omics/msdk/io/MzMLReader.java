/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.*;
import omics.msdk.model.*;
import omics.util.OmicsException;
import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;
import omics.util.cv.UserParam;
import omics.util.io.xml.XMLUtils;
import omics.util.ms.*;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessorChain;
import omics.util.ms.peaklist.Precision;
import omics.util.ms.util.MzMLSpectrumID;
import omics.util.utils.ArrayUtils;
import omics.util.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Reader of mzML file.
 *
 * @author JiaweiMao
 * @version 1.3.3
 * @since 08 Oct 2017, 9:21 PM
 */
public class MzMLReader extends MSReader<PeakAnnotation, Spectrum<PeakAnnotation>> {

    private final Logger logger = LoggerFactory.getLogger(MzMLReader.class);

    private final List<TitleParser> titleParsers;
    private XMLEventReader iReader;
    private double[] mzs;
    private double[] intensities;
    private StartElement spectrumStart;
    private MsnSpectrum lastPrecursorSpectrum;
    private MsnSpectrum previousSpectrum;
    private int lastScanNumber = 0;
    private TitleParser currentTitleParser;

    public MzMLReader(File file) throws IOException {

        this(file, Precision.DOUBLE);
    }

    /**
     * Construct with given file and buffer size
     *
     * @param file       a mzML file
     * @param bufferSize buffer size
     */
    public MzMLReader(File file, int bufferSize) throws IOException {

        this(file, bufferSize, new MSDataID(file.getName(), MSFileType.MZML, file.getName(), file.toURI()), Precision.DOUBLE, new PeakProcessorChain<>());
    }

    public MzMLReader(File file, Precision precision) throws IOException {

        this(new FileInputStream(file), new MSDataID(file.getName(), MSFileType.MZML, file.getName(), file.toURI()), precision, new PeakProcessorChain<>());
    }

    public MzMLReader(File file, int bufferSize, MSDataID source, Precision precision,
            PeakProcessorChain<PeakAnnotation> processorChain) throws IOException {

        super(file, bufferSize, source, precision, processorChain);

        lastPrecursorSpectrum = previousSpectrum = null;
        mzs = new double[100];
        intensities = new double[100];

        titleParsers = new ArrayList<>();
        ServiceLoader<TitleParser> loader = ServiceLoader.load(TitleParser.class);
        for (TitleParser parser : loader) {
            this.titleParsers.add(parser);
        }
        if (titleParsers.isEmpty()) {
            throw new IllegalStateException("No TitleParser implementation class found: org.openide.util.Lookup has " +
                    "not properly worked (hint: check that Annotation Compiler Processing is enable)");
        } else {
            currentTitleParser = titleParsers.get(0);
        }
        updateMessage("Reading mzML file: " + source);
    }

    public MzMLReader(InputStream inputStream, MSDataID source, Precision precision, PeakProcessorChain<PeakAnnotation>
            processorChain) throws IOException {

        super(inputStream, source, precision, processorChain);

        lastPrecursorSpectrum = previousSpectrum = null;
        mzs = new double[100];
        intensities = new double[100];

        titleParsers = new ArrayList<>();
        ServiceLoader<TitleParser> loader = ServiceLoader.load(TitleParser.class);
        for (TitleParser parser : loader) {
            this.titleParsers.add(parser);
        }
        if (titleParsers.isEmpty()) {
            throw new IllegalStateException("No TitleParser implementation class found: org.openide.util.Lookup has " +
                    "not properly worked (hint: check that Annotation Compiler Processing is enable)");
        } else {
            currentTitleParser = titleParsers.get(0);
        }
        updateMessage("Reading mzML file: " + source);
    }

    @Override
    protected void init(ParseContext parseContext) throws IOException {

        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        xmlif.setProperty("javax.xml.stream.isCoalescing", Boolean.TRUE);
        try {
            this.iReader = xmlif.createXMLEventReader(parseContext.getCurrentReader());
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    protected Spectrum<PeakAnnotation> parseNextEntry(ParseContext context) throws XMLStreamException, OmicsException {

        Optional<StartElement> spectrumOpt = XMLUtils.toStartElement(iReader, MzMLTags.TAG_SPECTRUM, MzMLTags.TAG_CHROMATOGRAM);
        if (!spectrumOpt.isPresent()) {
            context.setEndOfParsing(true);
            return null;
        }

        spectrumStart = spectrumOpt.get();
        String tagName = spectrumStart.getName().getLocalPart();
        if (tagName.equals(MzMLTags.TAG_SPECTRUM)) {
            MsnSpectrum spectrum = readSpectrum(spectrumStart);
            spectrum.getSpectrumID().setMSDataID(context.getSource());

            if (previousSpectrum != null && spectrum.getMsLevel() > previousSpectrum.getMsLevel())
                lastPrecursorSpectrum = previousSpectrum;

            if (lastPrecursorSpectrum != null) {
//                if (spectrum.getScanNumber().getValue() == 8862)
//                    System.out.println("Found");
                ((MzMLSpectrumID) (spectrum.getSpectrumID())).setParentScanNumber(lastPrecursorSpectrum.getScanNumberList().getFirst());
            }

            previousSpectrum = spectrum;
            updateProgress();

            return spectrum;
        } else if (tagName.equals(MzMLTags.TAG_CHROMATOGRAM)) {
            Chromatogram chromatogram = readChromatogram(spectrumStart);
            updateProgress();
            return chromatogram;
        }

        return null;
    }

    private boolean parseTitleTag(String value, MsnSpectrum spectrum) {
        boolean parsed = currentTitleParser.parse(value, spectrum);
        if (!parsed) {
            for (TitleParser titleParser : titleParsers) {
                if (titleParser.parse(value, spectrum)) {
                    parsed = true;
                    currentTitleParser = titleParser;
                    break;
                }
            }
        }
        spectrum.getSpectrumID().setTitle(value);
        return parsed;
    }

    private Chromatogram readChromatogram(StartElement startElement) throws XMLStreamException, OmicsException {

        Chromatogram chromatogram = new Chromatogram();
        chromatogram.setID(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID));
        chromatogram.setIndex(Integer.parseInt(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_INDEX)));

        int length = Integer.parseInt(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_DEFAULT_ARRAY_LENGTH));
        reinitMzsAndIntensities(length);

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_CHROMATOGRAM))
                break;
            if (event.isStartElement()) {
                StartElement cmStart = event.asStartElement();
                String localName = cmStart.getName().getLocalPart();
                if (localName.equals(MzMLTags.TAG_BINARY_DATA_ARRAY)) {
                    BinaryDataArray binaryDataArray = readBinaryDataArray(cmStart, length);
                    DataType dataType = binaryDataArray.getDataType();
                    double[] values = binaryDataArray.decode2Double();
                    if (dataType == DataType.TIME) {
                        System.arraycopy(values, 0, mzs, 0, length);
                    } else {
                        System.arraycopy(values, 0, intensities, 0, length);
                    }
                } else if (localName.equals(MzMLTags.TAG_CV_PARAM)) {
                    String acc = XMLUtils.mandatoryAttr(cmStart, MzMLTags.ATTR_ACCESSION);
                    for (ChromatogramType type : ChromatogramType.values()) {
                        if (acc.equals(type.getAccession())) {
                            chromatogram.setChromatogramType(type);
                            break;
                        }
                    }
                }
            }
        }

        addPeaksToSpectrum(chromatogram, mzs, intensities, length);
        return chromatogram;
    }

    private MsnSpectrum readSpectrum(StartElement startElement) throws XMLStreamException, OmicsException {
        int size = Integer.parseInt(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_DEFAULT_ARRAY_LENGTH));
        MsnSpectrum spectrum = new MsnSpectrum(size, precision);
        MzMLSpectrumID spectrumID = new MzMLSpectrumID();
        spectrum.setSpectrumID(spectrumID);
        reinitMzsAndIntensities(size);

        int index = Integer.parseInt(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_INDEX));
        String id = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID);

        spectrumID.setIndex(index);
        spectrumID.setID(id);

        double[] mzValues = null;
        double[] inValues = null;
        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_SPECTRUM))
                break;

            if (event.isStartElement()) {
                StartElement spectrumStart = event.asStartElement();
                String localName = spectrumStart.getName().getLocalPart();

                switch (localName) {
                    case MzMLTags.TAG_CV_PARAM: {
                        String acc = XMLUtils.mandatoryAttr(spectrumStart, MzMLTags.ATTR_ACCESSION);
                        switch (acc) {
                            case MzMLTags.ACC_MS1:
                                spectrum.setMsLevel(1);
                                break;
                            case CvTermList.ACC_MS_LEVEL:
                                spectrum.setMsLevel(Integer.parseInt(XMLUtils.mandatoryAttr(spectrumStart, MzMLTags.ATTR_VALUE)));
                                break;
                            case CvTermList.ACC_POSITIVE_SCAN:
                                spectrumID.setPolarity(Polarity.POSITIVE);
                                break;
                            case CvTermList.ACC_NEGATIVE_SCAN:
                                spectrumID.setPolarity(Polarity.NEGATIVE);
                                break;
                            case CvTermList.ACC_CENTROID_SPECTRUM:
                                spectrum.setSpectrumType(SpectrumType.CENTROIDED);
                                break;
                            case CvTermList.ACC_PROFILE_SPECTRUM:
                                spectrum.setSpectrumType(SpectrumType.PROFILE);
                                break;
                            case CvTermList.ACC_SPECTRUM_TITLE: {
                                // extract scan number
                                String title = XMLUtils.attr(spectrumStart, MzMLTags.ATTR_VALUE);
                                if (StringUtils.isNotEmpty(title)) {
                                    boolean parsed = parseTitleTag(title, spectrum);
                                    if (!parsed) {
                                        lastScanNumber = lastScanNumber + 1;
                                        spectrumID.addScanNumber(lastScanNumber);
                                    }
                                }
                                break;
                            }
                            default:
                                break;
                        }
                        break;
                    }
                    case MzMLTags.TAG_SCAN_LIST:
                        readScanList(spectrum);
                        break;
                    case MzMLTags.TAG_PRECURSOR_LIST:
                        readPrecursorList(spectrum);
                        break;
                    case MzMLTags.TAG_BINARY_DATA_ARRAY:
                        BinaryDataArray binaryDataArray = readBinaryDataArray(spectrumStart, size);
                        if (binaryDataArray.getDataType() == DataType.MZ) {
                            mzValues = binaryDataArray.decode2Double();
                        } else {
                            inValues = binaryDataArray.decode2Double();
                        }
                        break;
                }
            }
        }

        if (mzValues == null || inValues == null) {
            throw new OmicsException("BinaryDataArray for " + id + " is not present.");
        }

        if (spectrumID.getTitle() == null) {
            boolean parsed = parseTitleTag(id, spectrum);
            if (!parsed) {
                lastScanNumber = lastScanNumber + 1;
                spectrumID.addScanNumber(lastScanNumber);
            }
        }

        addPeaksToSpectrum(spectrum, mzValues, inValues, size);

        return spectrum;
    }

    private void reinitMzsAndIntensities(int expectedPeakCount) {
        if (mzs.length < expectedPeakCount) {
            int factor = expectedPeakCount - mzs.length;

            mzs = ArrayUtils.grow(mzs, factor);
            intensities = ArrayUtils.grow(intensities, factor);
        }

        Arrays.fill(mzs, 0);
        Arrays.fill(intensities, 0);
    }

    /**
     * List and descriptions of scans.
     */
    private void readScanList(MsnSpectrum spectrum) throws XMLStreamException {
        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_SCAN_LIST))
                break;
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String localName = startElement.getName().getLocalPart();
                if (localName.equals(MzMLTags.TAG_CV_PARAM)) {
                    String acc = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ACCESSION);
                    switch (acc) {
                        case CvTermList.ACC_SCAN_START_TIME:
                            String unitAccession = XMLUtils.attr(startElement, MzMLTags.ATTR_UNIT_ACCESSION);
                            double rt = Double.parseDouble(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_VALUE));
                            if (unitAccession == null || unitAccession.equals(CvTermList.ACC_UNIT_MIN)
                                    || !unitAccession.equals(CvTermList.ACC_UNIT_SECOND))
                                rt = rt * 60;
                            spectrumID.addRetentionTime(new RetentionTimeDiscrete(rt));
                            break;
                        case CvTermList.ACC_ION_INJECTION_TIME:
                            spectrumID.setIonInjectTime(Float.parseFloat(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_VALUE)));
                            break;
                        default:
                            spectrum.addPM(acc, XMLUtils.attr(startElement, MzMLTags.ATTR_VALUE));
                            break;
                    }
                } else if (localName.equals(MzMLTags.TAG_SCAN_WINDOW_LIST)) { // optional
                    while (iReader.hasNext()) {
                        XMLEvent winEvent = iReader.nextEvent();
                        if (XMLUtils.isEndElement(winEvent, MzMLTags.TAG_SCAN_WINDOW_LIST))
                            break;
                        if (XMLUtils.isStartElement(winEvent, MzMLTags.TAG_CV_PARAM)) {
                            StartElement winStart = winEvent.asStartElement();
                            String winAcc = XMLUtils.mandatoryAttr(winStart, MzMLTags.ATTR_ACCESSION);
                            if (winAcc.equals(CvTermList.ACC_SCAN_WINDOW_LOWER_LIMIT)) {
                                double lowLimit = Double.parseDouble(XMLUtils.mandatoryAttr(winStart, MzMLTags.ATTR_VALUE));
                                spectrumID.setScanLowerMz(lowLimit);
                            } else if (winAcc.equals(CvTermList.ACC_SCAN_WINDOW_UPPER_LIMIT)) {
                                double limit = Double.parseDouble(XMLUtils.mandatoryAttr(winStart, MzMLTags.ATTR_VALUE));
                                spectrumID.setScanUpperMz(limit);
                            }
                        }
                    }
                }
            }
        }
    }

    private void readPrecursorList(MsnSpectrum spectrum) throws XMLStreamException {

        MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();
        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_PRECURSOR_LIST))
                break;

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String localName = startElement.getName().getLocalPart();
                switch (localName) {
                    case MzMLTags.TAG_ISOLATION_WINDOW: {
                        while (iReader.hasNext()) {
                            XMLEvent isoEvent = iReader.nextEvent();
                            if (XMLUtils.isEndElement(isoEvent, MzMLTags.TAG_ISOLATION_WINDOW))
                                break;
                            if (XMLUtils.isStartElement(isoEvent, MzMLTags.TAG_CV_PARAM)) {
                                StartElement isoStart = isoEvent.asStartElement();
                                String acc = XMLUtils.mandatoryAttr(isoStart, MzMLTags.ATTR_ACCESSION);
                                switch (acc) {
                                    case CvTermList.ACC_ISOLATION_WINDOW_TARGET:
                                        spectrumID.setIsolateWindowTargetMz(Double.parseDouble(XMLUtils.mandatoryAttr(isoStart, MzMLTags.ATTR_VALUE)));
                                        break;
                                    case CvTermList.ACC_ISOLATION_WINDOW_LOWER:
                                        spectrumID.setIsolateWindowLowerOffset(Double.parseDouble(XMLUtils.mandatoryAttr(isoStart, MzMLTags.ATTR_VALUE)));
                                        break;
                                    case CvTermList.ACC_ISOLATION_WINDOW_UPPER:
                                        spectrumID.setIsolateWindowUpperOffset(Double.parseDouble(XMLUtils.mandatoryAttr(isoStart, MzMLTags.ATTR_VALUE)));
                                        break;
                                    case CvTermList.ACC_ISOLATION_WIDTH:
                                        double windowWidth = Double.parseDouble(XMLUtils.mandatoryAttr(isoStart, MzMLTags.ATTR_VALUE));
                                        double halfWidth = windowWidth / 2;
                                        spectrumID.setIsolateWindowUpperOffset(halfWidth);
                                        spectrumID.setIsolateWindowLowerOffset(halfWidth);
                                        break;
                                }
                            }
                        }
                        break;
                    }
                    case MzMLTags.TAG_SELECTED_ION: {
                        Double pMz = null;
                        Integer pCharge = null;
                        double pIntensity = 0.; // sometimes it is really not present
                        while (iReader.hasNext()) {
                            XMLEvent siEvent = iReader.nextEvent();
                            if (XMLUtils.isEndElement(siEvent, MzMLTags.TAG_SELECTED_ION))
                                break;
                            if (siEvent.isStartElement()) {
                                StartElement siStart = siEvent.asStartElement();
                                String siLocal = siStart.getName().getLocalPart();
                                if (siLocal.equals(MzMLTags.TAG_CV_PARAM)) {
                                    String acc = XMLUtils.mandatoryAttr(siStart, MzMLTags.ATTR_ACCESSION);
                                    switch (acc) {
                                        case CvTermList.ACC_SELECTED_ION_MZ:
                                        case CvTermList.ACC_UNIT_MZ: // some wrong use this as the mz
                                            pMz = Double.parseDouble(XMLUtils.mandatoryAttr(siStart, MzMLTags.ATTR_VALUE));
                                            break;
                                        case CvTermList.ACC_SELECTED_ION_PEAK_INTENSITY:
                                            pIntensity = Double.parseDouble(XMLUtils.mandatoryAttr(siStart, MzMLTags.ATTR_VALUE));
                                            break;
                                        case CvTermList.ACC_SELECTED_ION_CHARGE_STATE:
                                        case MzMLTags.ACC_POSSIBLE_CHARGE:
                                            pCharge = Integer.parseInt(XMLUtils.mandatoryAttr(siStart, MzMLTags.ATTR_VALUE));
                                            break;
                                        default:
                                            logger.warn("Unknown cvParam {} in selectedIOn.", acc);
                                    }

                                }
                            }
                        }
                        spectrum.getPrecursor().setValues(pMz, pIntensity, pCharge);
                        break;
                    }
                    case MzMLTags.TAG_ACTIVATION: {    // required
                        while (iReader.hasNext()) {
                            XMLEvent acEvent = iReader.nextEvent();
                            if (XMLUtils.isEndElement(acEvent, MzMLTags.TAG_ACTIVATION))
                                break;
                            if (acEvent.isStartElement()) {
                                StartElement acStart = acEvent.asStartElement();
                                String acLocalName = acStart.getName().getLocalPart();
                                if (acLocalName.equals(MzMLTags.TAG_CV_PARAM)) {
                                    String acAcc = XMLUtils.mandatoryAttr(acStart, MzMLTags.ATTR_ACCESSION);

                                    if (acAcc.equals(CvTermList.ACC_COLLISION_ENERGY) || acAcc.equals(MzMLTags.ACC_ACTIVATION_ENERGY))
                                        spectrumID.setCollisionEnergy(Double.parseDouble(XMLUtils.mandatoryAttr(acStart, MzMLTags.ATTR_VALUE)));
                                    else {
                                        Dissociation dis = Dissociation.valueOfAcc(acAcc);
                                        spectrumID.setDissociation(dis);
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    private BinaryDataArray readBinaryDataArray(StartElement startElement, int defaultArrayLength) throws XMLStreamException {

        int encodedLength = Integer.parseInt(XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ENCODED_LENGTH));
        String arrayLengthAtt = XMLUtils.attr(startElement, MzMLTags.ATTR_ARRAY_LENGTH);
        int arrayLength = defaultArrayLength;
        if (arrayLengthAtt != null)
            arrayLength = Integer.parseInt(arrayLengthAtt);

        BinaryDataArray binaryDataArray = new BinaryDataArray(encodedLength, arrayLength);
        boolean inBinary = false;
        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localPart = endElement.getName().getLocalPart();
                if (localPart.equals(MzMLTags.TAG_BINARY_DATA_ARRAY))
                    break;
                else if (localPart.equals(MzMLTags.TAG_BINARY)) {
                    inBinary = false;
                }
            } else if (event.isStartElement()) {
                StartElement bdaStart = event.asStartElement();
                String bdaLocal = bdaStart.getName().getLocalPart();
                if (bdaLocal.equals(MzMLTags.TAG_CV_PARAM)) {
                    CvParam param = readCvParam(bdaStart);
                    binaryDataArray.addCvParam(param);
                } else if (bdaLocal.equals(MzMLTags.TAG_BINARY)) {
                    inBinary = true;
                }
            } else if (event.isCharacters() && inBinary) {
                binaryDataArray.setStringAsBinaryData(event.asCharacters().getData());
            }
        }

        return binaryDataArray;
    }

    @Override
    protected MSHeader parseHeader(ParseContext context) throws XMLStreamException {
        MSHeader header = new MSHeader();

        Optional<StartElement> mzMLOpt = XMLUtils.toStartElement(iReader, MzMLTags.TAG_MZML);
        if (!mzMLOpt.isPresent()) {
            throw new XMLStreamException("missing mzML start element!");
        }

        while (iReader.hasNext()) {
            XMLEvent event = iReader.peek();

            if (XMLUtils.isStartElement(event, MzMLTags.TAG_SPECTRUM_LIST))
                break;

            iReader.nextEvent();
            if (event.isStartElement()) {
                StartElement stElement = event.asStartElement();
                String localName = stElement.getName().getLocalPart();
                switch (localName) {
                    case MzMLTags.TAG_CV_LIST:
                        readCvList(header);
                        break;
                    case MzMLTags.TAG_FILE_DESCRIPTION:
                        readFileDescription(header);
                        break;
                    case MzMLTags.TAG_REF_PARAM_GROUP_LIST:
                        readReferenceableParamGroupList(header);
                        break;
                    case MzMLTags.TAG_SAMPLE_LIST:
                        readSampleList(header);
                        break;
                    case MzMLTags.TAG_SOFTWARE_LIST:
                        readSoftwareList(header);
                        break;
                    case MzMLTags.TAG_INSTRUMENT_CONFIGURATION_LIST:
                        readInstrumentConfigurationList(header);
                        break;
                    case MzMLTags.TAG_DATA_PROCESSING_LIST:
                        readDataProcessingList(header);
                        break;
                    case MzMLTags.TAG_RUN: {
                        header.setRunId(XMLUtils.mandatoryAttr(stElement, MzMLTags.ATTR_ID));
                        header.setDefaultInstrumentConfigurationRef(XMLUtils.mandatoryAttr(stElement, MzMLTags.ATTR_DEFAULT_INSTRUMENT_CONFIGURATION_REF));
                        break;
                    }
                }
            }
        }

        Optional<StartElement> slistOp = XMLUtils.toStartElement(iReader, MzMLTags.TAG_SPECTRUM_LIST);
        if (!slistOp.isPresent()) {
            throw new XMLStreamException("Missing spectrumList node!");
        }

        StartElement slistStart = slistOp.get();
        int expectedScanNumber = Integer.parseInt(XMLUtils.attr(slistStart, MzMLTags.ATTR_COUNT));
        header.setScanCount(expectedScanNumber);

        String defaultDPR = XMLUtils.mandatoryAttr(slistStart, MzMLTags.ATTR_DEFAULT_DATA_PROCESSING_REF);
        header.setDefaultDataProcessingRef(defaultDPR);

        return header;
    }

    private Map<String, Cv> cvMap = new HashMap<>();

    private void readCvList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent evt = iReader.nextEvent();
            if (XMLUtils.isEndElement(evt, MzMLTags.TAG_CV_LIST))
                break;

            if (XMLUtils.isStartElement(evt, MzMLTags.TAG_CV)) {
                StartElement startElement = evt.asStartElement();
                String id = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID);
//                String fullName = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_FULL_NAME);
//                String version = XMLUtils.attr(startElement, MzMLTags.ATTR_VERSION);
//                String uri = XMLUtils.mandatoryAttr(startElement, MzMLTags.TAG_URI);
                if (id.equals(Cv.PSI_MS.getID())) {
                    header.addCv(Cv.PSI_MS);
                    cvMap.put(id, Cv.PSI_MS);
                } else if (id.equals(Cv.UNIT_ONTOLOGY.getID())) {
                    header.addCv(Cv.UNIT_ONTOLOGY);
                    cvMap.put(id, Cv.UNIT_ONTOLOGY);
                }
            }
        }
    }

    /**
     * 2. read fileDescription
     */
    private void readFileDescription(MSHeader header) throws XMLStreamException {

        FileDescription fileDescription = new FileDescription();
        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_FILE_DESCRIPTION))
                break;

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String localName = startElement.getName().getLocalPart();
                switch (localName) {
                    case MzMLTags.TAG_FILE_CONTENT:
                        ParamGroup fileContent = new ParamGroup();
                        while (iReader.hasNext()) {
                            XMLEvent fcEvt = iReader.nextEvent();
                            if (XMLUtils.isEndElement(fcEvt, MzMLTags.TAG_FILE_CONTENT))
                                break;
                            if (XMLUtils.isStartElement(fcEvt, MzMLTags.TAG_CV_PARAM))
                                fileContent.addCvParam(readCvParam(fcEvt.asStartElement()));
                        }
                        fileDescription.setFileContent(fileContent);
                        break;
                    case MzMLTags.TAG_SOURCE_FILE:
                        String id = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID);
                        String name = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_NAME);
                        String location = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_LOCATION);
                        SourceFile sourceFile = new SourceFile(id, name, location);

                        while (iReader.hasNext()) {
                            XMLEvent sfEvent = iReader.nextEvent();
                            if (XMLUtils.isEndElement(sfEvent, MzMLTags.TAG_SOURCE_FILE))
                                break;
                            if (XMLUtils.isStartElement(sfEvent, MzMLTags.TAG_CV_PARAM)) {
                                sourceFile.addCvParam(readCvParam(sfEvent.asStartElement()));
                            }
                        }
                        fileDescription.addSourceFile(sourceFile);
                        break;
                    case MzMLTags.TAG_CONTACT:
                        ParamGroup paramGroup = new ParamGroup();
                        while (iReader.hasNext()) {
                            XMLEvent contactEvt = iReader.nextEvent();
                            if (XMLUtils.isEndElement(contactEvt, MzMLTags.TAG_CONTACT))
                                break;
                            if (XMLUtils.isStartElement(contactEvt, MzMLTags.TAG_CV_PARAM)) {
                                paramGroup.addCvParam(readCvParam(contactEvt.asStartElement()));
                            }
                        }
                        fileDescription.addContact(paramGroup);
                        break;
                }
            }
        }

        header.setFileDescription(fileDescription);
    }


    /**
     * 3. referenceableParamGroupList
     */
    private void readReferenceableParamGroupList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_REF_PARAM_GROUP_LIST))
                break;
            if (XMLUtils.isStartElement(event, MzMLTags.TAG_REF_PARAM_GROUP)) {
                String id = XMLUtils.mandatoryAttr(event.asStartElement(), MzMLTags.ATTR_ID);
                ReferenceableParamGroup referenceableParamGroup = new ReferenceableParamGroup(id);
                while (iReader.hasNext()) {
                    XMLEvent rpgEvt = iReader.nextEvent();
                    if (XMLUtils.isEndElement(rpgEvt, MzMLTags.TAG_REF_PARAM_GROUP))
                        break;
                    if (XMLUtils.isStartElement(rpgEvt, MzMLTags.TAG_CV_PARAM)) {
                        referenceableParamGroup.addCvParam(readCvParam(rpgEvt.asStartElement()));
                    }
                }
                header.addReferenceableParamGroup(referenceableParamGroup);
            }
        }
    }

    /**
     * 4. sampleList
     */
    private void readSampleList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_SAMPLE_LIST))
                break;
            if (XMLUtils.isStartElement(event, MzMLTags.TAG_SAMPLE)) {
                StartElement startElement = event.asStartElement();
                String id = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID);
                String name = XMLUtils.attr(startElement, MzMLTags.ATTR_NAME);
                Sample sample = new Sample(id, name);

                while (iReader.hasNext()) {
                    XMLEvent sampleEvent = iReader.nextEvent();
                    if (XMLUtils.isEndElement(sampleEvent, MzMLTags.TAG_SAMPLE))
                        break;
                    if (XMLUtils.isStartElement(sampleEvent, MzMLTags.TAG_CV_PARAM)) {
                        sample.addCvParam(readCvParam(sampleEvent.asStartElement()));
                    }
                }

                header.addSample(sample);
            }
        }
    }

    /**
     * 5. softwareList
     */
    private void readSoftwareList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();

            if (XMLUtils.isEndElement(event, MzMLTags.TAG_SOFTWARE_LIST))
                break;
            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                String id = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ID);
                String version = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_VERSION);
                Software software = new Software(id, version);
                while (iReader.hasNext()) {
                    XMLEvent softEvent = iReader.nextEvent();
                    if (XMLUtils.isEndElement(softEvent, MzMLTags.TAG_SOFTWARE))
                        break;
                    if (XMLUtils.isStartElement(softEvent, MzMLTags.TAG_CV_PARAM)) {
                        software.addCvParam(readCvParam(softEvent.asStartElement()));
                    }
                }
                header.addSoftware(software);
            }
        }
    }

    /**
     * 6. instrumentConfigurationList
     */
    private void readInstrumentConfigurationList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_INSTRUMENT_CONFIGURATION_LIST))
                break;
            if (XMLUtils.isStartElement(event, MzMLTags.TAG_INSTRUMENT_CONFIGURATION)) {
                String id = XMLUtils.mandatoryAttr(event.asStartElement(), MzMLTags.ATTR_ID);
                InstrumentConfiguration ic = new InstrumentConfiguration(id);

                while (iReader.hasNext()) {
                    XMLEvent icEvent = iReader.nextEvent();
                    if (XMLUtils.isEndElement(icEvent, MzMLTags.TAG_INSTRUMENT_CONFIGURATION)) {
                        header.addInstrumentConfiguration(ic);
                        break;
                    }

                    if (icEvent.isStartElement()) {
                        StartElement icStart = icEvent.asStartElement();
                        String localName = icStart.getName().getLocalPart();
                        switch (localName) {
                            case MzMLTags.TAG_REF_PARAM_GROUP_REF:
                                ic.addReferenceableParamGroupRef(XMLUtils.mandatoryAttr(icStart, MzMLTags.ATTR_REF));
                                break;
                            case MzMLTags.TAG_CV_PARAM:
                                CvParam cvParam = readCvParam(icStart);
                                ic.addCvParam(cvParam);
                                break;
                            case MzMLTags.ATTR_SOFTWARE_REF:
                                String softwareRef = XMLUtils.mandatoryAttr(icStart, MzMLTags.ATTR_REF);
                                ic.setSoftwareRef(softwareRef);
                                break;
                            case MzMLTags.TAG_COMPONENT_LIST:
                                while (iReader.hasNext()) {
                                    XMLEvent comEvent = iReader.nextEvent();
                                    if (XMLUtils.isEndElement(comEvent, MzMLTags.TAG_COMPONENT_LIST))
                                        break;
                                    if (comEvent.isStartElement()) {
                                        StartElement comStart = comEvent.asStartElement();
                                        String compLocalName = comStart.getName().getLocalPart();
                                        switch (compLocalName) {
                                            case "source": {
                                                int order = Integer.parseInt(XMLUtils.mandatoryAttr(comStart, "order"));
                                                IonSource component = new IonSource();
                                                component.setOrder(order);
                                                while (iReader.hasNext()) {
                                                    XMLEvent cvEvent = iReader.nextEvent();
                                                    if (XMLUtils.isEndElement(cvEvent, "source"))
                                                        break;
                                                    if (cvEvent.isStartElement()) {
                                                        StartElement cvStart = cvEvent.asStartElement();
                                                        if (cvStart.getName().getLocalPart().equals(MzMLTags.TAG_CV_PARAM)) {
                                                            CvParam comParam = readCvParam(cvStart);
                                                            component.addCvParam(comParam);
                                                        }
                                                    }
                                                }
                                                ic.addComponent(component);
                                                break;
                                            }
                                            case "analyzer": {
                                                int order = Integer.parseInt(XMLUtils.mandatoryAttr(comStart, "order"));
                                                IonSource component = new IonSource();
                                                component.setOrder(order);
                                                while (iReader.hasNext()) {
                                                    XMLEvent cvEvent = iReader.nextEvent();
                                                    if (cvEvent.isEndElement() && cvEvent.asEndElement().getName().getLocalPart().equals("analyzer"))
                                                        break;
                                                    if (cvEvent.isStartElement()) {
                                                        StartElement cvStart = cvEvent.asStartElement();
                                                        if (cvStart.getName().getLocalPart().equals(MzMLTags.TAG_CV_PARAM)) {
                                                            CvParam comParam = readCvParam(cvStart);
                                                            component.addCvParam(comParam);
                                                        }
                                                    }
                                                }
                                                ic.addComponent(component);
                                                break;
                                            }
                                            case "detector": {
                                                int order = Integer.parseInt(XMLUtils.mandatoryAttr(comStart, "order"));
                                                Detector component = new Detector();
                                                component.setOrder(order);
                                                while (iReader.hasNext()) {
                                                    XMLEvent cvEvent = iReader.nextEvent();
                                                    if (cvEvent.isEndElement() && cvEvent.asEndElement().getName().getLocalPart().equals("detector"))
                                                        break;
                                                    if (cvEvent.isStartElement()) {
                                                        StartElement cvStart = cvEvent.asStartElement();
                                                        if (cvStart.getName().getLocalPart().equals(MzMLTags.TAG_CV_PARAM)) {
                                                            CvParam comParam = readCvParam(cvStart);
                                                            component.addCvParam(comParam);
                                                        }
                                                    }
                                                }
                                                ic.addComponent(component);
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
    }


    /**
     * 7. dataProcessingList
     */
    private void readDataProcessingList(MSHeader header) throws XMLStreamException {

        while (iReader.hasNext()) {
            XMLEvent event = iReader.nextEvent();
            if (XMLUtils.isEndElement(event, MzMLTags.TAG_DATA_PROCESSING_LIST))
                break;

            if (XMLUtils.isStartElement(event, MzMLTags.TAG_DATA_PROCESSING)) {
                String id = XMLUtils.mandatoryAttr(event.asStartElement(), MzMLTags.ATTR_ID);
                DataProcessing dataProcessing = new DataProcessing(id);

                while (iReader.hasNext()) {
                    XMLEvent processingEvent = iReader.nextEvent();
                    if (XMLUtils.isEndElement(processingEvent, MzMLTags.TAG_DATA_PROCESSING))
                        break;

                    if (XMLUtils.isStartElement(processingEvent, MzMLTags.TAG_PROCESSING_METHOD)) {
                        StartElement processingStart = processingEvent.asStartElement();
                        Integer order = Integer.parseInt(XMLUtils.mandatoryAttr(processingStart, MzMLTags.ATTR_ORDER));
                        String softwareRef = XMLUtils.mandatoryAttr(processingStart, MzMLTags.ATTR_SOFTWARE_REF);
                        ProcessingMethod processingMethod = new ProcessingMethod();
                        processingMethod.setOrder(order);
                        processingMethod.setSoftwareRef(softwareRef);

                        while (iReader.hasNext()) {
                            XMLEvent methodEvent = iReader.nextEvent();
                            if (XMLUtils.isEndElement(methodEvent, MzMLTags.TAG_PROCESSING_METHOD))
                                break;
                            if (methodEvent.isStartElement()) {
                                StartElement methodStart = methodEvent.asStartElement();
                                String methodLocalName = methodStart.getName().getLocalPart();
                                if (methodLocalName.equals(MzMLTags.TAG_CV_PARAM)) {
                                    CvParam cvParam = readCvParam(methodStart);
                                    processingMethod.addCvParam(cvParam);
                                } else if (methodLocalName.equals(MzMLTags.TAG_USER_PARAM)) {
                                    UserParam userParam = readUserParam(methodStart);
                                    processingMethod.addUserParam(userParam);
                                }
                            }
                        }
                        dataProcessing.addProcessingMethod(processingMethod);
                    }
                }

                header.addDataProcessing(dataProcessing);
            }
        }
    }

    private CvParam readCvParam(StartElement startElement) throws XMLStreamException {

        String cvRef = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_CV_REF);
        String accession = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_ACCESSION);
        String name = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_NAME);
        String value = XMLUtils.attr(startElement, MzMLTags.ATTR_VALUE);

        CvTerm cvTerm = new CvTerm(cvMap.get(cvRef), accession, name);
        CvParam cvParam = new CvParam(cvTerm, value);

        String unitCvRef = XMLUtils.attr(startElement, MzMLTags.ATTR_UNIT_CV_REF);
        String unitAcc = XMLUtils.attr(startElement, MzMLTags.ATTR_UNIT_ACCESSION);
        String unitName = XMLUtils.attr(startElement, MzMLTags.ATTR_UNIT_NAME);
        cvTerm.setUnitCv(cvMap.get(unitCvRef));
        cvTerm.setUnitAccession(unitAcc);
        cvTerm.setUnitName(unitName);

        return cvParam;
    }

    private UserParam readUserParam(StartElement startElement) throws XMLStreamException {

        String name = XMLUtils.mandatoryAttr(startElement, MzMLTags.ATTR_NAME);

        UserParam userParam = new UserParam();
        userParam.setName(name);
        userParam.setValue(XMLUtils.attr(startElement, MzMLTags.ATTR_VALUE));
        userParam.setType(XMLUtils.attr(startElement, MzMLTags.ATTR_USER_PARAM_TYPE));

        return userParam;
    }

    @Override
    public void close() {
        try {
            iReader.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
