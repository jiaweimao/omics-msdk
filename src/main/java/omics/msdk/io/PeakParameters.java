/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.util.utils.NumberFormatFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * format parameters for peaks.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Aug 2018, 10:07 AM
 */
public class PeakParameters {

    public static NumberFormat SCI_FORMAT = new DecimalFormat("0.#####E0");

    private NumberFormat mzFormat;
    private NumberFormat intensityFormat;
    private NumberFormat rtFormat;

    /**
     * Constructor with the default parameters.
     * mz: 6 digits
     * intensity: 6 digits
     * retention time: 3 digits
     */
    public PeakParameters() {
        this.mzFormat = NumberFormatFactory.valueOf(6);
        this.intensityFormat = NumberFormatFactory.valueOf(6);
        this.rtFormat = NumberFormatFactory.valueOf(3);
    }

    public PeakParameters(NumberFormat mzFormat, NumberFormat intensityFormat, NumberFormat rtFormat) {
        this.mzFormat = mzFormat;
        this.intensityFormat = intensityFormat;
        this.rtFormat = rtFormat;
    }

    public NumberFormat mzFormat() {
        return mzFormat;
    }

    public void setMzFormat(NumberFormat mzFormat) {
        this.mzFormat = mzFormat;
    }

    public NumberFormat intensityFormat() {
        return intensityFormat;
    }

    public void setIntensityFormat(NumberFormat intensityFormat) {
        this.intensityFormat = intensityFormat;
    }

    /**
     * @return retention time format of second
     */
    public NumberFormat rtFormat() {
        return rtFormat;
    }

    public void setRtFormat(NumberFormat rtFormat) {
        this.rtFormat = rtFormat;
    }
}
