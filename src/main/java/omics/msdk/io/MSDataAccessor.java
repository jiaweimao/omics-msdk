/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.MSFileTypeDetector;
import omics.msdk.model.MSDataFile;
import omics.util.OmicsTask;
import omics.util.ms.MSFileType;

import java.io.File;


/**
 * class for all Raw data file importer.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 17 Sep 2017, 8:39 PM
 */
public class MSDataAccessor extends OmicsTask<MSDataFile> {

    private final File iFile;

    public MSDataAccessor(File sourceFile) {
        this.iFile = sourceFile;
    }

    @Override
    public void start() throws Exception {
        MSFileType fileType = MSFileTypeDetector.detect(iFile);
        if (fileType == MSFileType.UNKNOWN) {
            updateMessage("Failed to detect type of file: " + iFile);
            return;
        }

        OmicsTask<MSDataFile> task;
        switch (fileType) {
            case MGF:
                task = new MgfAccessor(iFile);
                break;
            case MZML:
                task = new MzMLAccessor(iFile);
                break;
            case MZXML:
                task = new MzXMLAccessor(iFile);
                break;
            default: {
                updateMessage("Unsupported file format: " + iFile);
                return;
            }
        }
        task.messageProperty().addListener(evt -> updateMessage((String) evt.getNewValue()));
        task.titleProperty().addListener(evt -> updateTitle((String) evt.getNewValue()));
        task.exceptionProperty().addListener(evt -> setException((Throwable) evt.getNewValue()));
        task.progressProperty().addListener(evt -> updateProgress((Double) evt.getNewValue(), 1.0));

        task.start();

        updateValue(task.getValue());
    }
}
