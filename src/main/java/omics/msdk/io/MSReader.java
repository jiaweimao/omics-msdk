/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.ParseContext;
import omics.msdk.model.MSHeader;
import omics.util.InfoTask;
import omics.util.io.IterativeReader;
import omics.util.ms.MSDataID;
import omics.util.ms.Spectrum;
import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.PeakProcessorChain;
import omics.util.ms.peaklist.Precision;
import omics.util.ms.peaklist.impl.UnsortedPeakListException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * abstract class for mass spectrum file reader.
 *
 * @author JiaweiMao
 * @version 1.0.1
 * @since 08 Oct 2017, 8:30 PM
 */
public abstract class MSReader<A extends PeakAnnotation, S extends Spectrum<A>> extends InfoTask<Void> implements IterativeReader<S> {

    protected final Precision precision;
    private ParseContext context;
    private final MSHeader header;
    private final PeakProcessorChain<A> processorChain;

    /**
     * a parsed object waiting for a call to T next())
     */
    private S waitingForDelivery;
    /**
     * potential next parsing thrown exception goes there
     */
    private IOException exception;
    private boolean acceptUnsortedSpectra;

    public MSReader() {
        this.precision = Precision.DOUBLE;
        this.processorChain = new PeakProcessorChain<>();
        this.header = null;
        this.context = null;
    }

    /**
     * Construct a MSReader with empty {@link PeakProcessorChain}
     *
     * @param file        source file
     * @param spectraData {@link MSDataID} of the source
     */
    public MSReader(File file, MSDataID spectraData) throws IOException {
        this(file, spectraData, Precision.DOUBLE, new PeakProcessorChain<>());
    }

    /**
     * Construct a MSReader with empty {@link PeakProcessorChain}
     *
     * @param file        source file
     * @param spectraData {@link MSDataID} of the source
     */
    public MSReader(File file, int bufferSize, MSDataID spectraData) throws IOException {
        this(file, bufferSize, spectraData, Precision.DOUBLE, new PeakProcessorChain<>());
    }

    public MSReader(File file, MSDataID source, Precision precision, PeakProcessorChain<A> processorChain) throws IOException {
        this.context = new ParseContext(file, source);
        init(context);
        this.precision = precision;
        MSHeader tmpHeader;
        try {
            tmpHeader = parseHeader(context);
        } catch (Exception e) {
            tmpHeader = null;
            e.printStackTrace();
        }
        header = tmpHeader;

        this.processorChain = processorChain;
    }

    public MSReader(File file, int bufferSize, MSDataID source, Precision precision, PeakProcessorChain<A> processorChain) throws IOException {
        this.context = new ParseContext(file, source, bufferSize);
        init(context);
        this.precision = precision;
        MSHeader tmpHeader;
        try {
            tmpHeader = parseHeader(context);
        } catch (Exception e) {
            tmpHeader = null;
            e.printStackTrace();
        }
        header = tmpHeader;

        this.processorChain = processorChain;
    }


    protected MSReader(InputStream inputStream, MSDataID source, Precision precision, PeakProcessorChain<A> processorChain)
            throws IOException {
        context = new ParseContext(inputStream, source);
        init(context);

        this.precision = precision;
        MSHeader tmpHeader;
        try {
            tmpHeader = parseHeader(context);
        } catch (Exception e) {
            tmpHeader = null;
            e.printStackTrace();
        }
        header = tmpHeader;

        this.processorChain = processorChain;
    }

    protected MSReader(InputStream inputStream, int bufferSize, MSDataID source, Precision precision, PeakProcessorChain<A> processorChain)
            throws IOException {
        context = new ParseContext(inputStream, source, bufferSize);
        init(context);

        this.precision = precision;
        MSHeader tmpHeader;
        try {
            tmpHeader = parseHeader(context);
        } catch (Exception e) {
            tmpHeader = null;
            e.printStackTrace();
        }
        header = tmpHeader;

        this.processorChain = processorChain;
    }

    protected void init(ParseContext parseContext) throws IOException {
    }

    /**
     * @return the next T-object or null if no more object to read.
     */
    protected abstract S parseNextEntry(ParseContext context) throws Exception;

    /**
     * @return the context
     */
    public ParseContext getContext() {
        return context;
    }

    public void setContext(ParseContext context) {
        this.context = context;
    }

    protected void updateProgress() {
        updateProgress(context.getMonitorableInputStream().getProgress(), context.getMonitorableInputStream().getMaximum());
    }

    @Override
    public boolean hasNext() {
        if (context.isEndOfParsing())
            return false;

        // if no object awaiting -> parse next entry
        if (waitingForDelivery == null)
            exception = parseNextEntryAndWaitForDelivery();

        // if no object yet awaiting -> nothing more to parse
        if (waitingForDelivery == null && exception == null) {
            context.setEndOfParsing(true);
            return false;
        }

        return true;
    }

    @Override
    public S next() {
        // parse the next entry if not yet done
        if (exception == null && waitingForDelivery == null)
            exception = parseNextEntryAndWaitForDelivery();

        if (exception != null) {
            // cannot make a new T
            waitingForDelivery = null;

            try {
                throw exception;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // store the next entry
        S spectrum = waitingForDelivery;

        // ready for next parsing
        waitingForDelivery = null;

        if (!processorChain.isEmpty())
            spectrum.apply(processorChain);

        return spectrum;
    }

    @Override
    public void close() throws IOException {
        context.getCurrentReader().close();
    }

    /**
     * Get the next entry and store it before next() is called.
     */
    private IOException parseNextEntryAndWaitForDelivery() {
        try {
            waitingForDelivery = parseNextEntry(context);
            if (waitingForDelivery != null)
                context.incrementParsedEntryNumber();
        } catch (Exception e) {
            // no need to continue the parsing
            waitingForDelivery = null;
            return new IOException(e.getMessage() + ": cannot parse entry, context=[" + context + "]", e);
        }

        return null;
    }

    public void acceptUnsortedSpectra() {
        acceptUnsortedSpectra = true;
    }

    /**
     * This is called on reader initialization. Subclasses should parse any information that occurs before the first
     * spectrum. The information can returned as a string
     *
     * @param context the parser context
     * @return the data that is before the first spectrum
     */
    protected abstract MSHeader parseHeader(ParseContext context) throws Exception;

    /**
     * Get the header string, for mzML, mzXML ... MS file, there are some information except the Spectra. All these
     * information are provided here.
     *
     * @return file header info.
     */
    public MSHeader getHeader() {
        return header;
    }

    /**
     * Potentially non-order add peaks to spectrum.
     *
     * @param spectrum    the spectrum to add peaks into.
     * @param mzs         the peak's m/zs.
     * @param intensities the peak's intensities.
     * @param size        the spectrum size
     * @throws UnsortedPeakListException if reader is not allowed to add unsorted peaklists.
     */
    protected void addPeaksToSpectrum(S spectrum, double[] mzs, double[] intensities, int size) {
        checkNotNull(spectrum);

        if (!acceptUnsortedSpectra) {
            // catch lower-level runtime exception to rethrow appropriate higher-level runtime exception
            try {
                spectrum.addSorted(mzs, intensities, size);
            } catch (UnsortedPeakListException e) {
                throw new UnsortedPeakListException(
                        context + ": cannot read unsorted spectrum. "
                                + "Call method 'acceptUnsortedSpectra()' if your reader has to deal with unsorted " +
                                "peaks (will be sorted internally).\"", e);
            }
        } else {
            // add peaks separately (potentially unordered)
            for (int i = 0; i < size; i++) {
                spectrum.add(mzs[i], intensities[i]);
            }
        }
    }

    /**
     * Potentially non-order add peak to spectrum.
     *
     * @param spectrum  the spectrum to add peak into.
     * @param mz        the peak's m/z.
     * @param intensity the peak's intensity.
     * @param context   the parse context
     * @return the index at which the peak was added
     * @throws UnsortedPeakListException if reader is not allowed to add unsorted peaklists.
     */
    protected int addPeakToSpectrum(Spectrum<A> spectrum, double mz, double intensity, ParseContext context) {
        checkNotNull(spectrum);

        if (!acceptUnsortedSpectra) {
            if (spectrum.isEmpty() || mz >= spectrum.getX(spectrum.size() - 1))
                return spectrum.add(mz, intensity);
            else
                throw new UnsortedPeakListException(context + ": cannot read unsorted spectrum!", spectrum.size() - 1);
        } else {
            return spectrum.add(mz, intensity);
        }
    }
}
