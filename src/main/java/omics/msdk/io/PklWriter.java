/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSHeader;
import omics.util.ms.Chromatogram;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Peak;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Writer spectrum to pkl file.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 10 Nov 2018, 7:01 PM
 */
public class PklWriter implements MSWriter {

    public static void writeSpectrum(String path, MsnSpectrum spectrum) {
        try {
            PklWriter writer = new PklWriter(new File(path));
            writer.writeSpectrum(spectrum);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private final Writer writer;
    private PeakParameters peakParameters;

    public PklWriter(Writer writer, PeakParameters parameters) {
        checkNotNull(writer);
        checkNotNull(parameters);

        this.peakParameters = parameters;
        this.writer = writer;
    }

    public PklWriter(File file, PeakParameters peakParameters) throws IOException {
        this(new FileWriter(file), peakParameters);
    }

    public PklWriter(File file) throws IOException {
        this(file, new PeakParameters());
    }

    @Override
    public void writeStartDocument() throws Exception {
    }

    @Override
    public void writeFileHeader(MSHeader header) throws Exception {
    }

    @Override
    public void writeStartSpectrumList() throws IOException {
    }

    @Override
    public void writeSpectrum(MsnSpectrum spectrum) throws IOException {
        Peak precursor = spectrum.getPrecursor();

        writer.write(peakParameters.mzFormat().format(precursor.getMz()) + "\t");
        writer.write(peakParameters.intensityFormat().format(precursor.getIntensity()) + "\t");
        writer.write(precursor.getCharge() + "\n");

        for (int i = 0; i < spectrum.size(); i++) {
            writer.write(peakParameters.mzFormat().format(spectrum.getMz(i)) + "\t");
            writer.write(peakParameters.intensityFormat().format(spectrum.getIntensity(i)) + "\n");
        }
        writer.write("\n");
    }

    @Override
    public void writeEndSpectrumList() throws IOException {
    }

    @Override
    public void writeStartChromatogramList() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeChromatogram(Chromatogram chromatogram) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeEndChromatogramList() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void writeEndDocument() throws IOException {
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}
