/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.CompressionType;
import omics.msdk.model.MSDataFile;
import omics.util.Action;
import omics.util.ms.Chromatogram;
import omics.util.ms.MsnSpectrum;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * output rawdatafile to mzML.
 * <p>
 * parameters supported with PMs:
 * MzMLTags.AC_SCAN_FILTER_STRING
 * MzMLTags.AC_PRESET_SCAN_CONFIGURATION
 * <p>
 * NOTE:
 * 1. only support 1 precursor.
 * 2. do not support indexed mzML
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 24 Aug 2018, 8:46 PM
 */
public class DoExport2MzML extends Action {

    private MSDataFile rawDataFile;
    private File targetFile;
    private PeakParameters peakParameters;
    private CompressionType mzCompress;
    private CompressionType inCompress;

    public DoExport2MzML(MSDataFile rawDataFile, File targetFile, CompressionType mzCompress, CompressionType inCompress, PeakParameters peakParameters) {
        this.targetFile = targetFile;
        this.rawDataFile = rawDataFile;
        this.peakParameters = peakParameters;
        this.mzCompress = mzCompress;
        this.inCompress = inCompress;
    }

    public DoExport2MzML(MSDataFile rawDataFile, File targetFile, CompressionType mzCompress, CompressionType inCompress) {
        this(rawDataFile, targetFile, mzCompress, inCompress, new PeakParameters());
    }

    public DoExport2MzML(MSDataFile rawDataFile, File targetFile, CompressionType compressionType) {
        this(rawDataFile, targetFile, compressionType, compressionType);
    }

    /**
     * Create with the default the {@link CompressionType#ZLIB}
     *
     * @param rawDataFile {@link MSDataFile} to output
     * @param targetFile  target file
     */
    public DoExport2MzML(MSDataFile rawDataFile, File targetFile) {
        this(rawDataFile, targetFile, CompressionType.ZLIB);
    }

    @Override
    public void start() throws IOException, XMLStreamException {

        MzMLWriter writer = new MzMLWriter(targetFile, mzCompress, inCompress, peakParameters);
        writer.writeStartDocument();
        rawDataFile.getHeader().setScanCount(rawDataFile.getSpectrumCount());
        writer.writeFileHeader(rawDataFile.getHeader());
        writer.writeStartSpectrumList();
        for (MsnSpectrum spectrum : rawDataFile.getSpectrumList()) {
            writer.writeSpectrum(spectrum);
        }
        writer.writeEndSpectrumList();

        List<Chromatogram> chromatogramList = rawDataFile.getChromatogramList();
        if (chromatogramList.size() > 0) {
            writer.writeStartChromatogramList();
            for (Chromatogram chromatogram : chromatogramList) {
                writer.writeChromatogram(chromatogram);
            }
            writer.writeEndChromatogramList();
        }

        writer.writeEndDocument();
        writer.close();
    }
}
