/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSDataFile;
import omics.msdk.model.MSHeader;
import omics.util.OmicsTask;
import omics.util.ms.MSFileType;
import omics.util.ms.MsnSpectrum;
import omics.util.ms.peaklist.Precision;

import java.io.File;
import java.io.IOException;

/**
 * @author JiaweiMao
 * @version 1.0.1
 * @since 16 Jan 2018, 3:14 PM
 */
public class MzXMLAccessor extends OmicsTask<MSDataFile> {

    private File iFile;

    public MzXMLAccessor(File sourceFile) {
        this.iFile = sourceFile;
    }

    @Override
    public void start() throws IOException {
        updateTitle("Readng mzXML file: " + iFile);

        MSDataFile rawDataFile = new MSDataFile(iFile.getName(), iFile, MSFileType.MZXML);

        MzXMLReader reader = new MzXMLReader(iFile, Precision.DOUBLE);
        MSHeader header = reader.getHeader();
        rawDataFile.setHeader(header);
        while (reader.hasNext()) {
            if (isStopped()) {
                reader.close();
                rawDataFile.clear();
                updateMessage("Reading file '" + iFile + "' is cancelled.");
                return;
            }

            MsnSpectrum spectrum = reader.next();
            rawDataFile.addSpectrum(spectrum);

            updateProgress(reader.getWorkDone(), reader.getTotalWork());
        }

        reader.close();
        rawDataFile.trim();
        updateValue(rawDataFile);
        updateMessage("Done!");
    }
}
