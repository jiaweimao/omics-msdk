/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.*;
import omics.msdk.model.*;
import omics.util.cv.Cv;
import omics.util.cv.CvParam;
import omics.util.cv.CvTerm;
import omics.util.cv.UserParam;
import omics.util.io.xml.IdentingXMLStreamWriter;
import omics.util.ms.*;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.util.MzMLSpectrumID;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * output rawdatafile to mzML.
 * <p>
 * parameters supported with PMs:
 * MzMLTags.AC_SCAN_FILTER_STRING
 * MzMLTags.AC_PRESET_SCAN_CONFIGURATION
 * <p>
 * NOTE:
 * 1. only support 1 precursor.
 * 2. do not support indexed mzML
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 24 Aug 2018, 8:46 PM
 */
public class MzMLWriter implements MSWriter {

    private static final String MZML_NAMESPACE = "http://psi.hupo.org/ms/mzml";
    private static final String XML_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String PREFIX_XSI = "xsi";
    private static final String XML_SCHEMA_LOCATION =
            "https://github.com/HUPO-PSI/mzML/blob/master/schema/schema_1.1/mzML1.1.0.xsd";
    private static final String DEFAULT_VERSION = "1.1.0";

    private File targetFile;
    private MSHeader header;
    private XMLStreamWriter iWriter;
    private PeakParameters peakParameters;
    private CompressionType mzCompress;
    private CompressionType inCompress;

    private int parsedScan = 0;
    private int parsedChrom = 0;

    public MzMLWriter(File targetFile, int bufferSize, CompressionType mzCompress, CompressionType inCompress, PeakParameters peakParameters) throws IOException {
        this.targetFile = targetFile;
        this.peakParameters = peakParameters;
        this.mzCompress = mzCompress;
        this.inCompress = inCompress;

        iWriter = new IdentingXMLStreamWriter(targetFile, bufferSize);
    }

    public MzMLWriter(File targetFile, CompressionType mzCompress, CompressionType inCompress, PeakParameters peakParameters) throws IOException {
        this.targetFile = targetFile;
        this.peakParameters = peakParameters;
        this.mzCompress = mzCompress;
        this.inCompress = inCompress;

        iWriter = new IdentingXMLStreamWriter(targetFile);
    }

    public MzMLWriter(File targetFile, CompressionType mzCompress, CompressionType inCompress) throws IOException {
        this(targetFile, mzCompress, inCompress, new PeakParameters());
    }

    public MzMLWriter(File targetFile, CompressionType compressionType) throws IOException {
        this(targetFile, compressionType, compressionType);
    }

    public MzMLWriter(File targetFile, int bufferSize) throws IOException {
        this(targetFile, bufferSize, CompressionType.ZLIB, CompressionType.ZLIB, new PeakParameters());
    }

    public MzMLWriter(File targetFile) throws IOException {
        this(targetFile, CompressionType.ZLIB);
    }

    public void writeStartSpectrumList() throws IOException {
        try {
            iWriter.writeStartElement(MzMLTags.TAG_SPECTRUM_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(header.getScanCount()));
            String defaultDPR = header.getDefaultDataProcessingRef();
            if (defaultDPR == null) {
                defaultDPR = header.getDataProcessingList().get(0).getId();
            }
            iWriter.writeAttribute(MzMLTags.ATTR_DEFAULT_DATA_PROCESSING_REF, defaultDPR);
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    public void writeEndSpectrumList() throws IOException {
        try {
            iWriter.writeEndElement(); // spectrumList
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    public void writeStartChromatogramList() throws IOException {
        try {
            iWriter.writeStartElement(MzMLTags.TAG_CHROMATOGRAM_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, String.valueOf(header.getChromatogramCount()));
            iWriter.writeAttribute(MzMLTags.ATTR_DEFAULT_DATA_PROCESSING_REF, header.getDefaultDataProcessingRef());
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    public void writeEndChromatogramList() throws IOException {
        try {
            iWriter.writeEndElement(); // chromatogramList
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void writeEndDocument() throws IOException {
        try {
            iWriter.writeEndElement(); // end of run
            iWriter.writeEndElement(); // end of mzML
            iWriter.writeEndDocument();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    private void writeBinaryData(BinaryDataArray binaryDataArray) throws XMLStreamException {
        iWriter.writeStartElement(MzMLTags.TAG_BINARY_DATA_ARRAY);
        iWriter.writeAttribute(MzMLTags.ATTR_ENCODED_LENGTH, String.valueOf(binaryDataArray.getEncodedLength()));

        writeParamGroup(binaryDataArray);

        iWriter.writeStartElement(MzMLTags.TAG_BINARY);
        iWriter.writeCharacters(new String(binaryDataArray.getBinary()));
        iWriter.writeEndElement(); // binary
        iWriter.writeEndElement(); // binaryDataArray
    }

    private void writeParamGroup(ParamGroup paramGroup) throws XMLStreamException {
        for (CvParam cvParam : paramGroup.getCvParams()) {
            writeCvParam(cvParam);
        }
        for (UserParam userParam : paramGroup.getUserParams()) {
            writeUserParam(userParam);
        }
    }

    private void writeCvParam(CvParam param) throws XMLStreamException {
        iWriter.writeEmptyElement(MzMLTags.TAG_CV_PARAM);
        CvTerm cvTerm = param.getCvTerm();

        iWriter.writeAttribute(MzMLTags.ATTR_CV_REF, cvTerm.getCvRef());
        iWriter.writeAttribute(MzMLTags.ATTR_ACCESSION, cvTerm.getAccession());
        iWriter.writeAttribute(MzMLTags.ATTR_NAME, cvTerm.getName());
        iWriter.writeAttribute(MzMLTags.ATTR_VALUE, param.getValue());

        Cv unitCv = cvTerm.getUnitCv();
        if (unitCv != null)
            iWriter.writeAttribute("unitCvRef", cvTerm.getUnitCvRef());
        if (cvTerm.getUnitAccession() != null) {
            iWriter.writeAttribute("unitAccession", cvTerm.getUnitAccession());
        }
        if (cvTerm.getUnitName() != null) {
            iWriter.writeAttribute("unitName", cvTerm.getUnitName());
        }
    }

    private void writeUserParam(UserParam param) throws XMLStreamException {
        iWriter.writeEmptyElement(MzMLTags.TAG_USER_PARAM);
        iWriter.writeAttribute(MzMLTags.ATTR_NAME, param.getName());
        if (param.getValue() != null) {
            iWriter.writeAttribute(MzMLTags.ATTR_VALUE, param.getValue());
        }
        if (param.getType() != null) {
            iWriter.writeAttribute(MzMLTags.ATTR_USER_PARAM_TYPE, param.getType());
        }
    }

    @Override
    public void writeStartDocument() throws XMLStreamException {
        // set namespace
        iWriter.setDefaultNamespace(MZML_NAMESPACE);
        iWriter.setPrefix(PREFIX_XSI, XML_SCHEMA_INSTANCE);

        // <?xml>
        iWriter.writeStartDocument("UTF-8", "1.0");

        // mzML
        iWriter.writeStartElement(MzMLTags.TAG_MZML);
        iWriter.writeDefaultNamespace(MZML_NAMESPACE);
        iWriter.writeNamespace(PREFIX_XSI, XML_SCHEMA_INSTANCE);
        iWriter.writeAttribute(XML_SCHEMA_INSTANCE, MzMLTags.ATTR_SCHEME_LOCATION, XML_SCHEMA_LOCATION);
        iWriter.writeAttribute(MzMLTags.ATTR_ID, targetFile.getName());
        iWriter.writeAttribute(MzMLTags.ATTR_VERSION, DEFAULT_VERSION);
    }

    @Override
    public void writeFileHeader(MSHeader msHeader) throws XMLStreamException {
        this.header = msHeader;
        if (header == null) {
            header = new MSHeader();
        }
        // cvList
        List<Cv> cvList = header.getCvList();
        if (cvList.isEmpty()) { // at least these two CVs
            cvList.add(Cv.PSI_MS);
            cvList.add(Cv.UNIT_ONTOLOGY);
        }
        iWriter.writeStartElement(MzMLTags.TAG_CV_LIST);
        iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(cvList.size()));
        for (Cv cv : cvList) {
            iWriter.writeEmptyElement(MzMLTags.TAG_CV);
            iWriter.writeAttribute(MzMLTags.ATTR_ID, cv.getID());
            iWriter.writeAttribute(MzMLTags.ATTR_FULL_NAME, cv.getFullName());
            iWriter.writeAttribute(MzMLTags.ATTR_VERSION, cv.getVersion());
            iWriter.writeAttribute(MzMLTags.TAG_URI, cv.getUri());
        }
        iWriter.writeEndElement(); // end of cvList

        // <fileDescription>
        iWriter.writeStartElement(MzMLTags.TAG_FILE_DESCRIPTION);
        FileDescription fileDescription = header.getFileDescription();
        // fileContent
        ParamGroup fileContent = fileDescription.getFileContent();
        if (fileContent == null) {
            fileContent = new ParamGroup();
            fileContent.addCvParam(FileDescription.MS1_Spectrum);
            fileContent.addCvParam(FileDescription.MSn_Spectrum);
        }
        iWriter.writeStartElement(MzMLTags.TAG_FILE_CONTENT);
        writeParamGroup(fileContent);
        iWriter.writeEndElement(); // end of fileContent

        // <sourceFileList>
        List<SourceFile> sourceFileList = fileDescription.getSourceFileList();
        if (!sourceFileList.isEmpty()) {
            iWriter.writeStartElement(MzMLTags.TAG_SOURCE_FILE_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(sourceFileList.size()));
            for (SourceFile sourceFile : sourceFileList) {
                iWriter.writeStartElement(MzMLTags.TAG_SOURCE_FILE);
                iWriter.writeAttribute(MzMLTags.ATTR_ID, sourceFile.getId());
                iWriter.writeAttribute(MzMLTags.ATTR_NAME, sourceFile.getName());
                iWriter.writeAttribute(MzMLTags.ATTR_LOCATION, sourceFile.getLocation());
                writeParamGroup(sourceFile);
                iWriter.writeEndElement();
            }
            iWriter.writeEndElement(); // end of sourceFileList
        }
        // contact
        List<ParamGroup> contactList = fileDescription.getContactList();
        if (!contactList.isEmpty()) {
            for (ParamGroup paramGroup : contactList) {
                iWriter.writeStartElement(MzMLTags.TAG_CONTACT);
                writeParamGroup(paramGroup);
                iWriter.writeEndElement();
            }
        }
        iWriter.writeEndElement(); // end of fileDescription

        // referenceableParamGroupList
        List<ReferenceableParamGroup> referenceableParamGroupList = header.getReferenceableParamGroupList();
        if (referenceableParamGroupList != null) {
            iWriter.writeStartElement(MzMLTags.TAG_REF_PARAM_GROUP_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(header.getReferenceableParamGroupList().size()));
            for (ReferenceableParamGroup paramGroup : header.getReferenceableParamGroupList()) {
                iWriter.writeStartElement(MzMLTags.TAG_REF_PARAM_GROUP);
                iWriter.writeAttribute(MzMLTags.ATTR_ID, paramGroup.getId());
                List<CvParam> cvParamList = paramGroup.getCvParamList();
                if (cvParamList != null) {
                    for (CvParam cvParam : cvParamList) {
                        writeCvParam(cvParam);
                    }
                }
                iWriter.writeEndElement(); // referenceableParamGroup
            }
            iWriter.writeEndElement(); // referenceableParamGroupList
        }

        // sampleList
        List<Sample> sampleList = header.getSampleList();
        if (sampleList != null) {
            iWriter.writeStartElement(MzMLTags.TAG_SAMPLE_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(sampleList.size()));
            for (Sample sample : sampleList) {
                iWriter.writeStartElement(MzMLTags.TAG_SAMPLE);
                iWriter.writeAttribute(MzMLTags.ATTR_ID, sample.getId());
                if (sample.getName() != null) {
                    iWriter.writeAttribute(MzMLTags.ATTR_NAME, sample.getName());
                }
                writeParamGroup(sample);
                iWriter.writeEndElement();
            }
            iWriter.writeEndElement();
        }

        Software omicsMSDK = new Software("omics-msdk", "1.0");
        // softwareList
        iWriter.writeStartElement(MzMLTags.TAG_SOFTWARE_LIST);
        List<Software> softwareList = header.getSoftwareList();
        if (softwareList == null) {
            softwareList = new ArrayList<>();
            softwareList.add(omicsMSDK);
        }
        iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(softwareList.size()));
        for (Software software : softwareList) {
            iWriter.writeStartElement(MzMLTags.TAG_SOFTWARE);
            iWriter.writeAttribute(MzMLTags.ATTR_ID, software.getID());
            iWriter.writeAttribute(MzMLTags.ATTR_VERSION, software.getVersion());
            writeParamGroup(software);
            iWriter.writeEndElement();
        }
        iWriter.writeEndElement(); // softwareList

        // instrumentConfigurationList
        List<InstrumentConfiguration> instrumentConfigurationList = header.getInstrumentConfigurationList();
        if (instrumentConfigurationList.isEmpty()) {
            InstrumentConfiguration ic = new InstrumentConfiguration("IC1");
            instrumentConfigurationList.add(ic);
        }
        iWriter.writeStartElement(MzMLTags.TAG_INSTRUMENT_CONFIGURATION_LIST);
        iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(instrumentConfigurationList.size()));
        for (InstrumentConfiguration instrumentConfiguration : instrumentConfigurationList) {
            iWriter.writeStartElement(MzMLTags.TAG_INSTRUMENT_CONFIGURATION);
            iWriter.writeAttribute(MzMLTags.ATTR_ID, instrumentConfiguration.getId());
            writeParamGroup(instrumentConfiguration);

            List<String> referenceableParamGroupRefs = instrumentConfiguration.getReferenceableParamGroupRefs();
            if (!referenceableParamGroupRefs.isEmpty()) {
                for (String ref : referenceableParamGroupRefs) {
                    iWriter.writeEmptyElement(MzMLTags.TAG_REF_PARAM_GROUP_REF);
                    iWriter.writeAttribute(MzMLTags.ATTR_REF, ref);
                }
            }

            List<Component> componentList = instrumentConfiguration.getComponentList();
            if (!componentList.isEmpty()) {
                iWriter.writeStartElement(MzMLTags.TAG_COMPONENT_LIST);
                iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(componentList.size()));
                for (Component component : componentList) {
                    if (component instanceof IonSource) {
                        iWriter.writeStartElement(MzMLTags.TAG_SOURCE);
                    } else if (component instanceof Analyzer) {
                        iWriter.writeStartElement(MzMLTags.TAG_ANALYZER);
                    } else if (component instanceof Detector) {
                        iWriter.writeStartElement(MzMLTags.TAG_DETECTOR);
                    }
                    iWriter.writeAttribute(MzMLTags.ATTR_ORDER, Integer.toString(component.getOrder()));
                    writeParamGroup(component);
                    iWriter.writeEndElement();
                }
                iWriter.writeEndElement(); // componentList
            }

            String softwareRef = instrumentConfiguration.getSoftwareRef();
            if (softwareRef != null) {
                iWriter.writeEmptyElement(MzMLTags.ATTR_SOFTWARE_REF);
                iWriter.writeAttribute(MzMLTags.ATTR_REF, softwareRef);
            }

            iWriter.writeEndElement(); // instrumentConfiguration
        }
        iWriter.writeEndElement(); // instrumentConfigurationList

        // dataProcessingList, required
        List<DataProcessing> dataProcessingList = header.getDataProcessingList();
        if (dataProcessingList.isEmpty()) {
            DataProcessing dataProcessing = new DataProcessing("DP1");
            ProcessingMethod method = new ProcessingMethod(0, omicsMSDK, new CvParam(ProcessingMethod.CONVERSION_2_MZML, ""));
            dataProcessing.addProcessingMethod(method);
            dataProcessingList.add(dataProcessing);
        }
        iWriter.writeStartElement(MzMLTags.TAG_DATA_PROCESSING_LIST);
        iWriter.writeAttribute(MzMLTags.ATTR_COUNT, Integer.toString(dataProcessingList.size()));
        for (DataProcessing dataProcessing : dataProcessingList) {
            iWriter.writeStartElement(MzMLTags.TAG_DATA_PROCESSING);
            iWriter.writeAttribute(MzMLTags.ATTR_ID, dataProcessing.getId());

            for (ProcessingMethod processingMethod : dataProcessing.getProcessingMethodList()) {
                iWriter.writeStartElement(MzMLTags.TAG_PROCESSING_METHOD);
                iWriter.writeAttribute(MzMLTags.ATTR_ORDER, processingMethod.getOrder().toString());
                String softwareRef = processingMethod.getSoftwareRef();
                if (softwareRef == null)
                    softwareRef = "";
                iWriter.writeAttribute(MzMLTags.ATTR_SOFTWARE_REF, softwareRef);
                writeParamGroup(processingMethod);
                iWriter.writeEndElement();
            }

            iWriter.writeEndElement(); // dataProcessing
        }
        iWriter.writeEndElement(); // dataProcessingList

        iWriter.writeStartElement(MzMLTags.TAG_RUN);
        String runId = header.getRunId();
        if (runId == null) {
            runId = header.getMSDataID().getID();
        }
        iWriter.writeAttribute(MzMLTags.ATTR_ID, runId);
        String defaulticr = header.getDefaultInstrumentConfigurationRef();
        if (defaulticr == null) {
            defaulticr = instrumentConfigurationList.get(0).getId();
        }
        iWriter.writeAttribute(MzMLTags.ATTR_DEFAULT_INSTRUMENT_CONFIGURATION_REF, defaulticr);
    }

    @Override
    public void writeSpectrum(MsnSpectrum spectrum) throws IOException {
        // <spectrum>
        try {
            iWriter.writeStartElement(MzMLTags.TAG_SPECTRUM);
            iWriter.writeAttribute(MzMLTags.ATTR_INDEX, String.valueOf(parsedScan++));

            MzMLSpectrumID spectrumID = (MzMLSpectrumID) spectrum.getSpectrumID();

            String id = spectrumID.getID();
            if (id == null) {
                id = "scan=" + spectrumID.getScanNumber();
            }
            iWriter.writeAttribute(MzMLTags.ATTR_ID, id);
            iWriter.writeAttribute(MzMLTags.ATTR_DEFAULT_ARRAY_LENGTH, String.valueOf(spectrum.size()));

            int msLevel = spectrum.getMsLevel();
            if (msLevel == 1)
                writeCvParam(CvTermList.MS1);
            else
                writeCvParam(CvTermList.MSn);

            // ms level cvParam
            writeCvParam(new CvParam(CvTermList.MS_LEVEL, String.valueOf(msLevel)));

            // polarity cvParam
            Polarity polarity = spectrumID.getPolarity();
            if (polarity == Polarity.POSITIVE)
                writeCvParam(CvTermList.POSITIVE_SCAN);
            else if (polarity == Polarity.NEGATIVE) {
                writeCvParam(CvTermList.NEGATIVE_SCAN);
            }

            // spectrum type cvParam
            if (spectrum.getSpectrumType() == SpectrumType.CENTROIDED)
                writeCvParam(CvTermList.CENTROID_SPECTRUM);
            else if (spectrum.getSpectrumType() == SpectrumType.PROFILE) {
                writeCvParam(CvTermList.PROFILE_SPECTRUM);
            }

            // basePeak mz
            CvParam baseMzcvParam = new CvParam(CvTermList.BASEPEAK_MZ, peakParameters.mzFormat().format(spectrum.getBasePeakX()));
            writeCvParam(baseMzcvParam);

            // basePeak intensity cvParam
            CvParam baseYcvParam = new CvParam(CvTermList.BASEPEAK_INTENSITY, peakParameters.intensityFormat().format(spectrum.getBasePeakY()));
            writeCvParam(baseYcvParam);

            // total ion current
            CvParam ticParam = new CvParam(CvTermList.TOTAL_ION_CURRENT, String.valueOf(spectrum.getTotalIonCurrent()));
            writeCvParam(ticParam);

            // lowest observed m/z cvParam
            CvParam mz0Param = new CvParam(CvTermList.LOWEST_OBSERVED_MZ, peakParameters.mzFormat().format(spectrum.getMz(0)));
            writeCvParam(mz0Param);

            // highest observed m/z cvParam
            CvParam mzhParam = new CvParam(CvTermList.HIGHEST_OBSERVED_MZ, peakParameters.mzFormat().format(spectrum.getMz(spectrum.size() - 1)));
            writeCvParam(mzhParam);

            // spectrumTitle
            CvParam titleParam = new CvParam(CvTermList.SPECTRUM_TITLE, spectrumID.getTitle());
            writeCvParam(titleParam);

            // <scanList>
            iWriter.writeStartElement(MzMLTags.TAG_SCAN_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "1");
            iWriter.writeStartElement(MzMLTags.TAG_SCAN);

            // retention time cvParam
            CvParam timeCvParam = new CvParam(CvTermList.SCAN_START_TIME,
                    peakParameters.rtFormat().format(spectrumID.getRetentionTime().getTime() / 60));
            writeCvParam(timeCvParam);

            // scan definition
            String scanDefinition = spectrum.getPMString(CvTermList.ACC_SCAN_FILTER_STRING);
            if (scanDefinition != null) {
                CvParam filterStringParam = new CvParam(CvTermList.SCAN_FILTER_STRING, scanDefinition);
                writeCvParam(filterStringParam);
            }

            // preset scan configuration
            String scanConfiguration = spectrum.getPMString(CvTermList.ACC_PRESET_SCAN_CONFIGURATION);
            if (scanConfiguration != null) {
                CvParam scanConfigParam = new CvParam(CvTermList.PRESET_SCAN_CONFIGURATION, scanConfiguration);
                writeCvParam(scanConfigParam);
            }

            Float ionInjectTime = spectrumID.getIonInjectTime();
            if (ionInjectTime != null) {
                CvParam ionInjectTimeParam = new CvParam(CvTermList.ION_INJECTION_TIME,
                        peakParameters.rtFormat().format(ionInjectTime));
                writeCvParam(ionInjectTimeParam);
            }

            if (spectrumID.getScanLowerMz() != null && spectrumID.getScanUpperMz() != null) {
                // scanWindowList
                iWriter.writeStartElement(MzMLTags.TAG_SCAN_WINDOW_LIST);
                iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "1");
                // scanWindow
                iWriter.writeStartElement(MzMLTags.TAG_SCAN_WINDOW);
                CvParam lowerWindow = new CvParam(CvTermList.SCAN_WINDOW_LOWER_LIMIT, String.valueOf(spectrumID.getScanLowerMz()));
                CvParam upperWindow = new CvParam(CvTermList.SCAN_WINDOW_UPPER_LIMIT, String.valueOf(spectrumID.getScanUpperMz()));

                writeCvParam(lowerWindow);
                writeCvParam(upperWindow);
                iWriter.writeEndElement(); // scanWindow
                iWriter.writeEndElement(); // scanWindowList
            }

            iWriter.writeEndElement(); // scan
            iWriter.writeEndElement(); // scanList

            // currently only support 1 precursor
            Peak precursor = spectrum.getPrecursor();
            if (precursor != null && spectrum.getMsLevel() > 1) {
                iWriter.writeStartElement(MzMLTags.TAG_PRECURSOR_LIST);
                iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "1");
                iWriter.writeStartElement(MzMLTags.TAG_PRECURSOR);
                iWriter.writeAttribute(MzMLTags.ATTR_SPECTRUM_REF, id);

                // isolationWindow [0,1]
                Double isolateTargetMz = spectrumID.getIsolateWindowTargetMz();
                if (isolateTargetMz != null) {
                    iWriter.writeStartElement(MzMLTags.TAG_ISOLATION_WINDOW);
                    CvParam targetMzParam = new CvParam(CvTermList.ISOLATION_WINDOW_TARGET, spectrumID.getIsolateWindowTargetMz().toString());
                    CvParam isoLowerParam = new CvParam(CvTermList.ISOLATION_WINDOW_LOWER, spectrumID.getIsolateWindowLowerOffset().toString());
                    CvParam isoUpperParam = new CvParam(CvTermList.ISOLATION_WINDOW_UPPER, spectrumID.getIsolateWindowUpperOffset().toString());
                    writeCvParam(targetMzParam);
                    writeCvParam(isoLowerParam);
                    writeCvParam(isoUpperParam);
                    iWriter.writeEndElement(); // isolationWindow
                }

                // selectedIonList [0,1]
                iWriter.writeStartElement(MzMLTags.TAG_SELECTED_ION_LIST);
                iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "1");
                iWriter.writeStartElement(MzMLTags.TAG_SELECTED_ION);
                CvParam pMzParam = new CvParam(CvTermList.SELECTED_ION_MZ, String.valueOf(precursor.getMz()));
                CvParam zParam = new CvParam(CvTermList.SELECTED_ION_CHARGE_STATE, String.valueOf(precursor.getCharge()));
                CvParam inParam = new CvParam(CvTermList.SELECTED_ION_PEAK_INTENSITY, String.valueOf(precursor.getIntensity()));
                writeCvParam(pMzParam);
                writeCvParam(zParam);
                writeCvParam(inParam);
                iWriter.writeEndElement(); // selectedIon
                iWriter.writeEndElement(); // selectedIonList

                // activation
                iWriter.writeStartElement(MzMLTags.TAG_ACTIVATION);
                Dissociation dissociation = spectrumID.getDissociation();
                CvParam dissoParam = new CvParam(new CvTerm(dissociation.getAccession(), dissociation.getName()), "");
                writeCvParam(dissoParam);
                Double collisionEnergy = spectrumID.getCollisionEnergy();
                if (collisionEnergy != null) {
                    CvParam energy = new CvParam(CvTermList.COLLISION_ENERGY, collisionEnergy.toString());
                    writeCvParam(energy);
                }

                iWriter.writeEndElement(); // activation
                iWriter.writeEndElement(); // precursor
                iWriter.writeEndElement(); // precursorList
            }

            // binaryDataArrayList
            iWriter.writeStartElement(MzMLTags.TAG_BINARY_DATA_ARRAY_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "2");

            BinaryDataArray mzArray = new BinaryDataArray();
            mzArray.setValues(spectrum.getXs(), mzCompress);
            mzArray.setDataType(DataType.MZ);
            writeBinaryData(mzArray);

            BinaryDataArray inArray = new BinaryDataArray();
            inArray.setValues(spectrum.getYs(), inCompress);
            inArray.setDataType(DataType.INTENSITY);
            writeBinaryData(inArray);

            iWriter.writeEndElement(); // binaryDataArrayList
            iWriter.writeEndElement(); // <spectrum>
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void writeChromatogram(Chromatogram chromatogram) throws IOException {
        try {
            iWriter.writeStartElement(MzMLTags.TAG_CHROMATOGRAM);
            iWriter.writeAttribute(MzMLTags.ATTR_INDEX, String.valueOf(parsedChrom++));
            iWriter.writeAttribute(MzMLTags.ATTR_ID, chromatogram.getID());
            iWriter.writeAttribute(MzMLTags.ATTR_DEFAULT_ARRAY_LENGTH, String.valueOf(chromatogram.size()));

            ChromatogramType chromatogramType = chromatogram.getChromatogramType();
            CvParam param = new CvParam(new CvTerm(chromatogramType.getAccession(), chromatogramType.getName()), "");
            writeCvParam(param);

            iWriter.writeStartElement(MzMLTags.TAG_BINARY_DATA_ARRAY_LIST);
            iWriter.writeAttribute(MzMLTags.ATTR_COUNT, "2");

            BinaryDataArray binaryDataArray = new BinaryDataArray();
            binaryDataArray.setValues(chromatogram.getXs(), mzCompress);
            binaryDataArray.setDataType(DataType.TIME);
            writeBinaryData(binaryDataArray);

            BinaryDataArray inData = new BinaryDataArray();
            inData.setValues(chromatogram.getYs(), inCompress);
            inData.setDataType(DataType.INTENSITY);
            writeBinaryData(inData);

            iWriter.writeEndElement();
            iWriter.writeEndElement(); // chromatogram
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        try {
            iWriter.close();
        } catch (XMLStreamException e) {
            throw new IOException(e);
        }
    }
}
