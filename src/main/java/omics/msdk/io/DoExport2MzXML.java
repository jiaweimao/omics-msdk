/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.io.util.BitLength;
import omics.msdk.model.MSDataFile;
import omics.util.Action;
import omics.util.ms.Chromatogram;
import omics.util.ms.MsnSpectrum;

import java.io.File;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 12 Oct 2018, 8:39 PM
 */
public class DoExport2MzXML extends Action {

    private MSDataFile msDataFile;
    private File targetFile;
    private boolean compress;
    private BitLength bitLength;
    private PeakParameters peakParameters;

    public DoExport2MzXML(MSDataFile msDataFile, File targetFile, boolean compress, BitLength bitLength, PeakParameters peakParameters) {
        this.msDataFile = msDataFile;
        this.targetFile = targetFile;
        this.compress = compress;
        this.bitLength = bitLength;
        this.peakParameters = peakParameters;
    }

    public DoExport2MzXML(MSDataFile msDataFile, File targetFile) {
        this(msDataFile, targetFile, true, BitLength.FLOAT64BIT, new PeakParameters());
    }

    @Override
    public void start() throws Exception {

        MzXMLWriter writer = new MzXMLWriter(targetFile, compress, bitLength);
        writer.setPeakParameters(peakParameters);

        writer.writeStartDocument();
        msDataFile.getHeader().setScanCount(msDataFile.getSpectrumCount());
        writer.writeFileHeader(msDataFile.getHeader());
        writer.writeStartSpectrumList();
        for (MsnSpectrum spectrum : msDataFile) {
            writer.writeSpectrum(spectrum);
        }
        writer.writeEndSpectrumList();
        List<Chromatogram> chromatogramList = msDataFile.getChromatogramList();
        if (chromatogramList.size() > 0) {
            writer.writeStartChromatogramList();
            for (Chromatogram chromatogram : chromatogramList) {
                writer.writeChromatogram(chromatogram);
            }
            writer.writeEndChromatogramList();
        }

        writer.writeEndDocument();
        writer.close();
    }
}
