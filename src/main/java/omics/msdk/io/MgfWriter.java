/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msdk.io;

import omics.msdk.model.MSHeader;
import omics.util.ms.*;
import omics.util.ms.peaklist.Peak;
import omics.util.ms.peaklist.PeakAnnotation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * Abstract implementation of an MGF writer. This class takes care of writing
 * the standard structure of an MGF file. For each spectrum the following tags
 * are written:
 * <p>
 * BEGIN IONS </br>
 * TITLE= </br>
 * PEPMASS= </br>
 * CHARGE= </br>
 * RTINSECONDS= </br>
 * peak mz\tpeak intensity</br>
 * END IONS</br>
 * <p>
 * Subclasses have to implement methods to extract the charge list, retention
 * time, scan number and title to fill in the values for the tags.
 * <p>
 * By default this class does not write any ms1 header data, however subclasses
 * can generate a ms1 header by using a Parameters implementation that contains
 * data for the header and overriding writeMs1Header(P, Writer) method which is
 * called by the constructor.
 * <p>
 * To write a PeakList and associated meta data subclasses have to call
 * writeMs2(PeakList peakList, M metaData).
 *
 * @author JiaweiMao
 * @version 1.0.1
 */
public class MgfWriter implements MSWriter {

    private final Writer writer;
    private PeakParameters peakParameters;

    public MgfWriter(Writer writer, PeakParameters parameters) {
        checkNotNull(writer);
        checkNotNull(parameters);

        this.peakParameters = parameters;
        this.writer = writer;
    }

    public MgfWriter(File file, PeakParameters peakParameters) throws IOException {
        this(new FileWriter(file), peakParameters);
    }

    public MgfWriter(File file) throws IOException {
        this(file, new PeakParameters());
    }

    public void writeComment(String comment) throws IOException {
        writer.write("# ");
        writer.write(comment);
        writer.write("\n");
    }

    protected void writeMs2Header(MsnSpectrum spectrum, Writer writer) throws IOException {
        writeTitle(spectrum, writer);
        writePepmass(spectrum, writer);
        writeCharge(spectrum, writer);
        writeScans(spectrum, writer);
        writeRtInSeconds(spectrum, writer);
    }

    protected void writeTitle(MsnSpectrum spectrum, Writer writer) throws IOException {
        String comment = spectrum.getTitle();
        if (comment.length() > 0) {
            writer.write("TITLE=");
            writer.write(comment);
            writer.write("\n");
        }
    }

    protected void writePepmass(MsnSpectrum spectrum, Writer writer) throws IOException {
        Peak precursor = spectrum.getPrecursor();

        writer.write("PEPMASS=");

        writer.write(peakParameters.mzFormat().format(precursor.getMz()));
        writer.write(' ');

        writer.write(peakParameters.intensityFormat().format(precursor.getIntensity()));
        writer.write("\n");
    }

    protected void writeScans(MsnSpectrum spectrum, Writer writer) throws IOException {
        ScanNumberList scanNumberList = spectrum.getScanNumberList();
        if (!scanNumberList.isEmpty()) {

            writer.write("SCANS=");
            boolean first = true;
            for (ScanNumber sn : scanNumberList) {

                if (first) {
                    first = false;
                } else {
                    writer.write(",");
                }

                if (sn instanceof ScanNumberDiscrete) {
                    writer.write(Integer.toString(sn.getValue()));
                } else if (sn instanceof ScanNumberInterval) {
                    writer.write(Integer.toString(sn.getMinScanNumber()));
                    writer.write('-');
                    writer.write(Integer.toString(sn.getMaxScanNumber()));
                }
            }
            writer.write('\n');
        }
    }

    protected void writeRtInSeconds(MsnSpectrum metaData, Writer writer) throws IOException {
        RetentionTimeList retentionTimeList = metaData.getSpectrumID().getRetentionTimeList();
        if (retentionTimeList.isEmpty())
            return;

        writer.write("RTINSECONDS=");
        boolean first = true;
        for (RetentionTime rt : retentionTimeList) {

            if (first) {
                first = false;
            } else {
                writer.write(",");
            }

            if (rt instanceof RetentionTimeDiscrete) {
                writer.write(peakParameters.rtFormat().format(rt.getTime()));
            } else {
                writer.write(peakParameters.rtFormat().format(rt.getMinTime()));
                writer.write('-');
                writer.write(peakParameters.rtFormat().format(rt.getMaxTime()));
            }
        }
        writer.write('\n');
    }

    protected void writePeaks(MsnSpectrum spectrum, Writer writer) throws IOException {
        for (int i = 0; i < spectrum.size(); i++) {

            double mz = spectrum.getX(i);
            double intensity = spectrum.getY(i);

            writer.write(peakParameters.mzFormat().format(mz));
            writer.write(' ');
            writer.write(peakParameters.intensityFormat().format(intensity));
            writePeakAnnotations(spectrum.getAnnotations(i), writer);
            writer.write("\n");
        }
    }

    /**
     * Subclasses that want to add annotation information to a peak can override this method.
     * <p>
     * Before this method is called the peak mz and intensity were written.
     * Writing \n is taken care of in the method that calls this.
     *
     * @param annotations the annotations for the peak being written
     * @param writer      the writer
     */
    protected void writePeakAnnotations(List<PeakAnnotation> annotations, Writer writer) {
    }

    protected void writeCharge(MsnSpectrum spectrum, Writer writer) throws IOException {
        int[] charges = spectrum.getPrecursor().getCharges();

        if (charges.length > 0) {

            writer.write("CHARGE=");

//            Polarity polarity = peakList.getPrecursor().getPolarity();
            boolean first = true;
            for (int charge : charges) {
                if (first) {
                    first = false;
                } else {
                    writer.write(", ");
                }
                writer.write(String.valueOf(Math.abs(charge)));
//
//                if (polarity == Polarity.POSITIVE) {
//                    writer.write('+');
//                } else if (polarity == Polarity.NEGATIVE) {
//
//                    writer.write('-');
//                }
            }
            writer.write('\n');
        }
    }

    @Override
    public void writeStartDocument() {
    }

    @Override
    public void writeFileHeader(MSHeader header) {
    }

    @Override
    public void writeStartSpectrumList() {
    }

    @Override
    public void writeSpectrum(MsnSpectrum spectrum) throws IOException {
        writer.write("BEGIN IONS\n");
        writeMs2Header(spectrum, writer);
        writePeaks(spectrum, writer);
        writer.write("END IONS\n");
        writer.write("\n");
    }

    @Override
    public void writeEndSpectrumList() {
    }

    @Override
    public void writeStartChromatogramList() {
    }

    @Override
    public void writeChromatogram(Chromatogram chromatogram) {
    }

    @Override
    public void writeEndChromatogramList() {
    }

    @Override
    public void writeEndDocument() {
    }

    public void close() throws IOException {
        writer.close();
    }
}
